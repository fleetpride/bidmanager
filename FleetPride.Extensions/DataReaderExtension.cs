﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.Extensions
{
    public static class DataReaderExtension
    {
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public static bool IsDBNull(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return dr.IsDBNull(i);
            }
            return true;
        }

        public static string GetStringIfHas(this IDataRecord rdr, string columnName)
        {
            return rdr.HasColumn(columnName)
                ? (rdr.IsDBNull(columnName) ? "" : Convert.ToString(rdr[columnName]))
                : "";
        }

        public static Int32 GetIntIfHas(this IDataRecord rdr, string columnName)
        {
            return rdr.HasColumn(columnName) 
                ? (rdr.IsDBNull(columnName) ? 0 : Convert.ToInt32(rdr[columnName]))
                : 0;
        }

        public static bool GetBoolIfHas(this IDataRecord rdr, string columnName)
        {
            return rdr.HasColumn(columnName)
                ? (rdr.IsDBNull(columnName) ? false : Convert.ToBoolean(rdr[columnName]))
                : false;
        }

        public static Decimal GetDecimalIfHas(this IDataRecord rdr, string columnName)
        {
            return rdr.HasColumn(columnName)
                ? (rdr.IsDBNull(columnName) ? 0 : Convert.ToDecimal(rdr[columnName]))
                : 0;
        }

        public static DateTime GetDateTimeIfHas(this IDataRecord rdr, string columnName)
        {
            return rdr.HasColumn(columnName)
                ? (rdr.IsDBNull(columnName) ? DateTime.MinValue : Convert.ToDateTime(rdr[columnName]))
                : DateTime.MinValue;
        }
    }
}