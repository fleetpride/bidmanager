﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FleetPride.Extensions
{
    public static class ConvertExtensions
    {
        /// <summary>
        /// return int if the string is a valid number else return 0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(string value)
        {
            int output = 0;
            if(!string.IsNullOrWhiteSpace(value))
                int.TryParse(value.Trim(), out output);
            return output;
        }
    }
}