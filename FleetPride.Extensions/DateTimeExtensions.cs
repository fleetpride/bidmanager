﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FleetPride.Extensions
{
    public static class DateTimeExtensions
    {
        private static Dictionary<DateTime, string> _holidays = null;
        private static DateTime _lastLoad = DateTime.MinValue;

        /// <summary>
        /// laziely gets the holiday list from dbo.Holidays and stores it for further use.
        /// To keep the performance high this loads the list only once a day
        /// When the dbo.Holidays table is updated it will take a day to take effect here.
        /// or you can restart the app pool to force this to reload
        /// </summary>
        public static Dictionary<DateTime, string> Holidays
        {
            get
            {
                if (_holidays == null || DateTime.Now.Subtract(_lastLoad).TotalDays > 1)
                {
                    _holidays = GetHolidays();
                    _lastLoad = DateTime.Now;
                }

                return _holidays;
            }
        }

        private static Dictionary<DateTime, string> GetHolidays()
        {
            Dictionary<DateTime, string> holidays = new Dictionary<DateTime, string>();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BM"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.GetHolidays", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        holidays.Add(Convert.ToDateTime(rdr["HolidayDate"]), Convert.ToString(rdr["HolidayName"]));
                    }
                }
            }

            return holidays;
        }

        /// <summary>
        /// Add the business days to the given date
        /// </summary>
        /// <param name="originalDate">Given Date</param>
        /// <param name="workDays">Number of Business days</param>
        /// <returns>Returns the Business Days</returns>
        public static DateTime AddWorkdays(this DateTime originalDate, int workDays)
        {
            DateTime tmpDate = originalDate;
            while (workDays > 0)
            {
                tmpDate = tmpDate.AddDays(1);
                if (tmpDate.DayOfWeek < DayOfWeek.Saturday &&
                    tmpDate.DayOfWeek > DayOfWeek.Sunday &&
                    !tmpDate.IsHoliday())
                    workDays--;
            }
            return tmpDate;
        }

        /// <summary>
        /// Gets the date of the next Monday from the given date
        /// </summary>
        /// <param name="originalDate"></param>
        /// <returns>Next Monday</returns>
        public static DateTime GetNextMonday(this DateTime originalDate)
        {
            DateTime tmpDate = originalDate;
            while (tmpDate.DayOfWeek != DayOfWeek.Monday)
            {
                tmpDate = tmpDate.AddDays(1);
            }

            return tmpDate;
        }

        public static bool IsHoliday(this DateTime originalDate)
        {
            return Holidays.ContainsKey(new DateTime(originalDate.Year, originalDate.Month, originalDate.Day));
        }
    }
}