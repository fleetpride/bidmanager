﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FleetPride.BidManager;
using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using FleetPride.BidManager.Repository;

namespace FleetPride.BidManager.Tests
{
    [TestClass]
    public class OpportunityCreation
    {
        [TestMethod]
        public void OpportunityEntity_GetInsertJson()
        {
            OpportunityEntity opentity = new OpportunityEntity();
            opentity.OppId = "123";
            opentity.OpportunityAmount = (decimal)100.23;
            opentity.OpportunityName = "Test1";
            opentity.OpportunityStage = Constants.OpportunityStage.New;
            opentity.AccountGroup = "Group 1";
            opentity.AccountIseriesCode = "100";
            opentity.closeDate = new DateTime(2020, 02, 20);

            string json = opentity.GetInsertJson();
            string expectedjson = "{\"Record\":[{\"OpportunityName\":\"Test1\",\"AccountIseriesCode\":\"100\",\"AccountGroup\":\"Group 1\",\"OpportunityAmount\":100.23,\"OpportunityStage\":\"01-New\",\"closeDate\":\"02/20/2020\"}]}";
            Assert.AreEqual<string>(expectedjson, json);
        }

        [TestMethod]
        public void OpportunityEntity_GetUpdateJson()
        {
            OpportunityEntity opentity = new OpportunityEntity();
            opentity.OppId = "123";
            opentity.OpportunityAmount = (decimal)100.23;
            opentity.OpportunityName = "Test1";
            opentity.OpportunityStage = Constants.OpportunityStage.New;
            opentity.AccountGroup = "Group 1";
            opentity.AccountIseriesCode = "100";
            opentity.closeDate = new DateTime(2020, 02, 20);

            string json = opentity.GetUpdateJson();
            string expectedjson = "{\"Record\":[{\"OppId\":\"123\",\"OpportunityAmount\":100.23,\"OpportunityStage\":\"01-New\"}]}";
            Assert.AreEqual<string>(expectedjson, json);
        }

        [TestMethod]
        public void OpportunityRepository_GetAuthToken()
        {
            OpportunityRepository repo = new OpportunityRepository();
            string token = repo.GetAuthToken();
            Assert.IsTrue(!string.IsNullOrEmpty(token));
        }

        [TestMethod]
        public void OpportunityRepository_Insert()
        {
            OpportunityEntity opentity = new OpportunityEntity();
            opentity.OppId = "123";
            opentity.OpportunityAmount = (decimal)100.23;
            opentity.OpportunityName = "Test1";
            opentity.OpportunityStage = Constants.OpportunityStage.New;
            opentity.AccountGroup = "Group 1";
            opentity.AccountIseriesCode = "100";
            opentity.closeDate = new DateTime(2020, 02, 20);

            OpportunityRepository repo = new OpportunityRepository();
            string result = repo.Insert(opentity);
            Assert.IsTrue(!string.IsNullOrEmpty(result));
        }

        [TestMethod]
        public void OpportunityRepository_Update()
        {
            OpportunityEntity opentity = new OpportunityEntity();
            opentity.OppId = "123";
            opentity.OpportunityAmount = (decimal)100.23;
            opentity.OpportunityName = "Test1";
            opentity.OpportunityStage = Constants.OpportunityStage.New;
            opentity.AccountGroup = "Group 1";
            opentity.AccountIseriesCode = "100";
            opentity.closeDate = new DateTime(2020, 02, 20);

            OpportunityRepository repo = new OpportunityRepository();
            string result = repo.Update(opentity);
            Assert.IsTrue(!string.IsNullOrEmpty(result));
        }
    }
}
