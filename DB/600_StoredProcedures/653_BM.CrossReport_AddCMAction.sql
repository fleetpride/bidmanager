﻿USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gurpreet Singh
-- Create date: 08/16/19
-- Description:	add all CM action for a bid 
-- =============================================
CREATE
	OR

ALTER PROC [BM].[CrossReport_AddCMAction] (
	@BidId INT
	,@CMUserId NVARCHAR(100)
	,@Result INT = NULL OUTPUT
	)
AS
BEGIN

	declare @rank int
	select @rank = [Rank] from BM.BID where BidId = @BidId

	IF EXISTS (
			SELECT 1
			FROM FPCAPPS.BM.CMAction
			WHERE BidId = @BidId
				AND CMUserID = @CMUserId
			)
	BEGIN
		SET @Result = 1
	END
	ELSE
	BEGIN
		INSERT INTO FPCAPPS.BM.CMAction (
			BidId
			,CMUserId
			)
		VALUES (
			@BidId
			,@CMUserId
			)

		SET @Result = Scope_Identity()

		UPDATE BM.Bid
	SET [Rank] = 1, ModifiedOn = GETDATE()
	where BidId = @BidId

	UPDATE BM.Bid
				SET [Rank] = [Rank] + 1
				WHERE BidId <> @BidId
					AND [Rank] >= 1
					AND [Rank] < @rank
	END
END
