-- ================================================
-- Used t insert bid launch details
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manpreet Singh
-- Create date: 24 Aug, 2021
-- Description:	Used t insert bid launch details
-- =============================================
CREATE 
OR ALTER PROCEDURE BM.InsertBidLaunchDetails
	@bidId int,
	@launchedByUserId varchar(50)
AS
BEGIN
	declare @rank int
	IF @bidId > 0
		BEGIN
			select @rank = [Rank] from BM.BID where BidId = @bidId
			INSERT INTO BM.BidLaunchedDetails VALUES (@bidId, GETDATE(),@launchedByUserId)

			UPDATE BM.BID SET BidStatus = 'BidLaunched', ActualLaunchDate = GETDATE(), [Rank] = 1, ModifiedOn = GETDATE() where BidId = @bidId

			UPDATE BM.Bid
				SET [Rank] = [Rank] + 1
				WHERE BidId <> @bidId
					AND [Rank] >= 1
					AND [Rank] < @rank

			SELECT 1 AS 'RESULT'
		END
		ELSE
		BEGIN
			SELECT 0 AS 'RESULT'
		END
END
GO
