﻿USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================================
-- Author:		Nishant Mittal
-- Create date: 12/18/19
-- Description:	add all CM inventory review action for a bid 
-- ==============================================================
CREATE
	OR

ALTER PROC [BM].[Inventory_AddCMAction] (
	@BidId INT
	,@BidLaunchDate DATE = NULL
	,@CMUserId NVARCHAR(100)
	,@Result INT = NULL OUTPUT
	)
AS
BEGIN

	declare @rank int
	select @rank = [Rank] from BM.BID where BidId = @BidId
	--// update launch date of bid //--
	UPDATE BM.Bid
	SET LaunchDate = @BidLaunchDate
	, ModifiedOn = GETDATE()
	,[Rank] = 1
	WHERE BidId = @BidId


	UPDATE BM.Bid
				SET [Rank] = [Rank] + 1
				WHERE BidId <> @BidId
					AND [Rank] >= 1
					AND [Rank] < @rank

	IF EXISTS (
			SELECT 1
			FROM FPCAPPS.BM.InventoryReviewAction (nolock) 
			WHERE BidId = @BidId
				AND UserID = @CMUserId
			)
	BEGIN
		SET @Result = 1
	END
	ELSE
	BEGIN
		INSERT INTO FPCAPPS.BM.InventoryReviewAction (
			BidId
			,UserId
			,UserRole
			)
		VALUES (
			@BidId
			,@CMUserId
			,'CategoryManager'
			)

		UPDATE BM.Bid
				SET [Rank] = [Rank] + 1
				WHERE BidId <> @BidId
					AND [Rank] >= 1
					AND [Rank] < @rank

		SET @Result = Scope_Identity()
	END
END
