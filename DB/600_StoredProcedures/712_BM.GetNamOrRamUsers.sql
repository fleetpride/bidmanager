USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Manpreet Singh
-- Create date: 05/08/2022
-- Description:	Get NAM or RAM users
-- =============================================
CREATE OR ALTER PROCEDURE [BM].GetNamOrRamUsers (@userRole VARCHAR(50))
AS
BEGIN
	
	IF (@userRole = 'NAM')
	BEGIN
		SELECT DISTINCT   DS.UserId as UserId, DS.FirstName + ' ' + DS.LastName as FullName
		FROM    FPBIDW.EDW.DimSalesman        DS (NOLOCK)
				JOIN FPBIDW.EDW.DimCustomer DC (NOLOCK) ON    DS.SalesmanKey=DC.NatAccMngrKey  AND DC.IsNatAccount='Y'
				INNER JOIN FPBIDW.EDW.DimSalesTerritory DT WITH (NOLOCK) ON DS.SalesTerritoryKey = DT.SalesTerritoryKey 
				JOIN FPBIDW.EDW.DimEmployee DE (NOLOCK) ON DS.UserId = DE.UserName
				JOIN FPBIDW.EDW.DimRegion    DR (NOLOCK) ON    DS.RegionKey=DR.RegionKey
		RIGHT JOIN FPBIDW.EDW.DimLocation  DL (NoLOCK) ON DL.RegionKey = DR.RegionKey
		WHERE    DS.SalesmanStatus='A'
				
	END
	ELSE IF (@userRole = 'RAM')
	BEGIN
		select distinct uh.UserID as UserId, UPPER(e.FirstName + ' ' + e.LastName) as FullName from PEM.UserHierarchy uh 
		join FPBIDW_PRD.EDW.DimEmployee e on uh.UserID = e.UserName
		where uh.[ROLE] = @userRole
	END
END
