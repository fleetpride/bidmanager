USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Manpreet Singh
-- Create date: 18/04/2012
-- Description:	Get all the users in an email group
-- =============================================
-- =============================================

CREATE OR ALTER PROCEDURE [BM].[GetEmailIdFromGroup] (
	@groupType varchar(50)
	)
AS
BEGIN

	select STUFF((
				SELECT ',' + Email
				FROM fpcapps.bm.EmailGroup 
				where IsActive = 1 and GroupType = @groupType
				FOR XML PATH('')
				), 1, 1, '') as Email

END

