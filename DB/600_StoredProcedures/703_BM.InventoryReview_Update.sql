﻿USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vaqqas
-- Create date: 12/27/19
-- Description:	Save the approval of the parts
-- =============================================
CREATE OR ALTER PROCEDURE [BM].InventoryReview_Update (
	@CrossPartInventoryId int
	,@RevisedPool int
	,@RevisedPartNumber varchar(100)
	,@Comments varchar(2000)
	,@UserId varchar(100)
	,@Role varchar(100)
	)
AS
BEGIN
		declare @BidId int, @rank int

		UPDATE BM.CrossPartLineReview
		SET 
			RevisedPool = @RevisedPool
			,RevisedPartNumber = @RevisedPartNumber
			,LastUpdatedby = @UserId
			,LastUpdatedOn = getdate()
			,Comments = @Comments
		WHERE ID = @CrossPartInventoryId

		set @BidId = (select distinct top 1 BidId from BM.CrossPartLineReview where Id = @CrossPartInventoryId)
		select @rank = [Rank] from BM.Bid where BidId = @BidId

		UPDATE BM.Bid
		SET 
		ModifiedOn = GETDATE(),
		[Rank]=1
		WHERE BidID = @BidId

		
	UPDATE BM.Bid
				SET [Rank] = [Rank] + 1
				WHERE BidId <> @BidId
					AND [Rank] >= 1
					AND [Rank] < @rank

END
