USE FPCAPPS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manpreet Singh
-- Create date: 24Nov2021
-- Description:	Returns the email of NAM and creator of the bid
-- =============================================
CREATE OR ALTER PROCEDURE BM.Bid_GetNAMOrCreatorEMail
	@bidId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @namId varchar(100)
	declare @creatorId varchar(100)
	declare @emailIds varchar(max) = null

    IF @bidId > 0
	BEGIN
		select @namId = B.NamUserId, @creatorId = CreatedBy from BM.BID B where B.BidId = @bidId


		select EmployeeEmail as EmailId, UserName  from FPBIDW.EDW.DimEmployee where UserName in (@namId,@creatorId)
	END
	

END
GO
