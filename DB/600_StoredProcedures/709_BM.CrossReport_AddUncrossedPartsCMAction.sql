USE FPCAPPS

GO

CREATE 
OR ALTER PROCEDURE BM.CrossReport_AddUncrossedPartsCMAction
(
	@bidId int,
	@bidPartId int,
	@CMUserId varchar(100),
	@userId varchar(100),
	@Result int output
)
AS
BEGIN

	if exists (select 1 from BM.UncrossedPartCMAction where BidId = @bidId and BidPartId = @bidPartId)
	begin
		update BM.UncrossedPartCMAction set CMUserId = @CMUserId where BidId = @bidId and BidPartId = @bidPartId
		set @Result = 1
	end
	else
	begin
		insert into BM.UncrossedPartCMAction values (@bidId, @bidPartId, @CMUserId, 0, GETDATE(), @userId)
		set @Result = 1
	end

END
