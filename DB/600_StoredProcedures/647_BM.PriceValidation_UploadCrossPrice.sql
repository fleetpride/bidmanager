﻿USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Gurpreet Singh
-- Create date: 08/08/19
-- Description:	Upload Cross Price
-- =============================================
CREATE
	OR

ALTER PROC [BM].[PriceValidation_UploadCrossPrice] (
	@CrossPartsPrice AS BM.CrossPartsPriceUDT READONLY
	,@BidId INT
	)
AS
BEGIN
	UPDATE CP
	SET CP.PriceToQuote = CPP.PriceToQuote
		,CP.SuggestedPrice = CPP.SuggestedPrice
		,CP.ICOST = CPP.ICOST
		,CP.Margin = CPP.Margin
		,CP.AdjustedICost = CPP.AdjustedICOST
		,CP.AdjustedMargin = CPP.AdjustedMargin
	FROM FPCAPPS.BM.BidPart BP (nolock)
	INNER JOIN FPCAPPS.BM.CrossPart CP (nolock) ON BP.BidPartId = CP.BidPartId
	INNER JOIN @CrossPartsPrice CPP ON CP.BidPartId = CPP.BidPartId
		AND CP.CrossPartId = CPP.CrossPartId
	WHERE BidId = @BidId
		AND CP.FinalPreference in ('Primary', 'Alternate1', 'Alternate2')
		--- only CM verified parts or (Approved & Preferred parts can go to pricing)
		AND(  BP.IsVerified = 1
		     OR (UPPER(ltrim(rtrim(CP.VendorColorDefinition))) in ('Approved','Preferred'))

		   )
    --- Calculate pricing params on price update
	EXEC BM.CalcualtePricingParams @BidId
END
GO


