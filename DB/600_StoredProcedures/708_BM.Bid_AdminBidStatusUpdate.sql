/****** Script for SelectTopNRows command from SSMS  ******/
USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Manpreet Singh
-- Create date: 01/04/21
-- Description:	update Bid status for admin (mark invalid) 
-- =============================================
CREATE
	OR

ALTER PROCEDURE BM.Bid_AdminBidStatusUpdate (
	@bidId INT
	,@oldStatus varchar(100)
	,@newStatus varchar(100)
	
	)
AS
BEGIN
	declare @rank int
	select @rank = [Rank] from BM.BID where BidId = @bidId

	IF (@bidId > 0 AND @newStatus = 'Invalid')
	BEGIN
		UPDATE BM.BID  set BidStatus = @newStatus where BidId = @bidId
	END
	ELSE IF (@bidId > 0 AND @newStatus = 'Won' AND @oldStatus = 'InventoryUpdate')
	BEGIN
		UPDATE BM.BID  set BidStatus = @newStatus where BidId = @bidId

		UPDATE BM.Bid
		SET [Rank] = 1, ModifiedOn = GETDATE()
		where BidId = @BidId

		UPDATE BM.Bid
					SET [Rank] = [Rank] + 1
					WHERE BidId <> @BidId
						AND [Rank] >= 1
						AND [Rank] < @rank
	END
END

