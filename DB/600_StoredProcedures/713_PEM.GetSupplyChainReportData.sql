USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Manpreet Singh
-- Create date: 07/09/22
-- Description:	Get all won parts inventory list
-- =============================================
CREATE or ALTER PROCEDURE [BM].[GetSupplyChainReportData] (
	@BidId INT
	)
AS
BEGIN
	SELECT BP.BidPartId
		,BP.CustomerPartNumber
		,BP.ManufacturerPartNumber
		,BP.PartDescription
		,CP.CrossPartId
		,CP.PoolNumber AS CrossPoolNumber
		,CP.PartNumber AS CrossPartNumber
		,CP.PartDescription AS CrossPartDescription
		,CPI.[Location]
		,CPI.StockingType
		,CPI.CustPartUOM
		,CPI.EstimatedAnnualUsage
		,CPI.NewPool
		,CPI.NewPartNo
		,CPI.NewStockingType
		,CPI.NewPrice
		,CPI.IsActive		
		,cplr.RevisedPool
		,cplr.RevisedPartNumber as RevisedPartNo
		,cplr.Comments as Comments
		,cplr.[ProjectedL12GP] as ProjectedL12GP
	FROM FPCAPPS.BM.BidPart BP (nolock) 
	LEFT JOIN FPCAPPS.BM.CrossPart CP  (nolock) ON BP.BidPartId = CP.BidPartId
	LEFT JOIN FPCAPPS.BM.CrossPartInventory CPI  (nolock) ON CP.CrossPartId = CPI.CrossPartId
	left join FPCAPPS.BM.CrossPartLineReview cplr  (nolock) on CPI.CrossPartId = cplr.CrossPartId
	WHERE BP.BidId = @BidId
		AND CP.IsWon = 1
END
GO


