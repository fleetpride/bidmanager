﻿USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vaqqas
-- Create date: 7/12/2019
-- Description:	Submit the bid for crowd sourcing
-- =============================================
CREATE OR ALTER PROCEDURE [BM].[Bid_Submit] (
	@BidId int,
	@NewStatus varchar(50),
	@PhaseDueDate datetime,
	@CalculatedDueDate datetime,
	@Comments nvarchar(max)
	)
AS
BEGIN

	declare @rank int
	select @rank = [Rank] from BM.BID where BidId = @BidId

	UPDATE BM.Bid
	SET BidStatus = @NewStatus,
	CurrentPhaseDueDate = @PhaseDueDate,
	CalculatedDueDate = @CalculatedDueDate,
	Comments = Comments + char(10) + @Comments,
	[ModifiedOn] = GETDATE()
	WHERE BidId = @BidId;

	UPDATE BM.Bid
	SET [Rank] = 1, ModifiedOn = GETDATE()
	where BidId = @BidId

	UPDATE BM.Bid
				SET [Rank] = [Rank] + 1
				WHERE BidId <> @BidId
					AND [Rank] >= 1
					AND [Rank] < @rank
END
