USE FPCAPPS

GO

CREATE 
	OR ALTER PROCEDURE BM.GetCMListForUncrossedParts

AS 
BEGIN

	select distinct dp.[categorymanager] as userId, de.EmployeeName as FullName, de.EmployeeEmail from FPBIDW.EDW.DimProduct dp join
  FPBIDW.EDW.DimEmployee de on dp.categorymanager = de.username where de.EmploymentStatus = 'A' and dp.[categorymanager] not in ('EMOORE','SWING')

  union

	select de.username as userId, de.EmployeeName as FullName, de.EmployeeEmail from FPBIDW.EDW.DimEmployee de where de.UserName = 'MKELLER'

END