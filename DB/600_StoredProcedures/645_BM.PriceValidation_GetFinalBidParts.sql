﻿USE [FPCAPPS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================  
-- Author:  Gurpreet Singh   
-- Create date: 08/08/19  
-- Description: Get all final bid part details  
-- =============================================  
CREATE  
   OR ALTER
  
 PROCEDURE [BM].[PriceValidation_GetFinalBidParts] (  
 @BidId INT  
 ,@UserId NVARCHAR(100) = NULL  
 ,@UserRole VARCHAR(50) = NULL  
 )  
AS  
BEGIN  
  
 declare @bidStatus varchar(50)  
 declare @CMIds table  
 (  
  CMUserId varchar(50)  
 )  
  
 select @bidStatus = BidStatus from BM.BID where BidId = @BidId  
  
 
 IF (@bidStatus = 'CategoryManager')  
 BEGIN  
  insert into @CMIds  
  select cma.CMUserId  from BM.CMAction cma (nolock)  
  where BidId = @BidId and cma.CMActionStatus = 1  
  
  SELECT distinct BP.BidPartId  
  ,BP.BidId  
  ,BP.CustomerPartNumber  
  ,BP.PartDescription  
  ,BP.Manufacturer  
  ,BP.ManufacturerPartNumber  
  ,BP.Note  
  ,BP.EstimatedAnnualUsage  
  ,BP.IsFinalized  
  ,BP.CreatedBy  
  ,BP.CreatedOn  
 FROM BM.BidPart BP  (nolock)  
 INNER JOIN BM.CrossPart CP  (nolock) ON BP.BidPartId = CP.BidPartId    
   INNER JOIN FPBIDW.EDW.DimProduct DP  (nolock)  ON CP.PoolNumber = DP.Pool    
    AND CP.PartNumber = DP.PartNumber  
 INNER JOIN FPBIDW.EDW.DimEmployee DE  (nolock) ON DP.CategoryManager = DE.UserName    
 WHERE BP.BidId = @BidId  
 AND IsFinalized = 1 and  dp.CategoryManager in (select CMUserId  from @CMIds)  
 END  
 ELSE
 BEGIN  
  SELECT BidPartId  
   ,BidId  
   ,CustomerPartNumber  
   ,PartDescription  
   ,Manufacturer  
   ,ManufacturerPartNumber  
   ,Note  
   ,EstimatedAnnualUsage  
   ,IsFinalized  
   ,CreatedBy  
   ,CreatedOn  
  FROM BM.BidPart  (nolock)  
  WHERE BidId = @BidId  
  AND IsFinalized = 1  
 END  
END