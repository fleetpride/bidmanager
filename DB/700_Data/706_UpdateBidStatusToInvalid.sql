use FPCAPPS

GO

declare @bids VARCHAR(MAX) = 'SAIA top 100,PEPSICO_MARKETBASKET RFQ_4-30-2021,PEPSICO_AIR DRUM AND SYSTEM_RFQ_04-30-2021',  -- comma separated bids
@delimiter VARCHAR(100) = ','
declare @bidsToMarkInvalid TABLE (SNo int identity(1,1), Bids VARCHAR(MAX), MarkedInvalid bit) 

-- create table from comma separated bids

DECLARE @xml XML  
SET @xml = '<t>' + REPLACE(@bids,@delimiter,'</t><t>') + '</t>'  
INSERT INTO @bidsToMarkInvalid(Bids, MarkedInvalid)  
SELECT LTRIM(RTRIM( r.value('.','varchar(MAX)'))) , 0
FROM @xml.nodes('/t') as records(r) 

--select * from @bidsToMarkInvalid
--select * from BM.BID where BM.BID.BidName in (select Bids from @bidsToMarkInvalid)
-- select * from BM.Bid where Bidname like '%PEPSICO_AIR%'


declare @counter int = 1
declare @recordCount int = (select max(SNo) from @bidsToMarkInvalid)

--check and update status one by one 
--comment out the first update statement to check for the expected result. It won't update the main table. Uncomment it to mark status invalid.

	WHILE(@counter <= @recordCount)
	BEGIN
		IF EXISTS (select 1 from BM.BID where BM.BID.BidName = (select b.Bids from @bidsToMarkInvalid b where b.SNo = @counter))
		BEGIN
			UPDATE BM.BID SET BidStatus = 'Invalid' where BidName = (select b.Bids from @bidsToMarkInvalid b where b.SNo = @counter)
			UPDATE @bidsToMarkInvalid set MarkedInvalid = 1 where SNo = @counter
			
		END
		set @counter = @counter + 1

	END

	-- Returns Invalid Names
	IF EXISTS (select 1 from @bidsToMarkInvalid where MarkedInvalid = 0)
	BEGIN
		select Bids as InvalidBids from @bidsToMarkInvalid where MarkedInvalid = 0
	END
	ELSE
		PRINT 'BIDS STATUS UPDATED SUCCESSFULLY'

