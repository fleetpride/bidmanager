USE FPCAPPS
GO

Alter table BM.Bid
	Add ModifiedBy varchar(150) null, ModifiedOn DateTime null


GO

Update BM.Bid set ModifiedBy = CreatedBy, ModifiedOn = CreatedOn