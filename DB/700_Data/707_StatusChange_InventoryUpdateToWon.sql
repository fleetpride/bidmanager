USE FPCAPPS

GO

declare @bidName varchar(5000) = 'KRD_Haldex shoe kits_04/23/21'


if exists (select 1 from BM.BID where BidName = @bidName)
begin
	--select * from BM.BID where BidName = @bidName

	update BM.Bid set BidStatus='Won' where BidName = @bidName
end
