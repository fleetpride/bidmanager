USE [FPCAPPS]
GO

/****** Object:  Table [BM].[BidLaunchedDetails]    Script Date: 24-08-2021 17:04:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [BM].[BidLaunchedDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BidId] [int] NOT NULL,
	[ActualBidLaunchedDate] [datetime] NOT NULL,
	[LaunchedByUserId] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [BM].[BidLaunchedDetails]  WITH CHECK ADD  CONSTRAINT [FK_BidLaunchedDetails_BID] FOREIGN KEY([BidId])
REFERENCES [BM].[BID] ([BidId])
GO

ALTER TABLE [BM].[BidLaunchedDetails] CHECK CONSTRAINT [FK_BidLaunchedDetails_BID]
GO


