USE [FPCAPPS]
GO

/****** Object:  Table [BM].[CMAction]    Script Date: 14-02-2022 20:10:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [BM].[UncrossedPartCMAction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BidId] [int] NOT NULL,
	[BidPartId] int NOT NULL,
	[CMUserId] [nvarchar](100) NOT NULL,
	[CMActionStatus] [bit] NULL,
	[CMActionOn] [datetime] NULL,
	[UpdatedBy] varchar(100)
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

