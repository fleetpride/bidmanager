﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using System.Configuration;

namespace FleetPride.BidManager.TatMonitor
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailHelper emailHelper = new EmailHelper();
            BidRepository bidRepo = new BidRepository();
            Logger.QuickLog("BidManagement TAT Monitor job started running");
            List<BidEmailEntity> bees = bidRepo.GetTatMissingBids();
            emailHelper.SendTATMissNotification(bees);
        }
    }
}
