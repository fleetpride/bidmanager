﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace FleetPride.BidManager.App_Start
{
    public class AjaxData
    {
        public string data { get; set; }
    }

    public class JsonActionFilterAttribute : ActionFilterAttribute
    {
        public string Parameter { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.ContentType.Contains("application/text"))
            {
                string inputContent;
                using (var sr = new StreamReader(filterContext.HttpContext.Request.InputStream))
                {
                    inputContent = sr.ReadToEnd();
                }

                try
                {
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    jsonSerializer.MaxJsonLength = Int32.MaxValue;
                    //var payload = jsonSerializer.Deserialize<AjaxData>(inputContent);
                    var selectedData = jsonSerializer.Deserialize(inputContent, JsonDataType);
                    filterContext.ActionParameters[Parameter] = selectedData;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}