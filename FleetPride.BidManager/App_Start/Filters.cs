﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FleetPride.BidManager.App_Start
{
    public sealed class ExceptionFilter : IExceptionFilter
    {
        void IExceptionFilter.OnException(ExceptionContext exceptionFilterContext)
        {
            ILogger logger = new Logger();
            logger.Exception(exceptionFilterContext.Exception);

            string errorMessage = exceptionFilterContext.Exception.Message.ToString();
            if (errorMessage == "Sessiontimeout" || errorMessage == "SessionEmpty")
            {
                errorMessage = "Session timeout. Please go back to home page.";
            }

            if (exceptionFilterContext.RequestContext.HttpContext.Request.Headers["X-Requested-With"] != null)
            {
                exceptionFilterContext.ExceptionHandled = true;
                exceptionFilterContext.HttpContext.Response.Clear();
                exceptionFilterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                exceptionFilterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                exceptionFilterContext.Result =
                new JsonResult
                {
                    Data = errorMessage,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else if (exceptionFilterContext.Exception.Message != "Server cannot set content type after HTTP headers have been sent.")
            {
                exceptionFilterContext.ExceptionHandled = true;
                exceptionFilterContext.HttpContext.Response.Clear();
                exceptionFilterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                exceptionFilterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                var viewData = new ViewDataDictionary();
                viewData.Add("Error", errorMessage);
                exceptionFilterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Shared/Error.cshtml",
                    ViewData = viewData
                };
            }
        }
    }

    public class AuthorizationFilter : IAuthorizationFilter
    {
        private AuthorizationRepository _authHelper;
        public AuthorizationFilter()
        {
            _authHelper = new AuthorizationRepository();
        }

        public void OnAuthorization(AuthorizationContext authorizationFilterContext)
        {
            //var descriptor = authorizationFilterContext.ActionDescriptor;
            //var actionName = descriptor.ActionName;
            bool roleFound = false;
            System.Security.Principal.IPrincipal iuser = System.Web.HttpContext.Current.User;
            string userName = iuser.Identity.Name.Split('\\')[1];
            string[] userRoles = { "" };
            userRoles = new string[] { "CrossEntryUser" };
            HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity(userName), userRoles);

            // first check if user exist in delegate table
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["CheckUserDetailsFromDBAllowed"]))
            {
                UserEntity user;
                user = _authHelper.GetUserDetailsFromDB(HttpContext.Current.User.Identity.Name);
                if (user.UserId != null)
                {
                    roleFound = true;
                    HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(user.UserId), new string[] { user.UserRole });
                }
            }

            // check if user exist in any of the AD group if not found in delegate table
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["CheckUserDetailsFromADGroupAllowed"]))
            {
                if (!roleFound)
                {
                    roleFound = CheckUserInSuperUserADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInNAMSupervisorADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInLocalUserADGroup();  //Renamed from CheckUserInRegionalUserADGroup to CheckUserInLocalUserADGroup
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInNAMADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInNAMValidatorADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInPricingTeamADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInCategoryManagerADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInCrossValidatorADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInCategoryDirectorADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInCategoryVPADGroup ();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInDemandTeamADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInDataTeamADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInRAMADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInRAMSupervisorADGroup();
                }
                if (!roleFound)
                {
                    roleFound = CheckUserInRAMValidatorADGroup();
                }
            }
        }

        public bool CheckUserInRAMADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupRAM"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "RAM" });
                return true;
            }
            return false;
        }
        public bool CheckUserInRAMSupervisorADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupRAMSupervisor"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "RAMSupervisor" });
                return true;
            }
            return false;
        }
        public bool CheckUserInRAMValidatorADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupRAMValidator"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "RAMValidator" });
                return true;
            }
            return false;
            
        }

        public bool CheckUserInSuperUserADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupSuperUser"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "SuperUser" });
                return true;
            }
            return false;
        }

        public bool CheckUserInLocalUserADGroup()
        {

            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupLocalUser"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "LocalUser" });
                return true;
            }
            return false;
        }

        public bool CheckUserInNAMSupervisorADGroup()
        {
            
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupNAMSupervisor"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "NAMSupervisor" });
                return true;
            }
            return false;
        }

        public bool CheckUserInNAMValidatorADGroup()
        {
            
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupNAMValidator"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "NAMValidator" });
                return true;
            }
            return false;
        }

        public bool CheckUserInNAMADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupNAM"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "NAM" });
                return true;
            }
            return false;
        }

        public bool CheckUserInPricingTeamADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupPricingTeam"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "PricingTeam" });
                return true;
            }
            return false;
        }

        public bool CheckUserInCategoryManagerADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupCategoryManager"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "CategoryManager" });
                return true;
            }
            return false;
        }

        public bool CheckUserInCrossValidatorADGroup()
        {
            
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupCrossValidator"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { "CrossValidator" });
                return true;
            }
            return false;
        }

        public bool CheckUserInCategoryDirectorADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupCategoryDirector"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { UserRole.CategoryDirector.ToString() });
                return true;
            }
            return false;
        }

        public bool CheckUserInCategoryVPADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupCategoryVP"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { UserRole.CategoryVP.ToString() });
                return true;
            }
            return false;
        }

        public bool CheckUserInDataTeamADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupDataTeam"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { UserRole.DataTeam.ToString() });
                return true;
            }
            return false;
        }

        public bool CheckUserInDemandTeamADGroup()
        {
            if (_authHelper.IsUserInADGroup(HttpContext.Current.User.Identity.Name, ConfigurationManager.AppSettings["ADGroupDeamndTeam"].ToString()))
            {
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                      new System.Security.Principal.GenericIdentity(HttpContext.Current.User.Identity.Name), new string[] { UserRole.DemandTeam.ToString() });
                return true;
            }
            return false;
        }

    }
}
