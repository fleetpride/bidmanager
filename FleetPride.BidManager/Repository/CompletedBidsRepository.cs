﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetPride.BidManager.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using FleetPride.BidManager.Utilities;

namespace FleetPride.BidManager.Repository
{
    public class CompletedBidsRepository : ICompletedBidsRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        public List<BidEntity> GetAllBids(string userRole, string userId)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CompletedBids_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = BidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        /// <summary>
        /// Method to mark won cross part in DB
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="bidPartId"></param>
        /// <param name="crossPartId"></param>
        /// <returns></returns>
        public int MarkWonSelectedCross(int bidId, int bidPartId, int crossPartId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CompletedBids_MarkWonSelectedCross", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.BigInt)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.BigInt)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartId", SqlDbType.BigInt)).Value = crossPartId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        /// <summary>
        /// Mark All Primary Won in DB
        /// </summary>
        /// <param name="bidId"></param>
        public void MarkAllPrimaryWon(int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CompletedBids_MarkAllPrimaryWon", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Export crosses on completed bids page from DB
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public DataSet ExportFinalCrosses(int bidId)
        {
            DataSet crosses = new DataSet();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CompletedBids_DownloadCrosses", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(crosses);
                    }
                }
            }
            return crosses;
        }

        /// <summary>
        /// Method to Insert uploaded part marl won data in bulk in database
        /// </summary>
        /// <param name="partMarkWonData"></param>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public int BulkInsertPartMarkWonData(DataTable partMarkWonData, int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (var cmd = new SqlCommand("BM.CompletedBids_UploadPartMarkWon", conn))
                {
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@PartsMarkWon", SqlDbType.Structured)).Value = partMarkWonData;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return 1;
        }

        public BidEntity BidEntity(IDataReader rdr)
        {
            BidEntity record = new BidEntity();
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);

            //set the Bid status
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;

            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
            record.CalculatedDueDate = Convert.ToString(rdr["CalculatedDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CalculatedDueDate"]).ToString("MM/dd/yyyy");
            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
            record.Rank = Convert.ToString(rdr["Rank"]);
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.RequestType = Convert.ToString(rdr["RequestType"]);
            record.SubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("MM/dd/yyyy");
            record.FPFacingLocations = Convert.ToString(rdr["FPFacingLocations"]);
            record.TotalValueOfBid = Convert.ToString(rdr["TotalValueOfBid"]);
            record.IsFPPriceHold = Convert.ToBoolean(rdr["IsFPPriceHold"]);
            record.HoldDate = Convert.ToString(rdr["HoldDate"]) == "" ? "" : Convert.ToDateTime(rdr["HoldDate"]).ToString("MM/dd/yyyy");
            record.IsCustomerAllowSubstitutions = Convert.ToBoolean(rdr["IsCustomerAllowSubstitutions"]);
            record.BrandPreference = Convert.ToString(rdr["BrandPreference"]);
            record.IsCustomerInventoryPurchaseGuarantee = Convert.ToBoolean(rdr["IsCustomerInventoryPurchaseGuarantee"]);
            record.LevelOfInvetoryDetailCustomerCanProvide = Convert.ToString(rdr["LevelOfInvetoryDetailCustomerCanProvide"]);
            record.SourceExcelFileName = Convert.ToString(rdr["SourceExcelFileName"]);
            record.CreatedByRole = Convert.ToString(rdr["CreatedByRole"]);
            record.NAMUserID = Convert.ToString(rdr["NAMUserID"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);

            return record;
        }

        public DataSet GetSupplyChainData(int bidId, string userId, string userRole)
        {
            DataSet ds = new DataSet();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BM].[GetSupplyChainReportData]", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
            }
            var result = GetSupplyChainReportColumn(ds);
            return result;
        }

        private DataSet GetSupplyChainReportColumn(DataSet ds)
        {
            var data = new DataSet();
            var tbl = new DataTable("SupplyChain");
            tbl.Columns.Add("Customer Part Number");
            tbl.Columns.Add("Manufacturer Part Number");
            tbl.Columns.Add("Part Description");
            tbl.Columns.Add("FP Pool");
            tbl.Columns.Add("FP Part No");
            tbl.Columns.Add("FP Part Description");
            tbl.Columns.Add("Location");
            tbl.Columns.Add("Stocking Type");
            tbl.Columns.Add("Estimated Annual Usage");
            tbl.Columns.Add("ProjectedGP");
            tbl.Columns.Add("Cust Part UOM");
            tbl.Columns.Add("New Pool");
            tbl.Columns.Add("New Part No");
            tbl.Columns.Add("New Price");
            tbl.Columns.Add("IsActive");

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                var row = tbl.NewRow();
                row["Customer Part Number"] = item["CustomerPartNumber"];
                row["Manufacturer Part Number"] = item["ManufacturerPartNumber"];
                row["Part Description"] = item["PartDescription"];
                row["FP Pool"] = item["CrossPoolNumber"];
                row["FP Part No"] = item["CrossPartNumber"];
                row["FP Part Description"] = item["CrossPartDescription"];
                row["Location"] = item["Location"];
                row["Estimated Annual Usage"] = item["EstimatedAnnualUsage"];
                row["Cust Part UOM"] = item["CustPartUOM"];
                row["New Pool"] = item["NewPool"];
                row["New Part No"] = item["NewPartNo"];
                row["New Price"] = item["NewPrice"];
                row["IsActive"] = item["IsActive"];
                row["ProjectedGP"] = item["ProjectedL12GP"];
                row["Stocking Type"] = item["StockingType"];
                tbl.Rows.Add(row);
            }
            data.Merge(tbl);

            return data;
        }
    }
}