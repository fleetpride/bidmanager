﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public interface IOpportunityRepository
    {
        string Insert(OpportunityEntity entity);
        string Update(OpportunityEntity entity);
    }
}