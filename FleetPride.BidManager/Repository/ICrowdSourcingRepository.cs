﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace FleetPride.BidManager.Repository
{
    public interface ICrowdSourcingRepository
    {
        List<int> GetCrossEntryUserCounts(string userId);

        int CheckLockedEntryPartForUser(string userId);

        BidPartEntity GetPartForCrowdSourcing(string userId);

        List<CrossEntity> GetCrossesForEntry(int bidPartId);

        List<CrossEntity> CheckPartNumber(string partNumber, int bidId, bool matchPartial);

        List<Part> GetAllPartInfo(int bidId, string userRole, string userId);

        int SaveDbFoundPoolPart(int crowdSourcingPartId, string partNo, bool ssflip, List<int> pools,List<int> productKeys,  int isCrossReport, string source, string userId);

        void SaveSearchedDbFoundPoolPart(int crowdSourcingPartId, int isCrossReport, string source, List<Part> parts, string userId);

        int SaveManualPoolPart(int crowdSourcingPartId, string partNo, int? poolNo, string description, string category, int isCrossReport, string userId);

        void SubmitCrowdSourcingPart(int crowdSourcingPartId, string userId);

        int ReleaseCrowdSourcingPart(int crowdSourcingPartId, string userId);

        void DeleteCross(int crossPartId);

        List<int> GetCrossValidatorCounts(string userId);

        int CheckLockedValidationPartForUser(string userId);

        BidPartEntity GetPartForValidation(string userId);

        List<CrossEntity> GetCrossesForValidation(int crossValidationPartId, string userId);

        int UpdateCrossValidationStatus(int crossPartId, string status, string userId);

        void SubmitValidatedPart(int validatedCrossPartId, string userId);
    }
}

