﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetPride.BidManager.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using FleetPride.BidManager.Utilities;

namespace FleetPride.BidManager.Repository
{
    public class CrossRepository : ICrossRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        public List<CMUserEntity> GetCMList(int bidId)
        {
            List<CMUserEntity> lstCM = new List<CMUserEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_GetCMForBid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.CommandTimeout = 300;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            CMUserEntity crossReport = new CMUserEntity();
                            crossReport.CMName = Convert.ToString(rdr["Name"]);
                            crossReport.CMUserId = Convert.ToString(rdr["UserId"]);
                            crossReport.CMEmail = Convert.ToString(rdr["Email"]);
                            lstCM.Add(crossReport);
                        }
                    }
                }
            }

            return lstCM;
        }

        public List<BidEntity> GetAllBids(string userRole, string userId)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = CreateBidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        public List<BidEntity> GetAllBidsForDropDown(string userRole, string userId)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_GetBids_WithUncrossedParts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = CreateBidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        public BidEntity CreateBidEntity(IDataReader rdr)
        {
            BidEntity record = new BidEntity();
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);

            //set the Bid status
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;

            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
            record.Rank = Convert.ToString(rdr["Rank"]);
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.RequestType = Convert.ToString(rdr["RequestType"]);
            record.SubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("MM/dd/yyyy");
            record.FPFacingLocations = Convert.ToString(rdr["FPFacingLocations"]);
            record.TotalValueOfBid = Convert.ToString(rdr["TotalValueOfBid"]);
            record.IsFPPriceHold = Convert.ToBoolean(rdr["IsFPPriceHold"]);
            record.HoldDate = Convert.ToString(rdr["HoldDate"]) == "" ? "" : Convert.ToDateTime(rdr["HoldDate"]).ToString("MM/dd/yyyy");
            record.IsCustomerAllowSubstitutions = Convert.ToBoolean(rdr["IsCustomerAllowSubstitutions"]);
            record.BrandPreference = Convert.ToString(rdr["BrandPreference"]);
            record.IsCustomerInventoryPurchaseGuarantee = Convert.ToBoolean(rdr["IsCustomerInventoryPurchaseGuarantee"]);
            record.LevelOfInvetoryDetailCustomerCanProvide = Convert.ToString(rdr["LevelOfInvetoryDetailCustomerCanProvide"]);
            record.SourceExcelFileName = Convert.ToString(rdr["SourceExcelFileName"]);
            record.CreatedByRole = Convert.ToString(rdr["CreatedByRole"]);
            record.NAMUserID = Convert.ToString(rdr["NAMUserID"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);

            return record;
        }

        public List<CrossEntity> GetAllCrosses(int bidId)
        {
            List<CrossEntity> crosses = new List<CrossEntity>();
            
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_GetAllCrosses", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    IDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                    dataAdapter.Fill(ds);

                    for(int i= 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        crosses.Add(new CrossEntity(ds.Tables[0].Rows[i]));
                    }
                    

                }
            }
            return crosses;

        }

        public List<CrossEntity> GetCrosses(int bidPartID, bool isCrossReport)
        {
            List<CrossEntity> crosses = new List<CrossEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.AvailableCrosses_GetCrosses", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartID;
                    cmd.Parameters.Add(new SqlParameter("@IsCrossReport", SqlDbType.Bit)).Value = isCrossReport;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            crosses.Add(new CrossEntity(rdr));
                        }
                    }
                }
            }

            return crosses;
        }

        public void SaveAll(string role, List<CrosReportUpdates> updates)
        {
            DataTable dt = new DataTable("CrossReportUpdatesUDT");
            dt.Columns.Add(new DataColumn("BidPartId", typeof(int)));
            dt.Columns.Add(new DataColumn("CrossPartId", typeof(int)));
            dt.Columns.Add(new DataColumn("FinalPreference", typeof(string)));
            dt.Columns.Add(new DataColumn("IsVerified", typeof(bool)));

            foreach(var update in updates)
            {
                DataRow dr = dt.NewRow();
                dr["BidPartId"] = update.BidPartId;
                dr["CrossPartId"] = update.CrossPartId;
                dr["FinalPreference"] = update.FinalPreference;
                dr["IsVerified"] = update.IsVerified;
                dt.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_SaveAll", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@FinalizedCrossesUDT", SqlDbType.Structured)).Value = dt;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = System.Web.HttpContext.Current.User.Identity.Name;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.NVarChar)).Value = role;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int FinalizeSelectedCross(string userRole,List<FinalCrossEntity> records, int bidId)
        {
           
            int rowAffected = 0;
            foreach (var i in records)
            {
                using (SqlConnection conn = new SqlConnection(_bmConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand("BM.CrossReport_FinalizeSelectedCross", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = i.BidPartId;
                        cmd.Parameters.Add(new SqlParameter("@CrossPartId", SqlDbType.Int)).Value = i.CrossPartId;
                        cmd.Parameters.Add(new SqlParameter("@FinalPreference", SqlDbType.NVarChar)).Value = i.FinalPreference;
                        cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = System.Web.HttpContext.Current.User.Identity.Name;
                        cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.NVarChar)).Value = userRole;
                        cmd.Parameters.Add(new SqlParameter("@bidId", SqlDbType.Int)).Value = bidId;
                        cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                        conn.Open();
                        cmd.ExecuteScalar();

                        rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    }
                }
            }
            if (rowAffected > 0)
            {
                return rowAffected;
            }
            else
            {
                return 0;
            }
        }

        public int DeclineSelectedCross(int bidPartId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_DeclineSelectedCross", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = System.Web.HttpContext.Current.User.Identity.Name;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();

                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;

                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public DataSet DownloadCrosses(int bidId)
        {
            DataSet dsToFill = new DataSet();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_DownloadCrosses", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dsToFill);
                    }
                }
            }
            return dsToFill;
        }

        public DataSet DownloadPartsWithoutCross(int bidId)
        {
            DataSet dsToFill = new DataSet();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_PartsWithoutCross", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dsToFill);
                    }
                }
            }
            return dsToFill;
        }

        public List<CrossEntity> GetFinalCrosses(int bidId)
        {
            throw new NotImplementedException();
        }

        public int AddCMAction(List<CMUserEntity> dataToPost, int bidId)
        {
            int rowAffected = 0;

            foreach (var cm in dataToPost)
            {
                using (SqlConnection conn = new SqlConnection(_bmConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("BM.CrossReport_AddCMAction", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                        cmd.Parameters.Add(new SqlParameter("@CMUserId", SqlDbType.NVarChar)).Value = cm.CMUserId;
                        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                        conn.Open();
                        cmd.ExecuteScalar();

                        rowAffected = (int)cmd.Parameters["@Result"].Value;
                    }
                }
            }
            return rowAffected;
        }

        public int UpdateCMAction(int bidId, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_UpdateCMAction", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@CMUserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();

                    rowAffected = (int)cmd.Parameters["@Result"].Value;
                }
            }
            return rowAffected;
        }

        public int GetCMActionStatusForBid(int bidId, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_GetCMActionStatusForBid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@CMUserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();

                    rowAffected = (int)cmd.Parameters["@Result"].Value;
                }
            }
            return rowAffected;
        }

        public int UpdateBidPartVerificationAction(int bidPartId, bool isVerified, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_UpdateBidPartVerificationAction", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@IsVerified", SqlDbType.Bit)).Value = isVerified;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();

                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;

                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public int UpdateBidPartNote(int bidPartId, string note, string userId)
        {
            int rowAffected = 0;
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_UpdateBidPartNote", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@Note", SqlDbType.NVarChar)).Value = note;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                        return rowAffected;                    
                    else
                        return 0;
                }
            }
        }

        public List<string> PartsWithoutCross(int bidId)
        {
            List<string> lstPartsWithoutCross = new List<string>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_PartsWithoutCross", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        { 
                            lstPartsWithoutCross.Add(Convert.ToString(rdr["Parts Without Crosses"]));
                        }
                    }
                }
            }

            return lstPartsWithoutCross;
        }

        public List<CMUserEntity> GetCMForUncrossedParts()
        {
            List<CMUserEntity> lst = new List<CMUserEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.GetCMListForUncrossedParts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lst.Add(new CMUserEntity
                            {
                                CMUserId = Convert.ToString(rdr["userId"]),
                                CMName = Convert.ToString(rdr["FullName"]),
                                CMEmail = Convert.ToString(rdr["EmployeeEmail"])
                            });
                        }
                    }
                }
            }

            return lst;
        }

        public int AddUncrossedPartsCMAction(string cmUserId, int bidId, int bidPartId, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossReport_AddUncrossedPartsCMAction", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@bidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@bidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@CMUserId", SqlDbType.NVarChar)).Value = cmUserId;
                    cmd.Parameters.Add(new SqlParameter("@userId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();

                    rowAffected = (int)cmd.Parameters["@Result"].Value;
                }
            }
            return rowAffected;
        }
    }
}