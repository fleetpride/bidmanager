﻿using System.Web;

namespace FleetPride.BidManager.Repository
{
    public interface IExcelRepository
    {
        string GetExcel(string fileName, string localImagesFolder);

        void SaveExcel(HttpPostedFileBase file, string peEntryId, string localImagesFolder);

        void ClearCache(string localImagesFolder);
    }
}