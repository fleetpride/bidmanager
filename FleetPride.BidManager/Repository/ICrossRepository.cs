﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FleetPride.BidManager.Models;
using static FleetPride.BidManager.Models.CrossEntity;

namespace FleetPride.BidManager.Repository
{
    public interface ICrossRepository
    {
        List<CrossEntity> GetCrosses(int bidPartID, bool isCrossReport);
        List<CMUserEntity> GetCMList(int bidId);
        int FinalizeSelectedCross(string userRole,List<FinalCrossEntity> records, int bidId);
        int DeclineSelectedCross(int bidPartId);
        DataSet DownloadCrosses(int bidId);
        DataSet DownloadPartsWithoutCross(int bidId);        
        List<BidEntity> GetAllBids(string userRole, string userId);
        List<BidEntity> GetAllBidsForDropDown(string userRole, string userId);

        List<CrossEntity> GetFinalCrosses(int bidId);
        int AddCMAction(List<CMUserEntity> dataToPost, int bidId);
        int UpdateCMAction(int bidId, string userId);
        int GetCMActionStatusForBid(int bidId, string name);

        List<CrossEntity> GetAllCrosses(int bidId);
        int UpdateBidPartVerificationAction(int bidPartId, bool isVerified, string userId);

        int UpdateBidPartNote(int bidPartId, string note, string userId);

        List<string> PartsWithoutCross(int bidId);
        void SaveAll(string role, List<CrosReportUpdates> updates);
        List<CMUserEntity> GetCMForUncrossedParts();
        int AddUncrossedPartsCMAction(string cmUserId, int bidId, int bidPartId, string userId);
    }
}
