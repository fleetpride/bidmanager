﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FleetPride.BidManager.Models;
using static FleetPride.BidManager.Models.CrossEntity;

namespace FleetPride.BidManager.Repository
{
    public interface IPriceValidationRepository
    {
        List<BidEntity> GetAllBids(string userRole, string userId);
        List<BidPartEntity> GetFinalBidParts(int bidId, string userId, string userRole);
        List<CrossEntity> GetFinalCrosses(int bidPartId);
        DataSet ExportFinalCrosses(int bidPartId);
        int BulkInsertPricingData(DataTable pricingData, int bidId);
        int UpdateCrossPrice(int bidPartId, int crossPartId, string priceToQuote, string suggestedPrice, string iCost, string margin, string adjustedICost, string adjustedMargin);
    }
}
