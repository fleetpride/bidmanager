﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public class SummaryRepository : ISummaryRepository
    {
        private string _bmConnString;

        /// <summary>
        /// Default constructer
        /// </summary>
        public SummaryRepository()
        {
            _bmConnString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
        }

        public List<CrowdsourcingSummaryEntity> GetCrowdsourcingSummary(string userRole, string userId)
        {
            List<CrowdsourcingSummaryEntity> lstCrowdsourcingSummary = new List<CrowdsourcingSummaryEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Summary_GetCrowdsourcingSummary", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            CrowdsourcingSummaryEntity record = new CrowdsourcingSummaryEntity();
                            record.BidId = Convert.ToInt32(rdr["BidId"]);
                            record.BidStatus = Convert.ToString(rdr["BidStatus"]);
                            record.BidType = Convert.ToString(rdr["BidType"]);
                            record.BidName = Convert.ToString(rdr["BidName"]);
                            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
                            record.Rank = Convert.ToString(rdr["Rank"]);
                            record.UploadPartCount = Convert.ToString(rdr["UploadPartCount"]);
                            record.CrowdSourcingPartCount = Convert.ToString(rdr["CrowdSourcingPartCount"]);
                            record.UserSubmittedPartCount = Convert.ToString(rdr["UserSubmittedPartCount"]);
                            record.ValidatedPartCount = Convert.ToString(rdr["ValidatedPartCount"]);
                            lstCrowdsourcingSummary.Add(record);
                        }
                    }
                }
            }
            return lstCrowdsourcingSummary;
        }

        public List<ProgressSummaryEntity> GetProgressSummary(string userRole, string userId)
        {
            List<ProgressSummaryEntity> lstProgressSummary = new List<ProgressSummaryEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Summary_ProgressSummary", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            ProgressSummaryEntity record = new ProgressSummaryEntity();
                            record.BidId = Convert.ToInt32(rdr["BidId"]);
                            //set the Bid status
                            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
                            record.BidStatus = bidStatus.Key;
                            record.BidStatusDisplay = bidStatus.ShortDescription;
                            record.BidStatusDescirption = bidStatus.LongDescription;
                            record.BidType = Convert.ToString(rdr["BidType"]);
                            record.BidName = Convert.ToString(rdr["BidName"]);
                            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
                            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
                            record.LaunchDate = Convert.ToString(rdr["LaunchDate"]) == "" ? "" : Convert.ToDateTime(rdr["LaunchDate"]).ToString("MM/dd/yyyy");
                            record.CalculatedDueDate = Convert.ToString(rdr["CalculatedDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CalculatedDueDate"]).ToString("MM/dd/yyyy");
                            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
                            record.Company = Convert.ToString(rdr["Company"]);
                            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
                            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
                            record.CustName = Convert.ToString(rdr["CustName"]);
                            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
                            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
                            record.GroupId = Convert.ToString(rdr["GroupId"]);
                            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
                            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
                            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
                            record.CrossesReviewedBy = Convert.ToString(rdr["CrossesReviewedBy"]);
                            record.CrossesNotReviewedBy = Convert.ToString(rdr["CrossesNotReviewedBy"]);
                            record.InventoryReviewedBy = Convert.ToString(rdr["InventoryReviewedBy"]);
                            record.InventoryNotReviewedBy = Convert.ToString(rdr["InventoryNotReviewedBy"]);
                            record.CrossesFinalized = Convert.ToString(rdr["CrossesFinalized"]) + "/" + Convert.ToString(rdr["TotalParts"]);
                            lstProgressSummary.Add(record);
                        }
                    }
                }
            }
            return lstProgressSummary;
        }
        
    }
}