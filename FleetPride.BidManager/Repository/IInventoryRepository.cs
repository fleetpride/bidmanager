﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public interface IInventoryRepository
    {
        List<BidEntity> GetInventoryUpdateBids(string userRole, string userId);

        List<CrossPartInventoryEntity> GetCrossPartsInventoryList(int bidId, string userId, string userRole);

        DataSet ExportCrossPartsInventory(int bidId);

        DataSet ExportLocationInventory(List<string> locations, int bidId);

        bool ValidateLocation(string location, int bidId);

        int ValidateLocation(int cpInvID, string location, int bidId);

        void BulkSaveCrossPartsInventoryData(List<CrossPartInventoryEntity> records, int bidId, string userId, string userRole);

        void DeleteCrossPartsInventory(List<int> crossPartInventoryIds);

        int UpdateCrossPartInventory(CrossPartInventoryEntity crossPartInventoryEntity, int bidId, string userId);

        List<CMUserEntity> GetCMList(int bidId);

        List<LocationEntity> GetLocationsList(int bidId);
        
        int AddCMAction(List<CMUserEntity> dataToPost, int bidId, string bidLaunchDate);
        void PullbackBidFromInventoryReview(int bidId);
        List<BidEntity> GetVerificationBids(string userRole, string userName);
        List<CrossPartLineReviewEntity> GetCrossPartLineReview(int bidId, string userRole, string userName);
        int SaveInventoryApproval(bool isApproved, List<int> selectedIDs, string userRole, string userName);
        string SubmitInventoryReview(int bidId, string userRole, string userName);
        bool GetLineReviewAction(int bidId, string userRole, string userName);
        List<BidEntity> GetDataTeamBids();
        List<CrossPartInventoryEntity> GetPartsNotSetup(int bidId);
        int SendToDemandTeam(int bidId, string userRole, string name);
        bool HasRevisedParts(int bidId);
        int UpdateCrossPartInventoryReview(int iD, int revisedPool, string revisedPartNumber, string userId, string role, string comments);
    }
}