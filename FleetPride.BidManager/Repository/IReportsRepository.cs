﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public interface IReportsRepository
    {
        List<BidEntity> DemandTeamReportBids(string userRole, string userName);
        List<DemandTeamReportEntity> GetDemandTeamReport(int bidId, string approvalType, string userId, string userRole);
        List<BidStatusReportEntity> GetBidStatusReport(string userId, string userRole);
        List<BidEntity> RevisePartsBids(string userRole, string userName);
        List<CrossPartInventoryHistoryEntity> GetBidChangeReport(int bidId);
        List<CrossPartInventoryEntity> GetRevisePartsReport(int bidId);
        bool LaunchBid(int bidId, string launchedByUserId);
    }
}