﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public class VendorGradeRepository : IVendorGradeRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        public List<VendorGradeEntity> GetVendorGrades()
        {
            List<VendorGradeEntity> lstVendorGrades = new List<VendorGradeEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.VendorGrade_GetVendorGrades", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            VendorGradeEntity record = new VendorGradeEntity();
                            record.VendorGradeId = Convert.ToInt32(rdr["VendorGradeId"]);
                            record.Category = Convert.ToString(rdr["Category"]);
                            record.VendorName = Convert.ToString(rdr["VendorName"]);
                            record.VendorNumber = Convert.ToInt32(rdr["VendorNumber"]);
                            record.ColorCode = Convert.ToString(rdr["ColorCode"]);
                            record.Color = Convert.ToString(rdr["Color"]);
                            record.Definition = Convert.ToString(rdr["Definition"]);
                            record.LastModifiedBy = Convert.ToString(rdr["LastModifiedBy"]);
                            record.LastModifiedOn = Convert.ToDateTime(rdr["LastModifiedOn"]).ToString("MM/dd/yyyy");
                            lstVendorGrades.Add(record);
                        }
                    }
                }
            }
            return lstVendorGrades;
        }

        public void BulkSaveVendorGrades(List<VendorGradeEntity> vendorGrades,string userId)
        {
            DataTable dtVendorGrade = new DataTable("VendorGradeBulk");
            dtVendorGrade.Columns.Add(new DataColumn("Category", typeof(string)));
            dtVendorGrade.Columns.Add(new DataColumn("VendorName", typeof(string)));
            dtVendorGrade.Columns.Add(new DataColumn("VendorNumber", typeof(int)));
            dtVendorGrade.Columns.Add(new DataColumn("ColorCode", typeof(string)));
            dtVendorGrade.Columns.Add(new DataColumn("Color", typeof(string)));
            dtVendorGrade.Columns.Add(new DataColumn("Definition", typeof(string)));

            foreach (var item in vendorGrades)
            {
                DataRow dr = dtVendorGrade.NewRow();
                dr["Category"] = item.Category;
                dr["VendorName"] = item.VendorName;
                dr["VendorNumber"] = item.VendorNumber;
                dr["ColorCode"] = item.ColorCode;
                dr["Color"] = item.Color;
                dr["Definition"] = item.Definition;
                dtVendorGrade.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.VendorGrade_BulkSaveVendorGrade", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VendorGradeUDT", SqlDbType.Structured)).Value = dtVendorGrade;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}