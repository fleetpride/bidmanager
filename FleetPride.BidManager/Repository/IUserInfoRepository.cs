﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace FleetPride.BidManager.Repository
{
    public interface IUserInfoRepository
    {
        bool IsUserAuthorised();
        string GetAuthorisedUserRole();
        bool IsPricingTeamUser(string prmloggedInUserId);
        string GetUserEmail();
    }
}

