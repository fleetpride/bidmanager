﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetPride.BidManager.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using FleetPride.BidManager.Utilities;

namespace FleetPride.BidManager.Repository
{
    public class PriceValidationRepository : IPriceValidationRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        public List<BidEntity> GetAllBids(string userRole, string userId)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PriceValidation_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = CreateBidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        /// <summary>
        /// Method to get final Bid parts list from database
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public List<BidPartEntity> GetFinalBidParts(int bidId, string userId, string userRole)
        {
            List<BidPartEntity> lstBidParts = new List<BidPartEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PriceValidation_GetFinalBidParts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidPartEntity record = new BidPartEntity();
                            record.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
                            record.BidId = Convert.ToInt32(rdr["BidId"]);
                            record.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
                            record.PartDescription = Convert.ToString(rdr["PartDescription"]);
                            record.Manufacturer = Convert.ToString(rdr["Manufacturer"]);
                            record.ManufacturerPartNumber = Convert.ToString(rdr["ManufacturerPartNumber"]);
                            record.Note = Convert.ToString(rdr["Note"]);
                            record.EstimatedAnnualUsage = Convert.ToString(rdr["EstimatedAnnualUsage"]);
                            record.IsFinalized = rdr["IsFinalized"] == DBNull.Value ? "" : Convert.ToString(rdr["IsFinalized"]);
                            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
                            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
                            lstBidParts.Add(record);
                        }
                    }
                }
            }
            return lstBidParts;
        }

        public List<CrossEntity> GetFinalCrosses(int bidPartId)
        {
            List<CrossEntity> crosses = new List<CrossEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PriceValidation_GetFinalCrosses", conn))
                {
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            crosses.Add(new CrossEntity(rdr));
                        }
                    }
                }
            }

            return crosses;
        }

        public DataSet ExportFinalCrosses(int bidId)
        {
            DataSet crosses = new DataSet();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PriceValidation_DownloadCrosses", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(crosses);
                    }
                }
            }
            return crosses;
        }

        /// <summary>
        /// Method to Insert uploaded pricing data in bulk in database
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public int BulkInsertPricingData(DataTable pricingData, int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (var cmd = new SqlCommand("BM.PriceValidation_UploadCrossPrice", conn))
                {
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartsPrice", SqlDbType.Structured)).Value = pricingData;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return 1;
        }

        /// <summary>
        /// Method to update cross price in db
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <param name="crossPartId"></param>
        /// <param name="priceToQuote"></param>
        /// <param name="suggestedPrice"></param>
        /// <param name="iCost"></param>
        /// <param name="margin"></param>
        /// <returns></returns>
        public int UpdateCrossPrice(int bidPartId, int crossPartId, string priceToQuote, string suggestedPrice, string iCost, string margin, string adjsutedICost, string adjustedMargin )
        {
            int rowAffected = 0;
            if (priceToQuote == "")
            {
                priceToQuote = null;
            }
            if (suggestedPrice == "")
            {
                suggestedPrice = null;
            }
            if (iCost == "")
            {
                iCost = null;
            }
            if (margin == "")
            {
                margin = null;
            }
            if (adjsutedICost == "")
            {
                adjsutedICost = null;
            }
            if (adjustedMargin == "")
            {
                adjustedMargin = null;
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PriceValidation_UpdateCrossPrice", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartId", SqlDbType.Int)).Value = crossPartId; 
                    cmd.Parameters.Add(new SqlParameter("@PriceToQuote", SqlDbType.VarChar)).Value = priceToQuote;
                    cmd.Parameters.Add(new SqlParameter("@SuggestedPrice", SqlDbType.VarChar)).Value = suggestedPrice;
                    cmd.Parameters.Add(new SqlParameter("@ICOST", SqlDbType.VarChar)).Value = iCost;
                    cmd.Parameters.Add(new SqlParameter("@Margin", SqlDbType.VarChar)).Value = margin;
                    cmd.Parameters.Add(new SqlParameter("@AdjustedICost", SqlDbType.VarChar)).Value = adjsutedICost;
                    cmd.Parameters.Add(new SqlParameter("@AdjustedMargin", SqlDbType.VarChar)).Value = adjustedMargin;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public BidEntity CreateBidEntity(IDataReader rdr)
        {
            BidEntity record = new BidEntity();
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);

            //set the Bid status
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;

            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
            record.CalculatedDueDate = Convert.ToString(rdr["CalculatedDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CalculatedDueDate"]).ToString("MM/dd/yyyy");
            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
            record.Rank = Convert.ToString(rdr["Rank"]);
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.RequestType = Convert.ToString(rdr["RequestType"]);
            record.SubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("MM/dd/yyyy");
            record.FPFacingLocations = Convert.ToString(rdr["FPFacingLocations"]);
            record.TotalValueOfBid = Convert.ToString(rdr["TotalValueOfBid"]);
            record.IsFPPriceHold = Convert.ToBoolean(rdr["IsFPPriceHold"]);
            record.HoldDate = Convert.ToString(rdr["HoldDate"]) == "" ? "" : Convert.ToDateTime(rdr["HoldDate"]).ToString("MM/dd/yyyy");
            record.IsCustomerAllowSubstitutions = Convert.ToBoolean(rdr["IsCustomerAllowSubstitutions"]);
            record.BrandPreference = Convert.ToString(rdr["BrandPreference"]);
            record.IsCustomerInventoryPurchaseGuarantee = Convert.ToBoolean(rdr["IsCustomerInventoryPurchaseGuarantee"]);
            record.LevelOfInvetoryDetailCustomerCanProvide = Convert.ToString(rdr["LevelOfInvetoryDetailCustomerCanProvide"]);
            record.SourceExcelFileName = Convert.ToString(rdr["SourceExcelFileName"]);
            record.CreatedByRole = Convert.ToString(rdr["CreatedByRole"]);
            record.NAMUserID = Convert.ToString(rdr["NAMUserID"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);

            return record;
        }
    }
}