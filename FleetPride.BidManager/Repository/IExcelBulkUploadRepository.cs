﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public interface IExcelBulkUploadRepository
    {
        List<VendorCMRelationEntity> GetVendorCMRelations();
        void BulkSaveVendorCMRelations(List<VendorCMRelationEntity> vendorCMRelations, string userId);
    }
}