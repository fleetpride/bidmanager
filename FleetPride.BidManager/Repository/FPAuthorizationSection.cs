﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace FleetPride.BidManager.Repository
{
    public class FPAuthorizationSection : ConfigurationSection
    {
        private static ConfigurationPropertyCollection _properties;
        private static ConfigurationProperty _modules;

        static FPAuthorizationSection()
        {
            _modules = new ConfigurationProperty(
                "modules",
                typeof(FPAuthorizationElementCollection),
                null,
                ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsDefaultCollection
                );

            _properties = new ConfigurationPropertyCollection();

            _properties.Add(_modules);
        }

        public FPAuthorizationElementCollection Modules
        {
            get { return (FPAuthorizationElementCollection)base[_modules]; }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return _properties;
            }
        }
    }
}