﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public interface IVendorGradeRepository
    {
        List<VendorGradeEntity> GetVendorGrades();
        void BulkSaveVendorGrades(List<VendorGradeEntity> vendorGrades, string userId);
    }
}