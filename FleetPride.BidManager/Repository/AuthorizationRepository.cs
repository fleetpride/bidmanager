﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;

namespace FleetPride.BidManager.Utilities
{
    public class AuthorizationRepository : IAuthorizationRepository
    {
        string _connString;
        public AuthorizationRepository()
        {
            _connString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
        }

        /// <summary>
        /// Method to get logged in user details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserEntity GetUserDetailsFromDB(string userId)
        {
            UserEntity user = new UserEntity();

            using (SqlConnection con = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Authorization_GetUserDetails", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;

                    if (con.State != ConnectionState.Open)
                    {
                        con.ConnectionString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
                        con.Open();
                    }

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        user.UserId = Convert.ToString(rdr["PrincipalUserId"]);
                        user.UserRole = Convert.ToString(rdr["UserRole"]);
                        user.UserEmail = Convert.ToString(rdr["EmailAddress"]);
                    }
                }
            }

            return user;
        }

        public string GetUserEmail(string userId)
        {
            using (SqlConnection con = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Authorization_GetUserEmail", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;

                    if (con.State != ConnectionState.Open)
                    {
                        con.ConnectionString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
                        con.Open();
                    }

                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        return Convert.ToString(rdr["Email"]);
                    }
                }
            }

            return "";
        }

        internal List<UserEntity> GetPricingTeam()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupPricingTeam"], UserRole.PricingTeam);
        }

        public List<UserEntity> GetSuperUsers()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupSuperUser"], UserRole.SuperUser);
        }

        public List<UserEntity> GetNAMValidators()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupNAMValidator"], UserRole.NAMValidator);
        }

        public List<UserEntity> GetNAMSupervisors()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupNAMSupervisor"], UserRole.NAMSupervisor);
        }

        public List<UserEntity> GetRAMValidators()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupRAMValidator"], UserRole.RAMValidator);
        }

        public List<UserEntity> GetRAMSupervisors()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupRAMSupervisor"], UserRole.RAMSupervisor);
        }

        public List<UserEntity> GetDataTeam()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupDataTeam"], UserRole.DataTeam);
        }

        public List<UserEntity> GetDemandTeam()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupDemandTeam"], UserRole.DemandTeam);
        }

        public List<UserEntity> GetCategoryDirector()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupCategoryDirector"], UserRole.CategoryDirector);
        }

        public List<UserEntity> GetCategoryVP()
        {
            return GetUsersInGroup(ConfigurationManager.AppSettings["ADGroupCategoryVP"], UserRole.CategoryVP);
        }

        public List<UserEntity> GetUsersInGroup(string adGroup, UserRole role)
        {
            List<UserEntity> users = new List<UserEntity>();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["CheckUserDetailsFromADGroupAllowed"]))
            {
                users.AddRange(GetAllUsersInADGroup(adGroup, role));
            }
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["CheckDeligateTableForRoles"]))
            {
                users.AddRange(GetUsersInRoleFromDB(role));
            }
            return users; //.Distinct<UserEntity>(new UserEntityComparer()).ToList<UserEntity>();
        }

        public List<UserEntity> GetAllUsersInADGroup(string adGroups, UserRole role)
        {
            List<UserEntity> users = new List<UserEntity>();

            string[] groups = adGroups.Split(',');
            foreach (string adGroup in groups)
            {
                PrincipalContext principalContext = new PrincipalContext(ContextType.Domain);
                GroupPrincipal group = GroupPrincipal.FindByIdentity(principalContext, adGroup.Trim());

                if (group != null)
                {
                    foreach (Principal member in group.Members)
                    {
                        users.Add(new UserEntity { UserId = member.SamAccountName, UserRole = role.ToString(), UserEmail = GetUserEmail(member.SamAccountName) });
                    }
                }
                else
                {
                    throw new Exception("AD group not found: " + adGroup.Trim());
                }
            }

            return users;
        }

        public List<UserEntity> GetUsersInRoleFromDB(UserRole role)
        {
            List<UserEntity> users = new List<UserEntity>();

            using (SqlConnection con = new SqlConnection(_connString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Authorization_GetUsersInRole", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@role", SqlDbType.VarChar)).Value = role.ToString();
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        users.Add(new UserEntity
                        {
                            UserId = Convert.ToString(rdr["PrincipalUserId"]),
                            UserRole = Convert.ToString(rdr["UserRole"]),
                            UserEmail = Convert.ToString(rdr["EmailAddress"])
                        });
                    }
                }
            }

            return users;
        }

        /// <summary>
        /// Helper method to check if user is present in AD group
        /// </summary>
        /// <param name="loggedInUserId"></param>
        /// <param name="adGroup"></param>
        /// <returns></returns>
        public bool IsUserInADGroup(string userId, string adGroup)
        {
            string[] arrADGroups;

            if (adGroup.Contains(','))
                arrADGroups = adGroup.Split(',').Select(sValue => sValue.Trim()).ToArray();
            else
                arrADGroups = new string[] { adGroup };

            foreach (string group in arrADGroups)
            {
                PrincipalContext principalContext = new PrincipalContext(ContextType.Domain);
                GroupPrincipal gp = GroupPrincipal.FindByIdentity(principalContext, group.Trim());

                if (IsUserInADGroup(userId, gp))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Helper method to get users from nested group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool IsUserInADGroup(string userId, GroupPrincipal group)
        {
            foreach (Principal principal in group.Members)
            {
                if (principal.StructuralObjectClass.Equals("group"))
                {
                    if (IsUserInADGroup(userId, (GroupPrincipal)principal))
                        return true;
                }
                else if (principal.StructuralObjectClass.Equals("user"))
                {
                    if (userId == principal.SamAccountName)
                        return true;
                }
            }

            return false;
        }

        public bool IsUserAuthorised()
        {
            if (System.Web.HttpContext.Current.User.IsInRole(UserRole.SuperUser.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.LocalUser.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.NAMSupervisor.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.NAMValidator.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.NAM.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.RAMSupervisor.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.RAMValidator.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.RAM.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.PricingTeam.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CategoryManager.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CategoryDirector.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CategoryVP.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.DataTeam.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.DemandTeam.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CrossValidator.ToString()))
                return true;
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CrossEntryUser.ToString()))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Helper method to get the user role
        /// </summary>
        /// <returns></returns>
        public string GetAuthorisedUserRole()
        {
            if (System.Web.HttpContext.Current.User.IsInRole(UserRole.SuperUser.ToString()))
                return UserRole.SuperUser.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.LocalUser.ToString()))
                return UserRole.LocalUser.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.NAMSupervisor.ToString()))
                return UserRole.NAMSupervisor.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.NAMValidator.ToString()))
                return UserRole.NAMValidator.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.NAM.ToString()))
                return UserRole.NAM.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.PricingTeam.ToString()))
                return UserRole.PricingTeam.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CategoryManager.ToString()))
                return UserRole.CategoryManager.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CrossValidator.ToString()))
                return UserRole.CrossValidator.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CrossEntryUser.ToString()))
                return UserRole.CrossEntryUser.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CategoryDirector.ToString()))
                return UserRole.CategoryDirector.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.CategoryVP.ToString()))
                return UserRole.CategoryVP.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.DataTeam.ToString()))
                return UserRole.DataTeam.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.DemandTeam.ToString()))
                return UserRole.DemandTeam.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.RAM.ToString()))
                return UserRole.RAM.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.RAMSupervisor.ToString()))
                return UserRole.RAMSupervisor.ToString();
            else if (System.Web.HttpContext.Current.User.IsInRole(UserRole.RAMValidator.ToString()))
                return UserRole.RAMValidator.ToString();
            else
                return "";
        }

        /// <summary>
        /// Helper method to get user email
        /// </summary>
        /// <returns></returns>
        public string GetUserEmail()
        {
            ClaimsIdentity claimsIdentity = System.Web.HttpContext.Current.User.Identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(ClaimTypes.Email);
            return claim.Value ?? string.Empty;
        }

        public bool HasAccess(string moduleName)
        {
            string userRole = GetAuthorisedUserRole().ToString().ToLower();
            return HasAccess(moduleName, userRole);
        }

        public static bool HasAccess(string moduleName, string userRole)
        {
            FPAuthorizationSection authSection = ConfigurationManager.GetSection("FPAuthorization") as FPAuthorizationSection;
            FPAuthorizationElement module = authSection.Modules[moduleName];

            if (module != null)
            {
                if (module.Roles.ToLower().Contains("any"))
                    return true;

                return module.Roles.ToLower().Contains(userRole.ToLower());
            }

            return false;
        }
        
    }
}