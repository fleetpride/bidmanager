﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public class ExcelBulkUploadRepository : IExcelBulkUploadRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        public List<VendorCMRelationEntity> GetVendorCMRelations()
        {
            List<VendorCMRelationEntity> lstVendorCMRelations = new List<VendorCMRelationEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("VCM.VendorCMRelation_GetVendorCMRelations", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            VendorCMRelationEntity record = new VendorCMRelationEntity();
                            record.VendorNumber = Convert.ToInt32(rdr["VendorNumber"]);
                            record.VendorName = Convert.ToString(rdr["VendorName"]);
                            record.DefaultCategory = Convert.ToString(rdr["DefaultCategory"]);
                            record.CategoryManager = Convert.ToString(rdr["CategoryManager"]);
                            record.PartCount = Convert.ToString(rdr["PartCount"]);
                            record.Buyer = Convert.ToString(rdr["Buyer"]);
                            lstVendorCMRelations.Add(record);
                        }
                    }
                }
            }
            return lstVendorCMRelations;
        }

        public void BulkSaveVendorCMRelations(List<VendorCMRelationEntity> vendorGrades,string userId)
        {
            DataTable dtVendorCMRelations = new DataTable("VendorCMRelationBulk");
            dtVendorCMRelations.Columns.Add(new DataColumn("VendorNumber", typeof(int)));
            dtVendorCMRelations.Columns.Add(new DataColumn("VendorName", typeof(string)));
            dtVendorCMRelations.Columns.Add(new DataColumn("DefaultCategory", typeof(string)));
            dtVendorCMRelations.Columns.Add(new DataColumn("CategoryManager", typeof(string)));
            dtVendorCMRelations.Columns.Add(new DataColumn("PartCount", typeof(string)));
            dtVendorCMRelations.Columns.Add(new DataColumn("Buyer", typeof(string)));

            foreach (var item in vendorGrades)
            {
                DataRow dr = dtVendorCMRelations.NewRow();
                dr["VendorNumber"] = item.VendorNumber;
                dr["VendorName"] = item.VendorName;
                dr["DefaultCategory"] = item.DefaultCategory;
                dr["CategoryManager"] = item.CategoryManager;
                dr["PartCount"] = item.PartCount;
                dr["Buyer"] = item.Buyer;
                dtVendorCMRelations.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("VCM.VendorCMRelation_BulkSaveVendorCMRelation", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VendorCMRelationUDT", SqlDbType.Structured)).Value = dtVendorCMRelations;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}