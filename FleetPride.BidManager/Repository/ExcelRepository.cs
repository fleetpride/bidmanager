﻿using FleetPride.BidManager.Utilities;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public class ExcelRepository : IExcelRepository
    {
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeAccessTokenHandle phToken);

        public void ClearCache(string localExcelFolder)
        {
            try
            {
                string[] existingFiles = Directory.GetFiles(localExcelFolder, "*.*");
                foreach (string existingFile in existingFiles)
                {
                    File.Delete(existingFile);
                }
            }
            catch(Exception exp)
            {
                Logger.QuickLog(Convert.ToString(exp));
            }
        }

        public string GetExcel(string fileName, string localExcelFolder)
        {
            string remoteExcelFolder = ConfigurationManager.AppSettings["UploadExcelPath"];            
            bool isDeployed = Convert.ToBoolean(ConfigurationManager.AppSettings["Deployed"]);
            string sharedPath = remoteExcelFolder + fileName;
            string localPath = localExcelFolder + fileName;
            if (isDeployed)
            {
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.   
                const int LOGON32_LOGON_INTERACTIVE = 2;

                string userName = ConfigurationManager.AppSettings["ServiceUserName"];
                string domainName = ConfigurationManager.AppSettings["ServiceDomainName"];
                string pwd = ConfigurationManager.AppSettings["ServiceUserPwd"];
                SafeAccessTokenHandle safeAccessTokenHandle;

                bool loggedOn = LogonUser(userName, domainName, pwd, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out safeAccessTokenHandle);
                if (loggedOn)
                {
                    System.Security.Principal.WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                    {
                        //copy the path to the local folder
                        //fileBytes = File.ReadAllBytes(sharedPath);
                        if (File.Exists(localPath))
                            File.Delete(localPath);
                        File.Copy(sharedPath, localPath, true);
                    });
                }
                else
                {
                    throw new Exception("Not able to access the image folder.");
                }
            }

            return localPath;
        }

        public void SaveExcel(HttpPostedFileBase file, string newFileName, string localExcelFolder)
        {
            string imagesFolder = ConfigurationManager.AppSettings["UploadExcelPath"];
            bool isDeployed = Convert.ToBoolean(ConfigurationManager.AppSettings["Deployed"]);
            
            if (isDeployed)
            {
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.   
                const int LOGON32_LOGON_INTERACTIVE = 2;

                string userName = ConfigurationManager.AppSettings["ServiceUserName"];
                string domainName = ConfigurationManager.AppSettings["ServiceDomainName"];
                string pwd = ConfigurationManager.AppSettings["ServiceUserPwd"];
                SafeAccessTokenHandle safeAccessTokenHandle;

                bool loggedOn = LogonUser(userName, domainName, pwd, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out safeAccessTokenHandle);
                if (loggedOn)
                {
                    System.Security.Principal.WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                    {
                        SaveFile(file, imagesFolder, newFileName);
                    });
                }
                else
                {
                    throw new Exception("Not able to access the excel folder.");
                }
            }
            else
            {
                SaveFile(file, localExcelFolder, newFileName);
            }
        }

        private void SaveFile(HttpPostedFileBase file, string imagesFolder, string newFileName)
        {
            string path = imagesFolder + newFileName;

            file.SaveAs(path);
        }
    }
}