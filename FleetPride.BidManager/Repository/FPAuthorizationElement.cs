﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace FleetPride.BidManager.Repository
{
    public class FPAuthorizationElement : ConfigurationElement
    {
        private static ConfigurationProperty _name;
        private static ConfigurationProperty _roles;
        private static ConfigurationPropertyCollection _properties;

        static FPAuthorizationElement()
        {
            _name = new ConfigurationProperty(
              "name",
              typeof(string),
              null,
              ConfigurationPropertyOptions.IsRequired
          );

            _roles = new ConfigurationProperty(
                "roles",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired
            );

            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_name);
            _properties.Add(_roles);
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string) base[_name]; }
        }

        [ConfigurationProperty("roles", IsRequired = true)]
        public string Roles
        {
            get
            {
                string val = (string)base[_roles];
                if (!string.IsNullOrEmpty(val))
                    return val.Trim();
                return "";
            }
        }

        public List<string> RolesList
        {
            get
            {
                return this.Roles.Split(',').ToList<string>();
            }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get { return _properties; }
        }
    }
}