﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetPride.BidManager.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using FleetPride.BidManager.Utilities;
using FleetPride.Extensions;

namespace FleetPride.BidManager.Repository
{
    public class InventoryRepository : IInventoryRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        public int SaveInventoryApproval(bool isApproved, List<int> selectedIDs, string userRole, string userName)
        {
            int rowAffected = 0;

            DataTable dtIDs = new DataTable("CrossPartInventoryIds");
            dtIDs.Columns.Add("ID", typeof(int));
            foreach (int id in selectedIDs)
            {
                DataRow dr = dtIDs.NewRow();
                dr["ID"] = id;
                dtIDs.Rows.Add(dr);
            }
            //if (!isApproved)
            //{
            //    if (userRole == Constants.LineReview_VP)
            //    {
            //        comments = "CategoryCMO " + "'" + userName + "'" + " Comments: " + comments;
            //    }
            //    else
            //    {
            //        comments = "CategoryManager " + "'" + userName + "'" + " Comments: " + comments;
            //    }
            //}
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_SaveInventoryApproval", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IsApproved", SqlDbType.Bit)).Value = isApproved ? 1 : 0;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartInventoryIds", SqlDbType.Structured)).Value = dtIDs;
                    //cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.VarChar)).Value = comments;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    return rowAffected;
                }
            }
        }

        public List<BidEntity> GetVerificationBids(string userRole, string userName)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = BidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }

            return lstBid;
        }

        public List<BidEntity> GetDataTeamBids()
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PartsNotSetup_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = BidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        public List<BidEntity> GetInventoryUpdateBids(string userRole, string userId)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = BidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        /// <summary>
        /// Method to get Bid parts list from database
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public List<CrossPartInventoryEntity> GetCrossPartsInventoryList(int bidId, string userId, string userRole)
        {
            List<CrossPartInventoryEntity> lstCrossPartInventory = new List<CrossPartInventoryEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_GetWonPartsInventoryList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstCrossPartInventory.Add(CreateCrossPartInventoryEntity(rdr));
                        }
                    }
                }
            }

            return lstCrossPartInventory;
        }

        private CrossPartInventoryEntity CreateCrossPartInventoryEntity(SqlDataReader rdr)
        {
            CrossPartInventoryEntity record = new CrossPartInventoryEntity();
            record.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
            record.ManufacturerPartNumber = Convert.ToString(rdr["ManufacturerPartNumber"]);
            record.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
            record.PartDescription = Convert.ToString(rdr["PartDescription"]);
            record.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
            record.CrossPoolNumber = Convert.ToString(rdr["CrossPoolNumber"]);
            record.CrossPartNumber = Convert.ToString(rdr["CrossPartNumber"]);
            record.CrossPartDescription = Convert.ToString(rdr["CrossPartDescription"]);
            record.CrossPrice = Convert.ToString(rdr["CrossPrice"]);
            record.CrossICOST = Convert.ToString(rdr["CrossICOST"]);
            record.CrossPartInventoryId = Convert.ToInt32(rdr["CrossPartInventoryId"] == DBNull.Value ? 0 : Convert.ToInt32(rdr["CrossPartInventoryId"]));
            record.Location = Convert.ToString(rdr["Location"]);
            record.CustPartUOM = Convert.ToString(rdr["CustPartUOM"]);
            record.EstimatedAnnualUsage = rdr.GetIntIfHas("EstimatedAnnualUsage"); //Convert.ToString(rdr["EstimatedAnnualUsage"] == DBNull.Value ? "" : Convert.ToString(rdr["EstimatedAnnualUsage"]));
            record.NewPool = Convert.ToString(rdr["NewPool"]);
            record.NewPartNo = Convert.ToString(rdr["NewPartNo"]);
            record.NewPrice = Convert.ToString(rdr["NewPrice"] == DBNull.Value ? "" : Convert.ToString(rdr["NewPrice"]));
            record.IsActive = rdr.GetStringIfHas("IsActive");
            record.StockingType = rdr.GetStringIfHas("StockingType");
            record.NewStockingType = rdr.GetStringIfHas("NewStockingType");
            record.ParentKit = rdr.GetStringIfHas("ParentKit");
            record.RevisedPool = Convert.ToString(rdr["RevisedPool"] == DBNull.Value ? null : Convert.ToString(rdr["RevisedPool"]));
            record.RevisedPartNumber = Convert.ToString(rdr["RevisedPartNo"] == DBNull.Value ? null : Convert.ToString(rdr["RevisedPartNo"]));
            record.RevisePartComments = Convert.ToString(rdr["Comments"] == DBNull.Value ? null : Convert.ToString(rdr["Comments"]));

            return record;
        }

        /// <summary>
        /// Export CrossPartsInventory on Inventory page from DB
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public DataSet ExportCrossPartsInventory(int bidId)
        {
            DataSet crosses = new DataSet();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_DownloadWonPartsInventoryList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(crosses);
                    }
                }
            }
            return crosses;
        }

        /// <summary>
        /// Export Locationwise inventory from DB
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public DataSet ExportLocationInventory(List<string> locations, int bidId)
        {
            DataSet ds = new DataSet();
            DataTable dtlocations = new DataTable("Locations");
            dtlocations.Columns.Add(new DataColumn("Location", typeof(string)));
            foreach (var item in locations)
            {
                DataRow dr = dtlocations.NewRow();
                dr["Location"] = item;
                dtlocations.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_DownloadLocationInventory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@LocationsUDT", SqlDbType.Structured)).Value = dtlocations;
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
            }

            return ds;
        }

        /// <summary>
        /// Validate location from DB
        /// </summary>
        /// <param name="location"></param>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public bool ValidateLocation(string location, int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (var cmd = new SqlCommand("BM.ValidateLocation", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.VarChar)).Value = location;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.VarChar)).Value = bidId;

                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        if (rdr["Location"] != DBNull.Value)
                            return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// 0 - All well
        /// 1 - Invalid Location
        /// 2 - Duplicate Location in CPI for this part
        /// </summary>
        /// <param name="cpInvID"></param>
        /// <param name="location"></param>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public int ValidateLocation(int cpInvID, string location, int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (var cmd = new SqlCommand("BM.Inventory_ValidateLocation", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cpInvID", SqlDbType.Int)).Value = cpInvID;
                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.VarChar)).Value = location;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.VarChar)).Value = bidId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;

                    conn.Open();

                    cmd.ExecuteScalar();
                    return (int)cmd.Parameters["@Result"].Value;
                }
            }
        }

        /// <summary>
        /// Save uploaded cross parts inventory data location wise in DB
        /// </summary>
        /// <param name="crossPartInventory"></param>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        public void BulkSaveCrossPartsInventoryData(List<CrossPartInventoryEntity> crossPartInventory, int bidId, string userId, string userRole)
        {
            DataTable crossPartInventoryBulk = new DataTable("CrossPartInventoryBulk");

            crossPartInventoryBulk.Columns.Add(new DataColumn("BidId", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPartId", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("BidPartId", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CustomerPartNumber", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("ManufacturerPartNumber", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPoolNumber", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPartNumber", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPrice", typeof(decimal)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("Location", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CustPartUOM", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("EstimatedAnnualUsage", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("NewPool", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("NewPartNo", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("NewPrice", typeof(decimal)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("IsActive", typeof(string)));

            foreach (var item in crossPartInventory)
            {
                DataRow dr = crossPartInventoryBulk.NewRow();
                dr["BidId"] = bidId;
                dr["CustomerPartNumber"] = item.CustomerPartNumber;
                dr["ManufacturerPartNumber"] = item.ManufacturerPartNumber;
                dr["CrossPoolNumber"] = item.CrossPoolNumber;
                dr["CrossPartNumber"] = item.CrossPartNumber;
                dr["CrossPrice"] = item.CrossPrice != null ? item.CrossPrice : (object)DBNull.Value;
                dr["Location"] = item.Location.ToUpper();
                dr["CustPartUOM"] = item.CustPartUOM;
                dr["EstimatedAnnualUsage"] = item.EstimatedAnnualUsage;
                dr["NewPool"] = item.NewPool != null ? item.NewPool : (object)DBNull.Value;
                dr["NewPartNo"] = item.NewPartNo;
                dr["NewPrice"] = item.NewPrice != null ? item.NewPrice : (object)DBNull.Value;
                switch (string.IsNullOrWhiteSpace(item.IsActive) ? "yes" : item.IsActive.Trim().ToLower())
                {
                    case "no":
                        dr["IsActive"] = "No";
                        break;
                    case "n":
                        dr["IsActive"] = "No";
                        break;
                    default:
                        dr["IsActive"] = "Yes";
                        break;
                }
                crossPartInventoryBulk.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_BulkSaveCrossPartsInventory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartInventoryUDT", SqlDbType.Structured)).Value = crossPartInventoryBulk;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.NVarChar)).Value = userRole;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteCrossPartsInventory(List<int> crossPartInventoryIds)
        {
            DataTable CrossPartInventoryIds = new DataTable("CrossPartInventoryIds");
            CrossPartInventoryIds.Columns.Add(new DataColumn("CrossPartInventoryId", typeof(int)));
            foreach (var item in crossPartInventoryIds)
            {
                DataRow dr = CrossPartInventoryIds.NewRow();
                dr["CrossPartInventoryId"] = item;
                CrossPartInventoryIds.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_DeleteCrossPartsInventory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DeleteCrossPartsInventoryIdUDT", SqlDbType.Structured)).Value = CrossPartInventoryIds;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int UpdateCrossPartInventory(CrossPartInventoryEntity record, int bidId, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_UpdateCrossPartInventory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartId", SqlDbType.Int)).Value = record.CrossPartId;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartInventoryId", SqlDbType.Int)).Value = record.CrossPartInventoryId;
                    cmd.Parameters.Add(new SqlParameter("@CustomerPartNumber", SqlDbType.NVarChar)).Value = record.CustomerPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@ManufacturerPartNumber", SqlDbType.NVarChar)).Value = record.ManufacturerPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@CrossPoolNumber", SqlDbType.NVarChar)).Value = record.CrossPoolNumber;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartNumber", SqlDbType.VarChar)).Value = record.CrossPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@CrossPrice", SqlDbType.Decimal)).Value = record.CrossPrice;
                    cmd.Parameters.Add(new SqlParameter("@Location", SqlDbType.NVarChar)).Value = record.Location.ToUpper();
                    cmd.Parameters.Add(new SqlParameter("@CustPartUOM", SqlDbType.NVarChar)).Value = record.CustPartUOM;
                    cmd.Parameters.Add(new SqlParameter("@EstimatedAnnualUsage", SqlDbType.Int)).Value = (int) record.EstimatedAnnualUsage;
                    cmd.Parameters.Add(new SqlParameter("@NewPool", SqlDbType.NVarChar)).Value = record.NewPool;
                    cmd.Parameters.Add(new SqlParameter("@NewPartNo", SqlDbType.NVarChar)).Value = record.NewPartNo;
                    cmd.Parameters.Add(new SqlParameter("@NewPrice", SqlDbType.Decimal)).Value = record.NewPrice;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.VarChar)).Value = !string.IsNullOrWhiteSpace(record.IsActive) && record.IsActive.Trim().ToLower().Contains('n') ? "No" : "Yes";

                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    return (int)cmd.Parameters["@AffectedRowId"].Value;
                }
            }
        }

        public List<CMUserEntity> GetCMList(int bidId)
        {
            List<CMUserEntity> lstCM = new List<CMUserEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_GetCMForBid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            CMUserEntity crossReport = new CMUserEntity();
                            crossReport.CMName = Convert.ToString(rdr["Name"]);
                            crossReport.CMUserId = Convert.ToString(rdr["UserId"]);
                            crossReport.CMEmail = Convert.ToString(rdr["Email"]);
                            lstCM.Add(crossReport);
                        }
                    }
                }
            }

            return lstCM;
        }

        public List<LocationEntity> GetLocationsList(int bidId)
        {
            List<LocationEntity> lstLocations = new List<LocationEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_GetLocations", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            LocationEntity location = new LocationEntity();
                            location.Location = Convert.ToString(rdr["Location"]);
                            location.LocationName = Convert.ToString(rdr["LocationName"]);
                            location.TerritoryId = Convert.ToString(rdr["TerritoryId"]);
                            location.PriceRegion = Convert.ToString(rdr["PriceRegion"]);
                            lstLocations.Add(location);
                        }
                    }
                }
            }
            return lstLocations;
        }

        public int AddCMAction(List<CMUserEntity> dataToPost, int bidId, string bidLaunchDate)
        {
            int rowAffected = 0;

            foreach (var cm in dataToPost)
            {
                using (SqlConnection conn = new SqlConnection(_bmConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("BM.Inventory_AddCMAction", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                        cmd.Parameters.Add(new SqlParameter("@BidLaunchDate", SqlDbType.Date)).Value = bidLaunchDate;
                        cmd.Parameters.Add(new SqlParameter("@CMUserId", SqlDbType.NVarChar)).Value = cm.CMUserId;
                        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                        conn.Open();
                        cmd.ExecuteScalar();

                        rowAffected = (int)cmd.Parameters["@Result"].Value;
                    }
                }
            }
            return rowAffected;
        }

        /// <summary>
        /// Delete all inventory review action records for a bid when bid is pulled back
        /// </summary>
        /// <param name="bidId"></param>
        public void PullbackBidFromInventoryReview(int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_DeleteCMAction", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public BidEntity BidEntity(IDataReader rdr)
        {
            BidEntity record = new BidEntity();
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);

            //set the Bid status
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;

            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
            record.CalculatedDueDate = Convert.ToString(rdr["CalculatedDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CalculatedDueDate"]).ToString("MM/dd/yyyy");
            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
            record.Rank = Convert.ToString(rdr["Rank"]);
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.RequestType = Convert.ToString(rdr["RequestType"]);
            record.SubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("MM/dd/yyyy");
            record.FPFacingLocations = Convert.ToString(rdr["FPFacingLocations"]);
            record.TotalValueOfBid = Convert.ToString(rdr["TotalValueOfBid"]);
            record.IsFPPriceHold = Convert.ToBoolean(rdr["IsFPPriceHold"]);
            record.HoldDate = Convert.ToString(rdr["HoldDate"]) == "" ? "" : Convert.ToDateTime(rdr["HoldDate"]).ToString("MM/dd/yyyy");
            record.IsCustomerAllowSubstitutions = Convert.ToBoolean(rdr["IsCustomerAllowSubstitutions"]);
            record.BrandPreference = Convert.ToString(rdr["BrandPreference"]);
            record.IsCustomerInventoryPurchaseGuarantee = Convert.ToBoolean(rdr["IsCustomerInventoryPurchaseGuarantee"]);
            record.LevelOfInvetoryDetailCustomerCanProvide = Convert.ToString(rdr["LevelOfInvetoryDetailCustomerCanProvide"]);
            record.SourceExcelFileName = Convert.ToString(rdr["SourceExcelFileName"]);
            record.CreatedByRole = Convert.ToString(rdr["CreatedByRole"]);
            record.NAMUserID = Convert.ToString(rdr["NAMUserID"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);

            return record;
        }

        public List<CrossPartLineReviewEntity> GetCrossPartLineReview(int bidId, string userRole, string userName)
        {
            List<CrossPartLineReviewEntity> lstCrossPartInventory = new List<CrossPartLineReviewEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_GetLineReviewItems", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstCrossPartInventory.Add(CreateCrossPartLineReviewEntity(rdr));
                        }
                    }
                }
            }

            return lstCrossPartInventory;
        }

        private CrossPartLineReviewEntity CreateCrossPartLineReviewEntity(SqlDataReader rdr)
        {
            CrossPartLineReviewEntity record = new CrossPartLineReviewEntity();
            record.ID = Convert.ToInt32(rdr["ID"]);
            record.CrossPartID = Convert.ToInt32(rdr["CrossPartID"]);
            record.BidPartID = Convert.ToInt32(rdr["BidPartID"]);
            record.BidID = Convert.ToInt32(rdr["BidID"]);
            record.CrossPartNumber = Convert.ToString(rdr["CrossPartNumber"]);
            record.CrossPoolNumber = Convert.ToString(rdr["CrossPoolNumber"]);
            record.DCStockCount = Convert.ToInt32(rdr["DCStockCount"]);
            record.ExistingStockingLocations = Convert.ToInt32(rdr["ExistingStockingLocations"]);
            record.ICost = Convert.ToDecimal(rdr["ICost"]);
            record.MedianCost = Convert.ToDecimal(rdr["MedianCost"]);
            record.L12Units = Convert.ToInt32(rdr["L12Units"]);
            record.NewStockingLocations = Convert.ToInt32(rdr["NewStockingLocations"]);
            record.ProjectedInvestment = Convert.ToDecimal(rdr["ProjectedInvestment"]);
            record.ProjectedL12GP = Convert.ToDecimal(rdr["ProjectedL12GP"]);
            record.ProjectedL12Units = Convert.ToInt32(rdr["ProjectedL12Units"]);
            record.StockingType = Convert.ToString(rdr["StockingType"]);
            record.IsApproved = Convert.ToBoolean(rdr["IsApproved"]);
            record.ProjectedL12Revenue = Convert.ToDecimal(rdr["ProjectedL12Revenue"]);
            record.CrossPartDescription = Convert.ToString(rdr["CrossPartDescription"]);
            record.CrossPartCategory = Convert.ToString(rdr["CrossPartCategory"]);
            record.CrossPartVendor = Convert.ToString(rdr["CrossPartVendor"]);
            record.AlreadyStockingLocations = Convert.ToInt32(rdr["AlreadyStockingLocations"]);
            record.CategoryManagerID = Convert.ToString(rdr["CategoryManagerID"]);
            record.CategoryDirectorID = Convert.ToString(rdr["CategoryDirectorID"]);
            record.CategoryCmoID = Convert.ToString(rdr["CategoryCmoID"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);
            record.CurrentL12Revenue = Convert.ToDecimal(rdr["CurrentL12Revenue"]);
            record.PriceToQuote = rdr.GetDecimalIfHas("Price");
            record.SalesPack = rdr.GetIntIfHas("SalesPack");
            record.RevisedPartNumber = rdr.GetStringIfHas("RevisedPartNumber");
            record.RevisedPool = rdr.GetStringIfHas("RevisedPool");
            record.ProjectedMonthlyUnits = Convert.ToString(rdr["EstimatedMonthlyUsage"]);
            record.Estimated3MonthUsageWithoutSalesPack = Convert.ToString(rdr["Estimated3MonthUsageWithoutSalesPack"]);
            record.Estimated3MonthUsageWithSalesPack = Convert.ToString(rdr["Estimated3MonthUsageWithSalesPack"]);
            return record;
        }

        public string SubmitInventoryReview(int bidId, string userRole, string userName)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_Submit", conn))
                {
                    cmd.CommandTimeout = 300;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidID", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    cmd.Parameters.Add("@NewStatus", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    return Convert.ToString(cmd.Parameters["@NewStatus"].Value);
                }
            }
        }

        public bool HasRevisedParts(int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_HasRevisedParts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidID", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    return Convert.ToBoolean(cmd.ExecuteScalar());
                }
            }
        }

        public bool GetLineReviewAction(int bidId, string userRole, string userName)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_GetAction", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidID", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();
                    return Convert.ToBoolean(cmd.ExecuteScalar());
                }
            }
        }

        public List<CrossPartInventoryEntity> GetPartsNotSetup(int bidId)
        {
            List<CrossPartInventoryEntity> lstCrossPartInventory = new List<CrossPartInventoryEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.PartsNotSetup_GetParts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstCrossPartInventory.Add(CreateCrossPartInventoryEntity(rdr));
                        }
                    }
                }
            }

            return lstCrossPartInventory;
        }

        public int SendToDemandTeam(int bidId, string userRole, string userName)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventorySetup_SendToDemandTeam", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidID", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    conn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public void Save(CrossPartInventoryEntity item, int bidId, string userId, string userRole)
        {
            DataTable crossPartInventoryBulk = new DataTable("CrossPartInventoryBulk");

            crossPartInventoryBulk.Columns.Add(new DataColumn("BidId", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPartId", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("BidPartId", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CustomerPartNumber", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("ManufacturerPartNumber", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPoolNumber", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPartNumber", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CrossPrice", typeof(decimal)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("Location", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("CustPartUOM", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("EstimatedAnnualUsage", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("NewPool", typeof(int)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("NewPartNo", typeof(string)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("NewPrice", typeof(decimal)));
            crossPartInventoryBulk.Columns.Add(new DataColumn("IsActive", typeof(string)));

            DataRow dr = crossPartInventoryBulk.NewRow();
            dr["BidId"] = bidId;
            dr["CustomerPartNumber"] = item.CustomerPartNumber;
            dr["ManufacturerPartNumber"] = item.ManufacturerPartNumber;
            dr["CrossPoolNumber"] = item.CrossPoolNumber;
            dr["CrossPartNumber"] = item.CrossPartNumber;
            dr["CrossPrice"] = item.CrossPrice != null ? item.CrossPrice : (object)DBNull.Value;
            dr["Location"] = item.Location.ToUpper();
            dr["CustPartUOM"] = item.CustPartUOM;
            dr["EstimatedAnnualUsage"] = item.EstimatedAnnualUsage;
            dr["NewPool"] = item.NewPool != null ? item.NewPool : (object)DBNull.Value;
            dr["NewPartNo"] = item.NewPartNo;
            dr["NewPrice"] = item.NewPrice != null ? item.NewPrice : (object)DBNull.Value;
            dr["IsActive"] = !string.IsNullOrWhiteSpace(item.IsActive) && item.IsActive.Trim().ToLower().Contains('n') ? "No" : "Yes";
            crossPartInventoryBulk.Rows.Add(dr);

            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Inventory_SaveCrossPartsInventory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartInventoryUDT", SqlDbType.Structured)).Value = crossPartInventoryBulk;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.NVarChar)).Value = userRole;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int UpdateCrossPartInventoryReview(int id, int revisedPool, string revisedPartNumber, string comments, string userId, string role)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InventoryReview_Update", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartInventoryId", SqlDbType.Int)).Value = id;
                    cmd.Parameters.Add(new SqlParameter("@RevisedPool", SqlDbType.Int)).Value = revisedPool;
                    cmd.Parameters.Add(new SqlParameter("@RevisedPartNumber", SqlDbType.VarChar)).Value = revisedPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@Role", SqlDbType.VarChar)).Value = role;
                    cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.VarChar)).Value = comments;
                    conn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }
    }
}