﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using FleetPride.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public class ReportsRepository : IReportsRepository
    {
        private string _bmConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
            }
        }

        /// <summary>
        /// Method to get all bids for Demand Team Report
        /// </summary>
        /// <param name="userRole"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public List<BidEntity> DemandTeamReportBids(string userRole, string userName)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Reports_GetBids_DemandTeamReport", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = BidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }

            return lstBid;
        }

        public List<BidEntity> RevisePartsBids(string userRole, string userName)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.ReviseParts_GetBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userName;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = BidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }

            return lstBid;
        }

        public List<CrossPartInventoryEntity> GetRevisePartsReport(int bidId)
        {
            List<CrossPartInventoryEntity> lstRevisePartsReport = new List<CrossPartInventoryEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Reports_GetRevisedParts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstRevisePartsReport.Add(CreateCrossPartInventoryEntity(rdr));
                        }
                    }
                }
            }

            return lstRevisePartsReport;
        }


        public bool LaunchBid(int bidId, string launchedByUserId)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.InsertBidLaunchDetails", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@bidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@launchedByUserId", SqlDbType.VarChar)).Value = launchedByUserId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            result = rdr.GetIntIfHas("RESULT");
                        }
                    }
                }
            }
            return result > 0;
        }


        /// <summary>
        /// Method to get demand team report from database
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="approvalType"></param>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public List<DemandTeamReportEntity> GetDemandTeamReport(int bidId, string approvalType, string userId, string userRole)
        {
            List<DemandTeamReportEntity> lstDemandTeamReport = new List<DemandTeamReportEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Reports_GetDemandTeamReport", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@ApprovalType", SqlDbType.Bit)).Value = approvalType == "Approved" ? 1 : 0;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstDemandTeamReport.Add(CreateDemandTeamReportEntity(rdr));
                        }
                    }
                }
            }

            return lstDemandTeamReport;
        }

        /// <summary>
        /// Method to get Bid Status Report
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public List<BidStatusReportEntity> GetBidStatusReport(string userId, string userRole)
        {
            List<BidStatusReportEntity> lstBidStatusReportInventory = new List<BidStatusReportEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Reports_GetBidStatusReport", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstBidStatusReportInventory.Add(CreateBidStatusReportEntity(rdr));
                        }
                    }
                }
            }
            return lstBidStatusReportInventory;
        }

        /// <summary>
        /// common entity to fetch the bids from data reader
        /// </summary>
        /// <param name="rdr"></param>
        /// <returns></returns>
        public BidEntity BidEntity(IDataReader rdr)
        {
            BidEntity record = new BidEntity();
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);

            //set the Bid status
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;

            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
            record.CalculatedDueDate = Convert.ToString(rdr["CalculatedDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CalculatedDueDate"]).ToString("MM/dd/yyyy");
            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
            record.Rank = Convert.ToString(rdr["Rank"]);
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.RequestType = Convert.ToString(rdr["RequestType"]);
            record.SubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("MM/dd/yyyy");
            record.FPFacingLocations = Convert.ToString(rdr["FPFacingLocations"]);
            record.TotalValueOfBid = Convert.ToString(rdr["TotalValueOfBid"]);
            record.IsFPPriceHold = Convert.ToBoolean(rdr["IsFPPriceHold"]);
            record.HoldDate = Convert.ToString(rdr["HoldDate"]) == "" ? "" : Convert.ToDateTime(rdr["HoldDate"]).ToString("MM/dd/yyyy");
            record.IsCustomerAllowSubstitutions = Convert.ToBoolean(rdr["IsCustomerAllowSubstitutions"]);
            record.BrandPreference = Convert.ToString(rdr["BrandPreference"]);
            record.IsCustomerInventoryPurchaseGuarantee = Convert.ToBoolean(rdr["IsCustomerInventoryPurchaseGuarantee"]);
            record.LevelOfInvetoryDetailCustomerCanProvide = Convert.ToString(rdr["LevelOfInvetoryDetailCustomerCanProvide"]);
            record.SourceExcelFileName = Convert.ToString(rdr["SourceExcelFileName"]);
            record.CreatedByRole = Convert.ToString(rdr["CreatedByRole"]);
            record.NAMUserID = Convert.ToString(rdr["NAMUserID"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);

            return record;
        }

        /// <summary>
        /// common entity to fetch the parts from data reader
        /// </summary>
        /// <param name="rdr"></param>
        /// <returns></returns>
        private CrossPartInventoryEntity CreateCrossPartInventoryEntity(SqlDataReader rdr)
        {
            CrossPartInventoryEntity record = new CrossPartInventoryEntity();
            record.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
            record.ManufacturerPartNumber = Convert.ToString(rdr["ManufacturerPartNumber"]);
            record.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
            record.PartDescription = Convert.ToString(rdr["PartDescription"]);
            record.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
            record.CrossPoolNumber = Convert.ToString(rdr["CrossPoolNumber"]);
            record.CrossPartNumber = Convert.ToString(rdr["CrossPartNumber"]);
            record.CrossPrice = Convert.ToString(rdr["CrossPrice"]);
            record.StockingType = rdr.GetStringIfHas("StockingType");
            record.CrossPartInventoryId = Convert.ToInt32(rdr["CrossPartInventoryId"] == DBNull.Value ? 0 : Convert.ToInt32(rdr["CrossPartInventoryId"]));
            record.Location = Convert.ToString(rdr["Location"]);
            record.CustPartUOM = Convert.ToString(rdr["CustPartUOM"]);
            record.EstimatedAnnualUsage = rdr.GetIntIfHas("EstimatedAnnualUsage"); //Convert.ToString(rdr["EstimatedAnnualUsage"] == DBNull.Value ? "" : Convert.ToString(rdr["EstimatedAnnualUsage"]));
            record.NewPool = Convert.ToString(rdr["NewPool"]);
            record.NewPartNo = Convert.ToString(rdr["NewPartNo"]);
            record.NewPrice = Convert.ToString(rdr["NewPrice"] == DBNull.Value ? "" : Convert.ToString(rdr["NewPrice"]));
            record.NewStockingType = rdr.GetStringIfHas("NewStockingType");
            record.IsActive = rdr.GetStringIfHas("IsActive");
            record.LastUpdatedBy = Convert.ToString(rdr["LastUpdatedBy"]);
            record.LastUpdatedOn = Convert.ToDateTime(rdr["LastUpdatedOn"]).ToString("yyyy-dd-MM HH:mm");
            record.RevisePartComments = rdr.GetStringIfHas("Comments");
            record.RevisedPool = rdr.GetStringIfHas("RevisedPool");
            record.RevisedPartNumber = rdr.GetStringIfHas("RevisedPartNumber");
            return record;
        }

        /// <summary>
        /// common entity to fetch the parts from data reader
        /// </summary>
        /// <param name="rdr"></param>
        /// <returns></returns>
        private BidStatusReportEntity CreateBidStatusReportEntity(SqlDataReader rdr)
        {
            BidStatusReportEntity record = new BidStatusReportEntity();
            record.Id = Convert.ToInt32(rdr["BidPartId"]);
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;
            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);             
            record.LaunchDate = rdr.IsDBNull("LaunchDate") || Convert.ToString(rdr["LaunchDate"]) == "" ? "" : Convert.ToDateTime(rdr["LaunchDate"]).ToString("MM/dd/yyyy");
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
            record.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
            record.Manufacturer = Convert.ToString(rdr["Manufacturer"]);
            record.ManufacturerPartNumber = Convert.ToString(rdr["ManufacturerPartNumber"]);           
            record.PartDescription = Convert.ToString(rdr["PartDescription"]);
            record.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
            record.CrossPoolNumber = Convert.ToString(rdr["CrossPoolNumber"]);
            record.CrossPartNumber = Convert.ToString(rdr["CrossPartNumber"]);
            record.CrossPartDescription = Convert.ToString(rdr["CrossPartDescription"]);
            record.PriceToQuote = Convert.ToString(rdr["PriceToQuote"]);
            record.CrossPartInventoryId = rdr.GetIntIfHas("CrossPartInventoryId");
            record.Location = Convert.ToString(rdr["Location"]);
            record.CustPartUOM = Convert.ToString(rdr["CustPartUOM"]);
            record.EstimatedAnnualUsage = rdr.GetStringIfHas("EstimatedAnnualUsage");
            record.NewPool = Convert.ToString(rdr["NewPool"] == DBNull.Value ? "" : Convert.ToString(rdr["NewPool"]));
            record.NewPartNo = Convert.ToString(rdr["NewPartNo"]);
            record.NewPrice = Convert.ToString(rdr["NewPrice"] == DBNull.Value ? "" : Convert.ToString(rdr["NewPrice"]));
            record.IsActive = Convert.ToString(rdr["IsActive"]);
            record.UpdatedBy = Convert.ToString(rdr["UpdatedBy"]);
            record.UpdatedOn = Convert.ToDateTime(rdr["UpdatedOn"]).ToString("MM/dd/yyyy");
            record.IType = Convert.ToString(rdr["IType"]);
            record.NewIType = Convert.ToString(rdr["NewIType"]);
            record.CrossPartLineReviewId = rdr.GetIntIfHas("CrossPartLineReviewId");
            record.DCSafetyStock = rdr.GetIntIfHas("DCSafetyStock");
            record.NPPartType = Convert.ToString(rdr["NPPartType"]);
            record.NewNPPartType = Convert.ToString(rdr["NewNPPartType"]);
            record.CrossSupersedeNote = Convert.ToString(rdr["CrossSupersedeNote"]);
            record.NewSupersedeNote = Convert.ToString(rdr["NewSupersedeNote"]);
            record.CategoryManagerID = Convert.ToString(rdr["CategoryManagerID"]);
            record.CategoryManagerName = Convert.ToString(rdr["CategoryManagerName"]);
            record.IsApproved = rdr.GetBoolIfHas("IsApproved");
            record.ParentKit = rdr.GetStringIfHas("ParentKit");
            return record;
        }

        /// <summary>
        /// common entity to fetch the parts from data reader
        /// </summary>
        /// <param name="rdr"></param>
        /// <returns></returns>
        private DemandTeamReportEntity CreateDemandTeamReportEntity(SqlDataReader rdr)
        {
            DemandTeamReportEntity record = new DemandTeamReportEntity();
            record.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
            record.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
            record.ManufacturerPartNumber = Convert.ToString(rdr["ManufacturerPartNumber"]);           
            record.PartDescription = Convert.ToString(rdr["PartDescription"]);
            record.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
            record.Pool = Convert.ToString(rdr["Pool"]);
            record.PartNumber = Convert.ToString(rdr["PartNumber"]);
            record.Price = Convert.ToString(rdr["Price"]);
            record.StockingType = rdr.GetStringIfHas("StockingType");
            record.CrossPartInventoryId = Convert.ToInt32(rdr["CrossPartInventoryId"] == DBNull.Value ? 0 : Convert.ToInt32(rdr["CrossPartInventoryId"]));
            record.Location = Convert.ToString(rdr["Location"]);
            record.CustPartUOM = Convert.ToString(rdr["CustPartUOM"]);
            record.EstimatedAnnualUsage = Convert.ToString(rdr["EstimatedAnnualUsage"] == DBNull.Value ? "" : Convert.ToString(rdr["EstimatedAnnualUsage"]));
            record.OldPool = Convert.ToString(rdr["OldPool"]);
            record.OldPartNumber = Convert.ToString(rdr["OldPartNumber"]);
            record.OldPrice = Convert.ToString(rdr["OldPrice"] == DBNull.Value ? "" : Convert.ToString(rdr["OldPrice"]));
            record.OldStockingType = rdr.GetStringIfHas("OldStockingType");
            record.IsActive = rdr.GetStringIfHas("IsActive");
            record.CategoryManagerID = rdr.GetStringIfHas("CategoryManagerID");
            record.LastUpdatedBy = Convert.ToString(rdr["LastUpdatedBy"]);
            record.LastUpdatedOn = Convert.ToDateTime(rdr["LastUpdatedOn"]).ToString("yyyy-dd-MM HH:mm");
            record.ParentKit = rdr.GetStringIfHas("ParentKit");
            return record;
        }

        public List<CrossPartInventoryHistoryEntity> GetBidChangeReport(int bidId)
        {
            List<CrossPartInventoryHistoryEntity> lstBidStatusReportInventory = new List<CrossPartInventoryHistoryEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.BidChangeReport_GetCrossInventoryHistory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            lstBidStatusReportInventory.Add(CreateCrossInventoryHistoryEntity(rdr));
                        }
                    }
                }
            }
            return lstBidStatusReportInventory;
        }

        private CrossPartInventoryHistoryEntity CreateCrossInventoryHistoryEntity(SqlDataReader rdr)
        {
            CrossPartInventoryHistoryEntity record = new CrossPartInventoryHistoryEntity();
            record.ID = rdr.GetIntIfHas("Id");
            record.BidPartId = rdr.GetIntIfHas("BidPartId");
            record.ManufacturerPartNumber = rdr.GetStringIfHas("ManufacturerPartNumber");
            record.CustomerPartNumber = rdr.GetStringIfHas("CustomerPartNumber");
            record.PartDescription = rdr.GetStringIfHas("PartDescription");
            record.CrossPartId = rdr.GetIntIfHas("CrossPartId");
            record.CrossPoolNumber = rdr.GetStringIfHas("CrossPoolNumber");
            record.CrossPartNumber = rdr.GetStringIfHas("CrossPartNumber");
            record.CrossPartDescription = rdr.GetStringIfHas("CrossPartDescription");
            record.CrossPrice = rdr.GetStringIfHas("CrossPrice");
            record.CrossICOST = rdr.GetStringIfHas("CrossICOST");
            record.CrossPartInventoryId = rdr.GetIntIfHas("CrossPartInventoryId");
            record.Location = rdr.GetStringIfHas("Location");
            record.CustPartUOM = rdr.GetStringIfHas("CustPartUOM");
            record.EstimatedAnnualUsage = rdr.GetStringIfHas("EstimatedAnnualUsage");
            record.NewPool = rdr.GetStringIfHas("NewPool");
            record.NewPartNo = rdr.GetStringIfHas("NewPartNo");
            record.NewPrice = rdr.GetStringIfHas("NewPrice");
            record.IsActive = rdr.GetStringIfHas("IsActive");
            record.StockingType = rdr.GetStringIfHas("StockingType");
            record.NewStockingType = rdr.GetStringIfHas("NewStockingType");
            record.EventType = rdr.GetStringIfHas("EventType");
            record.ActionType = rdr.GetStringIfHas("ActionType");
            record.ColumnsUpdated = rdr.GetStringIfHas("ColumnsUpdated");
            record.LastUpdatedBy = rdr.GetStringIfHas("LastUpdatedBy");
            record.LastUpdatedOn = rdr.GetDateTimeIfHas("LastUpdatedOn").ToString("yy-MM-dd HH:mm");
            record.Rank = rdr.GetIntIfHas("Rank");

            return record;
        }
    }
}