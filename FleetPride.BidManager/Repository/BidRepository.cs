﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using FleetPride.Extensions;

namespace FleetPride.BidManager.Repository
{
    public class BidRepository : IBidRepository
    {
        private string _bmConnString;

        /// <summary>
        /// Default constructer
        /// </summary>
        public BidRepository()
        {
            _bmConnString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
        }

        public BidRepository(string connString)
        {
            _bmConnString = connString;
        }

        /// <summary>
        /// Method to get all bids from DB
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public List<BidEntity> GetAllBids(string userRole, string userId)
        {
            List<BidEntity> lstBid = new List<BidEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_GetAllBids", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidEntity record = CreateBidEntity(rdr);
                            lstBid.Add(record);
                        }
                    }
                }
            }
            return lstBid;
        }

        /// <summary>
        /// Method to get all customer info from DB
        /// </summary>
        /// <param name="userRole"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Customer> GetAllCustInfo(string bidType, int company, string userRole, string userId)
        {
            List<Customer> lstCust = new List<Customer>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_GetAllCustInfo", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@Company", SqlDbType.Int)).Value = company;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    cmd.Parameters.Add(new SqlParameter("@BidType", SqlDbType.VarChar)).Value = bidType;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Customer c = new Customer();
                            c.CustomerInfo = Convert.ToString(rdr["CustInfo"]);
                            lstCust.Add(c);
                        }
                    }
                }
            }

            return lstCust;
        }

        /// <summary>
        /// Method to create a Bid in DB
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public int CreateBid(BidEntity record, string userId, string userRole)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_CreateBid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidType", SqlDbType.VarChar)).Value = record.BidType;
                    cmd.Parameters.Add(new SqlParameter("@IsProspect", SqlDbType.Bit)).Value = record.IsProspect;
                    cmd.Parameters.Add(new SqlParameter("@BidStatus", SqlDbType.VarChar)).Value = record.BidStatus;
                    cmd.Parameters.Add(new SqlParameter("@Company", SqlDbType.Int)).Value = record.Company;
                    cmd.Parameters.Add(new SqlParameter("@CustNumber", SqlDbType.Int)).Value = record.CustNumber;
                    cmd.Parameters.Add(new SqlParameter("@CustBranch", SqlDbType.Int)).Value = record.CustBranch;
                    cmd.Parameters.Add(new SqlParameter("@CustName", SqlDbType.VarChar)).Value = record.CustName;
                    cmd.Parameters.Add(new SqlParameter("@CustCorpId", SqlDbType.Int)).Value = record.CustCorpId;
                    cmd.Parameters.Add(new SqlParameter("@CustCorpAccName", SqlDbType.VarChar)).Value = record.CustCorpAccName;
                    cmd.Parameters.Add(new SqlParameter("@BidName", SqlDbType.VarChar)).Value = record.BidName;
                    cmd.Parameters.Add(new SqlParameter("@RequestType", SqlDbType.VarChar)).Value = record.RequestType;
                    cmd.Parameters.Add(new SqlParameter("@SubmittedDate", SqlDbType.DateTime)).Value = record.SubmittedDate;
                    cmd.Parameters.Add(new SqlParameter("@BidDescription", SqlDbType.VarChar)).Value = record.BidDescription;
                    cmd.Parameters.Add(new SqlParameter("@FPFacingLocations", SqlDbType.Int)).Value = record.FPFacingLocations;
                    cmd.Parameters.Add(new SqlParameter("@IsFPPriceHold", SqlDbType.Bit)).Value = record.IsFPPriceHold;
                    cmd.Parameters.Add(new SqlParameter("@DueDate", SqlDbType.DateTime)).Value = record.DueDate;
                    cmd.Parameters.Add(new SqlParameter("@TotalValueOfBid", SqlDbType.Decimal)).Value = record.TotalValueOfBid;
                    cmd.Parameters.Add(new SqlParameter("@IsCustomerInventoryPurchaseGuarantee", SqlDbType.Bit)).Value = record.IsCustomerInventoryPurchaseGuarantee;
                    cmd.Parameters.Add(new SqlParameter("@HoldDate", SqlDbType.DateTime)).Value = record.HoldDate;
                    cmd.Parameters.Add(new SqlParameter("@LevelOfInvetoryDetailCustomerCanProvide", SqlDbType.VarChar)).Value = record.LevelOfInvetoryDetailCustomerCanProvide;
                    cmd.Parameters.Add(new SqlParameter("@IsCustomerAllowSubstitutions", SqlDbType.Bit)).Value = record.IsCustomerAllowSubstitutions;
                    cmd.Parameters.Add(new SqlParameter("@BrandPreference", SqlDbType.VarChar)).Value = record.BrandPreference;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    rowAffected = (int)cmd.Parameters["@Result"].Value;

                    return rowAffected;
                }
            }
        }

        /// <summary>
        /// Method to save uploaded excel in DB
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="userId"></param>
        public void SaveUploadedExcel(string fileName, int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_UpdateExcelFileName", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@FileName", SqlDbType.VarChar)).Value = fileName;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Method to update bid rank in db
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="oldRank"></param>
        /// <param name="newRank"></param>
        /// <returns></returns>
        public int UpdateBid(string bidId, string oldRank, string newRank, string dueDate, string phaseDueDate, string totalValueOfBid, string projectedGPDollar, string projectedGPPerc, string userId)
        {
            int rowAffected = 0;
            if (totalValueOfBid == "")
            {
                totalValueOfBid = null;
            }
            if (projectedGPDollar == "")
            {
                projectedGPDollar = null;
            }
            if (projectedGPPerc == "")
            {
                projectedGPPerc = null;
            }
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_UpdateBid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@OldRank", SqlDbType.Int)).Value = oldRank;
                    cmd.Parameters.Add(new SqlParameter("@NewRank", SqlDbType.Int)).Value = newRank;
                    cmd.Parameters.Add(new SqlParameter("@DueDate", SqlDbType.Date)).Value = dueDate;
                    cmd.Parameters.Add(new SqlParameter("@PhaseDueDate", SqlDbType.Date)).Value = phaseDueDate;
                    cmd.Parameters.Add(new SqlParameter("@TotalValueOfBid", SqlDbType.VarChar)).Value = totalValueOfBid;
                    cmd.Parameters.Add(new SqlParameter("@ProjectedGPDollar", SqlDbType.VarChar)).Value = projectedGPDollar;
                    cmd.Parameters.Add(new SqlParameter("@ProjectedGPPerc", SqlDbType.VarChar)).Value = projectedGPPerc;
                    cmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteNonQuery();

                    rowAffected = (int)cmd.Parameters["@Result"].Value;

                    return rowAffected;
                }
            }
        }

        /// <summary>
        /// Method to Insert uploaded bid part in bulk in database
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userId"></param>
        /// <param name="userRole"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public int BulkInsertBidParts(DataTable bidParts, int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (var cmd = new SqlCommand("BM.UploadBidPart_BulkInsertBidParts", conn))
                {
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidParts", SqlDbType.Structured)).Value = bidParts;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return 1;
        }

        /// <summary>
        /// Method to get Bid parts list from database
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public List<BidPartEntity> GetBidPartsList(int bidId, string userId, string userRole)
        {
            List<BidPartEntity> lstBidParts = new List<BidPartEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.UploadBidPart_GetBidPartsList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            BidPartEntity record = CreateBidPartEntity(rdr);
                            lstBidParts.Add(record);
                        }
                    }
                }
            }
            return lstBidParts;
        }

        /// <summary>
        /// Method to insert bid part in database
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="customerPartNumber"></param>
        /// <param name="partDescription"></param>
        /// <param name="manufacturer"></param>
        /// <param name="manufacturerPartNumber"></param>
        /// <param name="note"></param>
        /// <param name="estimatedAnnualUsage"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int InsertBidPart(int bidId, string customerPartNumber, string partDescription, string manufacturer, string manufacturerPartNumber, string note, int? estimatedAnnualUsage, string userId)
        {
            int rowAffected = 0;


            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.UploadBidPart_InsertBidPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@CustomerPartNumber", SqlDbType.NVarChar)).Value = customerPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@PartDescription", SqlDbType.NVarChar)).Value = partDescription;
                    cmd.Parameters.Add(new SqlParameter("@Manufacturer", SqlDbType.NVarChar)).Value = manufacturer;
                    cmd.Parameters.Add(new SqlParameter("@ManufacturerPartNumber", SqlDbType.NVarChar)).Value = manufacturerPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@Note", SqlDbType.NVarChar)).Value = note;
                    cmd.Parameters.Add(new SqlParameter("@EstimatedAnnualUsage", SqlDbType.Int)).Value = estimatedAnnualUsage;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        /// <summary>
        /// Method to update existing bid part in database
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <param name="customerPartNumber"></param>
        /// <param name="partDescription"></param>
        /// <param name="manufacturer"></param>
        /// <param name="manufacturerPartNumber"></param>
        /// <param name="note"></param>
        /// <param name="estimatedAnnualUsage"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateBidPart(int bidPartId, string customerPartNumber, string partDescription, string manufacturer, string manufacturerPartNumber, string note, int? estimatedAnnualUsage, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.UploadBidPart_UpdateBidPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add(new SqlParameter("@CustomerPartNumber", SqlDbType.NVarChar)).Value = customerPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@PartDescription", SqlDbType.NVarChar)).Value = partDescription;
                    cmd.Parameters.Add(new SqlParameter("@Manufacturer", SqlDbType.NVarChar)).Value = manufacturer;
                    cmd.Parameters.Add(new SqlParameter("@ManufacturerPartNumber", SqlDbType.NVarChar)).Value = manufacturerPartNumber;
                    cmd.Parameters.Add(new SqlParameter("@Note", SqlDbType.NVarChar)).Value = note;
                    cmd.Parameters.Add(new SqlParameter("@EstimatedAnnualUsage", SqlDbType.Int)).Value = estimatedAnnualUsage;
                    cmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        /// <summary>
        /// Method to delete bid part from database
        /// </summary>
        /// <param name="bidPartId"></param>
        public int DeleteBidPart(int bidPartId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.UploadBidPart_DeleteBidPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        /// <summary>
        /// Method to generate crosses for the bid parts from database
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public void GenerateCrossesForBidPartsSegmented(int bidId, string userId)
        {
            int bidPartsCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["MaxRecordPerCall"]);

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.GenerateCrosses_GetBidPartsCount", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    bidPartsCount = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }

            if (bidPartsCount > 0)
            {
                int totalPages = (int)Math.Ceiling((decimal)bidPartsCount / pageSize);
                if (totalPages == 0)
                    totalPages = 1;

                int startIndex = 1;
                int endIndex = pageSize;

                for (int pageNo = 1; pageNo <= totalPages; pageNo++)
                {
                    GenerateCrossesForBidParts(bidId, userId, startIndex, endIndex);
                    startIndex = endIndex + 1;
                    endIndex = endIndex + pageSize;
                }
            }
        }

        public void GenerateCrossesForBidParts(int bidId, string userId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.UploadBidPart_GenerateCrosses", conn))
                {
                    cmd.CommandTimeout = 36000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    conn.Open();
                    int result = cmd.ExecuteNonQuery();
                }
            }
        }

        private void GenerateCrossesForBidParts(int bidId, string userId, int startIndex, int endIndex)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.UploadBidPart_GenerateCrossesSegmented", conn))
                {
                    cmd.CommandTimeout = 36000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@StartIndex", SqlDbType.Int)).Value = startIndex;
                    cmd.Parameters.Add(new SqlParameter("@EndIndex", SqlDbType.Int)).Value = endIndex;
                    conn.Open();
                    int result = cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Get the status of a bid
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public CustomEnum GetBidStatus(int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.AvailableCrosses_GetBidStatus", conn))
                {
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            if (!DBNull.Value.Equals(rdr["BidStatus"]))
                                return Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
                        }
                    }
                }
            }

            return Constants.BidStatusEnum.Get(Constants.NotFound);
        }

        /// <summary>
        /// Get a bid by its bid id
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public BidEntity GetBid(int bidId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_GetBid", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@bidId", SqlDbType.VarChar)).Value = bidId;
                    conn.Open();

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            return CreateBidEntity(rdr);
                        }
                    }
                }
            }

            return null;
        }

        public void SubmitForCrowdSourcing(List<BidPartEntity> bidParts)
        {
            DataTable IDs = new DataTable("IDs");
            IDs.Columns.Add(new DataColumn("ID", typeof(int)));
            foreach (BidPartEntity bp in bidParts)
            {
                DataRow dr = IDs.NewRow();
                dr["ID"] = bp.BidPartId;
                IDs.Rows.Add(dr);
            }

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.AvailableCrosses_SubmitForCrowdSourcing", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartIds", SqlDbType.Structured)).Value = IDs;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Submit(int bidId, string newStatus, string comments)
        {
            Tat tat = GetTat(bidId, newStatus);
            DateTime phaseDueDate = DateTime.Now.AddWorkdays(tat.PhaseTat);
            DateTime calculatedDueDates = DateTime.Now.AddWorkdays(tat.TotalTat).GetNextMonday();

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_Submit", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@NewStatus", SqlDbType.NVarChar)).Value = newStatus;
                    cmd.Parameters.Add(new SqlParameter("@PhaseDueDate", SqlDbType.DateTime)).Value = phaseDueDate;
                    cmd.Parameters.Add(new SqlParameter("@CalculatedDueDate", SqlDbType.DateTime)).Value = calculatedDueDates;
                    cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.NVarChar)).Value = comments;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Complete(int bidId, string status, string comments)
        {
            using(SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using(SqlCommand cmd = new SqlCommand("BM.Bid_Complete", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 36000;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@NewStatus", SqlDbType.NVarChar)).Value = status;
                    cmd.Parameters.Add(new SqlParameter("@Comments", SqlDbType.NVarChar)).Value = comments;
                    conn.Open();

                    string oppId = Convert.ToString(cmd.ExecuteScalar());
                    //if(!string.IsNullOrEmpty(oppId) && (status == Constants.Won || status == Constants.Lost))
                    //{
                    //    OpportunityEntity oppEntity = new OpportunityEntity();
                    //    oppEntity.OppId = oppId;
                    //    oppEntity.OpportunityStage = (status == Constants.Won ? Constants.OpportunityStage.Won : Constants.OpportunityStage.Lost);
                    //    OpportunityRepository oppRepo = new OpportunityRepository();
                    //    oppRepo.Update(oppEntity);
                    //}
                }
            }
        }

        public Tat GetTat(int bidId, string newStatus)
        {
            Tat tat = new Tat();

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_GetTat", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@NewStatus", SqlDbType.NVarChar)).Value = newStatus;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        tat.PhaseTat = Convert.ToInt32(dr["PhaseTat"] == DBNull.Value ? 0 : dr["PhaseTat"]);
                        tat.TotalTat = Convert.ToInt32(dr["TotalTat"] == DBNull.Value ? 0 : dr["TotalTat"]);
                    }
                }
            }

            return tat;
        }

        public BidPartEntity CreateBidPartEntity(SqlDataReader rdr)
        {
            var record = new BidPartEntity();
            record.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
            record.PartDescription = Convert.ToString(rdr["PartDescription"]);
            record.Manufacturer = Convert.ToString(rdr["Manufacturer"]);
            record.ManufacturerPartNumber = Convert.ToString(rdr["ManufacturerPartNumber"]);
            record.Note = Convert.ToString(rdr["Note"]);
            record.EstimatedAnnualUsage = Convert.ToString(rdr["EstimatedAnnualUsage"]);
            record.IsFinalized = rdr["IsFinalized"] == DBNull.Value ? "" : Convert.ToString(rdr["IsFinalized"]);
            record.IsVerified = rdr["IsVerified"] == DBNull.Value ? false : Convert.ToBoolean(rdr["IsVerified"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.CMUserId = rdr["CMUserId"] == DBNull.Value ? string.Empty : Convert.ToString(rdr["CMUserId"]);

            if (record.IsFinalized == "True" &&  !rdr.IsDBNull("CrossPartId"))
            {
                record.PrimaryCross = new CrossEntity(rdr);
                //record.PrimaryCross_CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
                //record.PrimaryCross_BidPartId = Convert.ToInt32(rdr["BidPartId"]);
                //record.PrimaryCross_CrossRank = Convert.ToString(rdr["CrossRank"] == DBNull.Value ? 0 : rdr["CrossRank"]);
                ////record.PrimaryCross_ProductKey = Convert.ToInt32(rdr["ProductKey"]);
                //record.PrimaryCross_ReferenceTableName = Convert.ToString(rdr["ReferenceTableName"]);
                //record.PrimaryCross_Source = Convert.ToString(rdr["Source"]);
                //record.PrimaryCross_Company = Convert.ToString(rdr["Company"]);
                //record.PrimaryCross_PartNumber = Convert.ToString(rdr["PartNumber"]);
                //record.PrimaryCross_PoolNumber = Convert.ToString(rdr["PoolNumber"]);
                //record.PrimaryCross_PartDescription = Convert.ToString(rdr["PartDescription"]);
                //record.PrimaryCross_PartCategory = Convert.ToString(rdr["PartCategory"]);
                //record.PrimaryCross_VendorNumber = Convert.ToString(rdr["VendorNumber"]);
                //record.PrimaryCross_VendorColor = Convert.ToString(rdr["VendorColor"]);
                //record.PrimaryCross_VendorName = Convert.ToString(rdr["VendorName"]);
                //record.PrimaryCross_VendorColorDefinition = Convert.ToString(rdr["VendorColorDefinition"]);
                //record.PrimaryCross_Manufacturer = Convert.ToString(rdr["Manufacturer"]);
                //record.PrimaryCross_IsFlip = Convert.ToBoolean(rdr["IsFlip"] == DBNull.Value ? false : rdr["IsFlip"]);
                //record.PrimaryCross_IsNonFP = Convert.ToBoolean(rdr["IsNonFP"] == DBNull.Value ? false : rdr["IsNonFP"]);
                //record.PrimaryCross_ValidationStatus = Convert.ToString(rdr["ValidationStatus"] == DBNull.Value ? Constants.ValidationAwaited : rdr["ValidationStatus"]);
                //record.PrimaryCross_ValidatedBy = Convert.ToString(rdr["ValidatedBy"] == DBNull.Value ? "" : rdr["ValidatedBy"]);
                //record.PrimaryCross_ValidatedOn = Convert.ToString(rdr["ValidatedOn"] == DBNull.Value ? "" : rdr["ValidatedOn"]);
                //record.PrimaryCross_CreatedBy = Convert.ToString(rdr["CreatedBy"] == DBNull.Value ? "" : rdr["CreatedBy"]);
                //record.PrimaryCross_CreatedOn = Convert.ToString(rdr["CreatedOn"] == DBNull.Value ? "" : rdr["CreatedOn"]);
                //record.PrimaryCross_FinalPreference = Convert.ToString(rdr["FinalPreference"] == DBNull.Value ? "" : rdr["FinalPreference"]);
                //record.PrimaryCross_Comments = Convert.ToString(rdr["Comments"] == DBNull.Value ? "" : rdr["Comments"]);
                //record.PrimaryCross_PriceToQuote = Convert.ToString(rdr["PriceToQuote"]);
                //record.PrimaryCross_SuggestedPrice = Convert.ToString(rdr["SuggestedPrice"]);
                //record.PrimaryCross_ICost = Convert.ToString(rdr["ICOST"]);
                //record.PrimaryCross_Margin = Convert.ToString(rdr["Margin"]);
                //record.PrimaryCross_AdjustedICost = Convert.ToString(rdr["AdjustedICost"]);
                //record.PrimaryCross_AdjustedMargin = Convert.ToString(rdr["AdjustedMargin"]);
                //record.PrimaryCross_IsWon = Convert.ToString(rdr["IsWon"]);
                //record.PrimaryCross_DCStockCount = Convert.ToString(rdr["DCStockCount"] == DBNull.Value ? "" : rdr["DCStockCount"]);
                //record.PrimaryCross_StoresStockCount = Convert.ToString(rdr["StoresStockCount"] == DBNull.Value ? "" : rdr["StoresStockCount"]);
                //record.PrimaryCross_NationalL12StoresSold = Convert.ToString(rdr["NationalL12StoresSold"] == DBNull.Value ? "" : rdr["NationalL12StoresSold"]);
                //record.PrimaryCross_LocalL12StoresSold = Convert.ToString(rdr["LocalL12StoresSold"] == DBNull.Value ? "" : rdr["LocalL12StoresSold"]);
                //record.PrimaryCross_L12Units = Convert.ToString(rdr["L12Units"] == DBNull.Value ? "" : rdr["L12Units"]);
                //record.PrimaryCross_L12Revenue = Convert.ToString(rdr["L12Revenue"] == DBNull.Value ? "" : rdr["L12Revenue"]);
                //record.PrimaryCross_NPMPartType = Convert.ToString(rdr["NPMPartType"] == DBNull.Value ? "" : rdr["NPMPartType"]);
            }           

            return record;
        }

        public BidEntity CreateBidEntity(IDataReader rdr)
        {
            BidEntity record = new BidEntity();
            record.BidId = Convert.ToInt32(rdr["BidId"]);
            record.IsProspect = Convert.ToBoolean(rdr["IsProspect"]);

            //set the Bid status
            CustomEnum bidStatus = Constants.BidStatusEnum.Get(Convert.ToString(rdr["BidStatus"]));
            record.BidStatus = bidStatus.Key;
            record.BidStatusDisplay = bidStatus.ShortDescription;
            record.BidStatusDescirption = bidStatus.LongDescription;

            record.BidType = Convert.ToString(rdr["BidType"]);
            record.BidName = Convert.ToString(rdr["BidName"]);
            record.BidDescription = Convert.ToString(rdr["BidDescription"]);
            record.DueDate = Convert.ToDateTime(rdr["DueDate"]).ToString("MM/dd/yyyy");
            record.LaunchDate =  rdr.IsDBNull("LaunchDate") || Convert.ToString(rdr["LaunchDate"]) == "" ? "" : Convert.ToDateTime(rdr["LaunchDate"]).ToString("MM/dd/yyyy");
            record.ActualLaunchDate = rdr.IsDBNull("ActualLaunchDate") || Convert.ToString(rdr["ActualLaunchDate"]) == "" ? "" : Convert.ToDateTime(rdr["ActualLaunchDate"]).ToString("MM/dd/yyyy");
            record.CalculatedDueDate = Convert.ToString(rdr["CalculatedDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CalculatedDueDate"]).ToString("MM/dd/yyyy");
            record.CurrentPhaseDueDate = Convert.ToString(rdr["CurrentPhaseDueDate"]) == "" ? "" : Convert.ToDateTime(rdr["CurrentPhaseDueDate"]).ToString("MM/dd/yyyy");
            record.Rank = Convert.ToString(rdr["Rank"]);
            record.Company = Convert.ToString(rdr["Company"]);
            record.CustNumber = Convert.ToString(rdr["CustNumber"]);
            record.CustBranch = Convert.ToString(rdr["CustBranch"]);
            record.CustName = Convert.ToString(rdr["CustName"]);
            record.CustCorpId = Convert.ToString(rdr["CustCorpId"]);
            record.CustCorpAccName = Convert.ToString(rdr["CustCorpAccName"]);
            record.GroupId = Convert.ToString(rdr["GroupId"]);
            record.GroupDesc = Convert.ToString(rdr["GroupDesc"]);
            record.RequestType = Convert.ToString(rdr["RequestType"]);
            record.SubmittedDate = Convert.ToDateTime(rdr["SubmittedDate"]).ToString("MM/dd/yyyy");
            record.FPFacingLocations = Convert.ToString(rdr["FPFacingLocations"]);
            record.TotalValueOfBid = Convert.ToString(rdr["TotalValueOfBid"]);
            record.ProjectedGPDollar = Convert.ToString(rdr["ProjectedGPDollar"]);
            record.ProjectedGPPerc = Convert.ToString(rdr["ProjectedGPPerc"]);
            record.IsFPPriceHold = Convert.ToBoolean(rdr["IsFPPriceHold"]);
            record.HoldDate = Convert.ToString(rdr["HoldDate"]) == "" ? "" : Convert.ToDateTime(rdr["HoldDate"]).ToString("MM/dd/yyyy");
            record.IsCustomerAllowSubstitutions = Convert.ToBoolean(rdr["IsCustomerAllowSubstitutions"]);
            record.BrandPreference = Convert.ToString(rdr["BrandPreference"]);
            record.IsCustomerInventoryPurchaseGuarantee = Convert.ToBoolean(rdr["IsCustomerInventoryPurchaseGuarantee"]);
            record.LevelOfInvetoryDetailCustomerCanProvide = Convert.ToString(rdr["LevelOfInvetoryDetailCustomerCanProvide"]);
            record.SourceExcelFileName = Convert.ToString(rdr["SourceExcelFileName"]);
            record.CreatedByRole = Convert.ToString(rdr["CreatedByRole"]);
            record.NAMUserID = Convert.ToString(rdr["NAMUserID"]);
            record.CreatedBy = Convert.ToString(rdr["CreatedBy"]);
            record.CreatedOn = Convert.ToDateTime(rdr["CreatedOn"]).ToString("MM/dd/yyyy");
            record.Comments = Convert.ToString(rdr["Comments"]);
            record.CrossActionStatus = rdr.GetBoolIfHas("CrossActionStatus");
            record.InventoryActionStatus = rdr.GetBoolIfHas("InventoryActionStatus");
            record.CreatedByFullName = rdr["CreatedByFullName"] != null ? Convert.ToString(rdr["CreatedByFullName"]) : null;
            return record;
        }

        public List<BidEmailEntity> GetTatMissingBids()
        {
            List<BidEmailEntity> bees = new List<BidEmailEntity>();

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_GetTatMiss", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        BidEmailEntity bee = new BidEmailEntity();
                        bee.BidId = Convert.ToInt32(dr["BidId"]);
                        bee.BidName = Convert.ToString(dr["BidName"]);
                        bee.BidStatus = Convert.ToString(dr["BidStatus"]);
                        bee.BidType = Convert.ToString(dr["BidType"]);
                        bee.CreatorEmail = Convert.ToString(dr["CreatorEmail"]);
                        bee.CMEmail = Convert.ToString(dr["CMEmail"]);
                        bee.RVPEmail = Convert.ToString(dr["RVPEmail"]);
                        bee.NAMEmail = Convert.ToString(dr["NAMEmail"]);
                        bee.CmoEmail = Convert.ToString(dr["CmoEmail"]);
                        bees.Add(bee);
                    }
                }
            }

            return bees;
        }

        public void SaveOpportunityID(int bidId, string opportunityId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_SaveOpId", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@OpportunityId", SqlDbType.VarChar)).Value = opportunityId.Trim('\"');
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<UserEntity> GetBidCreatorDetails(int bidId)
        {
            List<UserEntity> users = new List<UserEntity>();

            using (SqlConnection con = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_GetNAMOrCreatorEMail", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@bidId", SqlDbType.VarChar)).Value = bidId;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        users.Add(new UserEntity
                        {
                            UserId = Convert.ToString(rdr["UserName"]),
                            UserEmail = Convert.ToString(rdr["EmailId"])
                        });
                    }
                }
            }

            return users;
        }

        public int AdminBidStatusUpdate(int bidId, string oldStatus, string newStatus)
        {
            int result = 0;
            if (bidId < 1 || string.IsNullOrWhiteSpace(oldStatus) || string.IsNullOrWhiteSpace(newStatus))
            {
                return 0;
            }

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.Bid_AdminBidStatusUpdate", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@bidId", SqlDbType.Int)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@oldStatus", SqlDbType.VarChar)).Value = oldStatus;
                    cmd.Parameters.Add(new SqlParameter("@newStatus", SqlDbType.VarChar)).Value = newStatus;
                    
                    //cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    
                    result = cmd.ExecuteNonQuery();

                }
            }
            return result;
        }
    }
}