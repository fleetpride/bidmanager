﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetPride.BidManager.Models;
using System.Net;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.IO;
using System.Text;

namespace FleetPride.BidManager.Repository
{
    public class OpportunityRepository : IOpportunityRepository
    {
        public string APIUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_ApiUrl"];
            }
        }

        public string AuthUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_AuthUrl"];
            }
        }

        public string GrantType
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_GrantType"];
            }
        }

        public string ClientID
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_ClientID"];
            }
        }

        public string ClientSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_ClientSecret"];
            }
        }

        public string UserName
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_UserName"];
            }
        }

        public string Password
        {
            get
            {
                return ConfigurationManager.AppSettings["Opportunity_Password"];
            }
        }

        public string GetAuthToken()
        {
            NameValueCollection QueryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
            QueryString.Add("grant_type", this.GrantType);
            QueryString.Add("client_id", this.ClientID);
            QueryString.Add("client_secret", this.ClientSecret);
            QueryString.Add("username", this.UserName);
            QueryString.Add("password", this.Password);

            string uri = string.Format("{0}?{1}", this.AuthUrl, QueryString.ToString());
            var request = WebRequest.CreateHttp(uri);
            request.Method = "POST";

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        string content = sr.ReadToEnd();
                        response.Close();
                        dynamic result = JsonConvert.DeserializeObject(content);
                        return result.access_token;
                    }
                }                
            }
        }

        public string Invoke(string methodName, string json)
        {
            string responseValue = "";

            var request = WebRequest.CreateHttp(this.APIUrl);
            request.Method = methodName;
            request.ContentType = "application/json";

            string token = this.GetAuthToken();
            request.Headers.Add("Authorization", "Bearer " + token);

            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            using (WebResponse response = request.GetResponse())
            {
                using (dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    responseValue = reader.ReadToEnd();
                    response.Close();
                }
            }

            return responseValue;
        }

        public string Insert(OpportunityEntity entity)
        {
            return Invoke("POST", entity.GetInsertJson());
        }

        public string Update(OpportunityEntity entity)
        {
            return Invoke("PATCH", entity.GetUpdateJson());
        }
    }
}