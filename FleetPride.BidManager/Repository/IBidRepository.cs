﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FleetPride.BidManager.Utilities;

namespace FleetPride.BidManager.Repository
{
    public interface IBidRepository
    {
        void SaveOpportunityID(int bidId, string opportunityId);
        List<BidEntity> GetAllBids(string userRole, string userId);

        List<Customer> GetAllCustInfo(string bidType, int company, string userRole, string userId);

        int BulkInsertBidParts(DataTable bidParts, int bidId);

        List<BidPartEntity> GetBidPartsList(int bidId, string userId, string userRole);

        int InsertBidPart(int bidId, string customerPartNumber, string partDescription, string manufacturer, string manufacturerPartNumber, string note, int? estimatedAnnualUsage, string userId);

        int UpdateBidPart(int bidPartId, string customerPartNumber, string partDescription, string manufacturer, string manufacturerPartNumber, string note, int? estimatedAnnualUsage, string userId);

        int DeleteBidPart(int bidPartId);

        int CreateBid(BidEntity record, string userId, string userRole);

        void GenerateCrossesForBidParts(int bidId, string userId);

        int UpdateBid(string bidId, string oldRank, string newRank, string dueDate, string phaseDueDate, string totalValueOfBid, string projectedGPDollar, string projectedGPPerc, string userId);

        void SaveUploadedExcel(string fileName, int bidId);

        CustomEnum GetBidStatus(int bidId);

        BidEntity GetBid(int bidId);
        void SubmitForCrowdSourcing(List<BidPartEntity> selectedRows);
        void Submit(int bidId, string nextStatus, string comments);

        void Complete(int bidId, string status, string comments);

        List<BidEmailEntity> GetTatMissingBids();
        List<UserEntity> GetBidCreatorDetails(int bidId);

        int AdminBidStatusUpdate(int bidId, string oldStatus, string newStatus);

    }
}

