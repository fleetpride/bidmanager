﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Utilities;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;

namespace FleetPride.BidManager.Repository
{
    public interface IAuthorizationRepository
    {
        /// <summary>
        /// Method to get logged in user details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserEntity GetUserDetailsFromDB(string userId);
        string GetUserEmail(string userId);
        List<UserEntity> GetSuperUsers();
        List<UserEntity> GetNAMValidators();
        List<UserEntity> GetNAMSupervisors();
        List<UserEntity> GetRAMValidators();
        List<UserEntity> GetRAMSupervisors();
        List<UserEntity> GetDataTeam();
        List<UserEntity> GetDemandTeam();
        List<UserEntity> GetCategoryDirector();
        List<UserEntity> GetCategoryVP();
        List<UserEntity> GetAllUsersInADGroup(string adGroup, UserRole role);
        List<UserEntity> GetUsersInRoleFromDB(UserRole role);
        /// <summary>
        /// Helper method to check if user is present in AD group
        /// </summary>
        /// <param name="loggedInUserId"></param>
        /// <param name="adGroup"></param>
        /// <returns></returns>
        bool IsUserInADGroup(string userId, string adGroup);
        /// <summary>
        /// Helper method to get users from nested group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        bool IsUserInADGroup(string userId, GroupPrincipal group);

        bool IsUserAuthorised();
        string GetAuthorisedUserRole();
        string GetUserEmail();
        bool HasAccess(string pageName);
    }
}