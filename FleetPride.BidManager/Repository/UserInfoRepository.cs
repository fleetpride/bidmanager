﻿using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Configuration;
using System.Data;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;

namespace FleetPride.BidManager.Repository
{
    public class UserInfoRepository : IUserInfoRepository
    {
        string groupUserIds = "";

        /// <summary>
        /// Defualt constructor
        /// </summary>
        //SqlConnection con;
        public UserInfoRepository()
        {
            //con = new SqlConnection(ConfigurationManager.ConnectionStrings["BM"].ConnectionString);
        }

        /// <summary>
        /// Helper method to check if user is allowed
        /// </summary>
        /// <returns></returns>
        

        /// <summary>
        /// Helper method to get users from nested group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public String GetAllUserIdsFromNestedADGroups(GroupPrincipal group)
        {
            foreach (Principal principal1 in group.Members)
            {
                if (principal1.StructuralObjectClass.Equals("group"))
                {
                    GroupPrincipal group1 = GroupPrincipal.FindByIdentity(principal1.Context, principal1.SamAccountName);

                    foreach (Principal principal2 in group1.Members)
                    {
                        GetAllUserIdsFromNestedADGroups(group1);
                    }
                }
                else
                {
                    groupUserIds += principal1.SamAccountName + ",";
                }
            }
            return groupUserIds;
        }
    }
}