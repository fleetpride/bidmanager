﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Repository
{
    public class CrowdSourcingRepository : ICrowdSourcingRepository
    {
        private string _bmConnString;

        /// <summary>
        /// Default constructer
        /// </summary>
        public CrowdSourcingRepository()
        {
            _bmConnString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
        }

        #region CrossEntry Methods

        /// <summary>
        /// Method to get the count/statistics for cross entry User from database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<int> GetCrossEntryUserCounts(string userId)
        {
            List<int> crossEntryUserCount = new List<int>();

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_GetCrossEntryUserCounts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@CrossMTDEnteredCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@CrossMTDValidatedCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@CrossTodayEnteredCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    crossEntryUserCount.Add((int)cmd.Parameters["@CrossMTDEnteredCount"].Value);
                    crossEntryUserCount.Add((int)cmd.Parameters["@CrossMTDValidatedCount"].Value);
                    if ((Convert.ToInt32(ConfigurationManager.AppSettings["CrossEntryLimit"]) - (int)cmd.Parameters["@CrossTodayEnteredCount"].Value) < 0)
                    {
                        crossEntryUserCount.Add(0);
                    }
                    else
                    {
                        crossEntryUserCount.Add(Convert.ToInt32(ConfigurationManager.AppSettings["CrossEntryLimit"]) - (int)cmd.Parameters["@CrossTodayEnteredCount"].Value);
                    }

                }
            }

            return crossEntryUserCount;
        }

        /// <summary>
        /// Method to get the Check Locked Entry Part For User from DB
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckLockedEntryPartForUser(string userId)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_CheckLockedEntryPartForUser", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@Result"].Value;
                }
            }
            return result;
        }

        /// <summary>
        /// Method to get the crowd sourcing part for the user from database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public BidPartEntity GetPartForCrowdSourcing(string userId)
        {
            BidPartEntity bidPart = new BidPartEntity();

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_GetPartForCrowdSourcing", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        bidPart.BidId = Convert.ToInt32(rdr["BidId"]);
                        bidPart.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
                        bidPart.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
                        bidPart.PartDescription = Convert.ToString(rdr["PartDescription"] == DBNull.Value ? "" : rdr["PartDescription"]);
                        bidPart.Manufacturer = Convert.ToString(rdr["Manufacturer"] == DBNull.Value ? "" : rdr["Manufacturer"]);
                        bidPart.Note = Convert.ToString(rdr["Note"] == DBNull.Value ? "" : rdr["Note"]);
                    }
                }
            }
            return bidPart;
        }

        /// <summary>
        /// Method to load the crosses for the part from database
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <returns></returns>
        public List<CrossEntity> GetCrossesForEntry(int bidPartId)
        {
            List<CrossEntity> lstCrosses = new List<CrossEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_GetCrossesForEntry", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BidPartId", SqlDbType.Int)).Value = bidPartId;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            CrossEntity crosses = new CrossEntity();
                            crosses.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
                            crosses.PartNumber = Convert.ToString(rdr["PartNumber"] == DBNull.Value ? "" : rdr["PartNumber"]);
                            crosses.PoolNumber = Convert.ToString(rdr["PoolNumber"] == DBNull.Value ? "" : rdr["PoolNumber"]);
                            crosses.PartDescription = Convert.ToString(rdr["PartDescription"] == DBNull.Value ? "" : rdr["PartDescription"]);
                            crosses.PartCategory = Convert.ToString(rdr["PartCategory"] == DBNull.Value ? "" : rdr["PartCategory"]);
                            crosses.ValidationStatus = Convert.ToString(rdr["ValidationStatus"] == DBNull.Value ? "" : rdr["ValidationStatus"]);
                            crosses.IsNonFP = Convert.ToBoolean(rdr["IsNonFP"]);
                            crosses.IsFlip = Convert.ToBoolean(rdr["IsFlip"]);
                            lstCrosses.Add(crosses);
                        }
                    }
                }
            }
            return lstCrosses;
        }

        /// <summary>
        /// Method to check the part number in INMNPM table in database
        /// </summary>
        /// <param name="partNumber"></param>
        /// <returns></returns>
        public List<CrossEntity> CheckPartNumber(string partNumber, int bidId, bool matchPartial)
        {
            List<CrossEntity> lstParts = new List<CrossEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_CheckPartNumber", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@PartNo", SqlDbType.NVarChar)).Value = partNumber;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.NVarChar)).Value = bidId;
                    cmd.Parameters.Add(new SqlParameter("@MatchPartial", SqlDbType.Bit)).Value = matchPartial;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            CrossEntity crossPart = new CrossEntity();
                            //crossPart.CrossPartId = Convert.ToInt32(rdr["CrossPartId"] == DBNull.Value ? "" : rdr["CrossPartId"]);
                            crossPart.ProductKey = Convert.ToInt32(rdr["ProductKey"] == DBNull.Value ? "" : rdr["ProductKey"]);
                            crossPart.PoolNumber = Convert.ToString(rdr["Pool"] == DBNull.Value ? "" : rdr["Pool"]);
                            crossPart.PartNumber = Convert.ToString(rdr["PartNumber"] == DBNull.Value ? "" : rdr["PartNumber"]);
                            crossPart.PartDescription = Convert.ToString(rdr["PartDesciption"] == DBNull.Value ? "" : rdr["PartDesciption"]);
                            crossPart.PartCategory = Convert.ToString(rdr["Category"] == DBNull.Value ? "" : rdr["Category"]);
                            crossPart.IsFlip = Convert.ToBoolean(rdr["IsFlip"] == DBNull.Value ? "" : rdr["IsFlip"]);
                            crossPart.DCStockCount = Convert.ToString(rdr["DCStockCount"] == DBNull.Value ? "" : rdr["DCStockCount"]);
                            crossPart.StoresStockCount = Convert.ToString(rdr["StoresStockCount"] == DBNull.Value ? "" : rdr["StoresStockCount"]);
                            crossPart.NationalL12StoresSold = Convert.ToString(rdr["NationalL12StoresSold"] == DBNull.Value ? "" : rdr["NationalL12StoresSold"]);
                            crossPart.LocalL12StoresSold = Convert.ToString(rdr["LocalL12StoresSold"] == DBNull.Value ? "" : rdr["LocalL12StoresSold"]);
                            crossPart.L12Units = Convert.ToString(rdr["L12Units"] == DBNull.Value ? "" : rdr["L12Units"]);
                            crossPart.L12Revenue = Convert.ToString(rdr["L12Revenue"] == DBNull.Value ? "" : rdr["L12Revenue"]);
                            crossPart.NPMPartType = Convert.ToString(rdr["NPMPartType"] == DBNull.Value ? "" : rdr["NPMPartType"]);
                            lstParts.Add(crossPart);
                        }
                    }
                }
            }
            return lstParts;
        }

        /// <summary>
        /// Method to get all part info from DB
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="userRole"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Part> GetAllPartInfo(int bidId, string userRole, string userId)
        {
            List<Part> lstParts = new List<Part>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_GetAllPartInfo", conn))
                {
                    cmd.CommandTimeout = 3600;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@UserRole", SqlDbType.VarChar)).Value = userRole;
                    cmd.Parameters.Add(new SqlParameter("@BidId", SqlDbType.NVarChar)).Value = bidId;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Part crossPart = new Part();
                            crossPart.PartInfo = Convert.ToString(rdr["PartInfo"]);
                            crossPart.ProductKey = Convert.ToInt32(rdr["ProductKey"]);
                            crossPart.IsFlip = Convert.ToBoolean(rdr["IsFlip"]);
                            lstParts.Add(crossPart);
                        }
                    }
                }
            }
            return lstParts;
        }

        /// <summary>
        /// Metohd to save the part numbers from DimProduct table in database
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <param name="partNo"></param>
        /// <param name="ssflip"></param>
        /// <param name="pools"></param>
        /// <param name="isCrossReport"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int SaveDbFoundPoolPart(int crowdSourcingPartId, string partNo, bool ssflip, List<int> pools, List<int> productKeys, int isCrossReport, string source, string userId)
        {
            int rowAffected = 0;

            for (var i = 0; i < pools.Count; i++)
            {
                using (SqlConnection conn = new SqlConnection(_bmConnString))
                {
                    using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_SaveDbFoundPoolPart", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@CrowdSourcingPartId", SqlDbType.Int)).Value = crowdSourcingPartId;
                        cmd.Parameters.Add(new SqlParameter("@PartNo", SqlDbType.NVarChar)).Value = partNo;
                        cmd.Parameters.Add(new SqlParameter("@SSFlip", SqlDbType.Bit)).Value = ssflip;
                        cmd.Parameters.Add(new SqlParameter("@Pool", SqlDbType.Int)).Value = pools[i];
                        cmd.Parameters.Add(new SqlParameter("@ProductKey", SqlDbType.Int)).Value = productKeys[i];
                        cmd.Parameters.Add(new SqlParameter("@IsCrossReport", SqlDbType.Int)).Value = isCrossReport;
                        cmd.Parameters.Add(new SqlParameter("@Source", SqlDbType.VarChar)).Value = source;
                        cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                        cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                        conn.Open();
                        cmd.ExecuteScalar();
                        rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    }
                }
            }
            return rowAffected;
        }

        /// <summary>
        ///  Metohd to save the part numbers in DimProduct table in database using part or desc 
        /// </summary>
        /// <param name="parts"></param>
        /// <param name="userId"></param>
        public void SaveSearchedDbFoundPoolPart(int crowdSourcingPartId, int isCrossReport, string source, List<Part> parts, string userId)
        {
            int rowAffected = 0;

            foreach (var part in parts)
            {
                using (SqlConnection conn = new SqlConnection(_bmConnString))
                {
                    using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_SaveSearchedDbFoundPoolPart", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@CrowdSourcingPartId", SqlDbType.Int)).Value = crowdSourcingPartId;
                        cmd.Parameters.Add(new SqlParameter("@SSFlip", SqlDbType.Bit)).Value = part.IsFlip;
                        cmd.Parameters.Add(new SqlParameter("@ProductKey", SqlDbType.Int)).Value = part.ProductKey;
                        cmd.Parameters.Add(new SqlParameter("@IsCrossReport", SqlDbType.Int)).Value = isCrossReport;
                        cmd.Parameters.Add(new SqlParameter("@Source", SqlDbType.VarChar)).Value = source;
                        cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                        cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                        conn.Open();
                        cmd.ExecuteScalar();
                        rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    }
                }
            }
        }

        /// <summary>
        /// Method to save user entered part details in database
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <param name="partNo"></param>
        /// <param name="poolNo"></param>
        /// <param name="description"></param>
        /// <param name="category"></param>
        /// <param name="domainId"></param>
        /// <returns></returns>
        public int SaveManualPoolPart(int crowdSourcingPartId, string partNo, int? poolNo, string description, string category, int isCrossReport, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_SaveManualPoolPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrowdSourcingPartId", SqlDbType.Int)).Value = crowdSourcingPartId;
                    cmd.Parameters.Add(new SqlParameter("@PartNo", SqlDbType.NVarChar)).Value = partNo;
                    cmd.Parameters.Add(new SqlParameter("@PoolNo", SqlDbType.Int)).Value = poolNo;
                    cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar)).Value = description;
                    cmd.Parameters.Add(new SqlParameter("@Category", SqlDbType.VarChar)).Value = category;
                    cmd.Parameters.Add(new SqlParameter("@IsCrossReport", SqlDbType.Int)).Value = isCrossReport;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        /// <summary>
        /// Method to submit the part from user in database
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <param name="domainId"></param>
        public void SubmitCrowdSourcingPart(int crowdSourcingPartId, string userId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_SubmitCrowdSourcingPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrowdSourcingPartId", SqlDbType.Int)).Value = crowdSourcingPartId;
                    cmd.Parameters.Add(new SqlParameter("@SubmittedBy", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@IsSubmitted", SqlDbType.Bit)).Value = true;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Method to release the part from user in database
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int ReleaseCrowdSourcingPart(int crowdSourcingPartId, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_ReleaseCrowdSourcingPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrowdSourcingPartId", SqlDbType.Int)).Value = crowdSourcingPartId;
                    cmd.Parameters.Add(new SqlParameter("@ReleasedUser", SqlDbType.VarChar)).Value = userId;
                    conn.Open();
                    rowAffected = cmd.ExecuteNonQuery();
                    if (rowAffected > 0)
                    {
                        return rowAffected;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        /// <summary>
        /// Method to delete the entered cross in database
        /// </summary>
        /// <param name="crossPartId"></param>
        public void DeleteCross(int crossPartId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossEntry_DeleteCross", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartId", SqlDbType.Int)).Value = crossPartId;
                    conn.Open();

                    cmd.ExecuteNonQuery();

                    rowAffected = crossPartId;
                }
            }
        }

        #endregion

        #region  CrossValidation Methods

        /// <summary>
        /// Method to get the count/statistics for cross validation User from database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<int> GetCrossValidatorCounts(string userId)
        {
            List<int> CrossValidatorCount = new List<int>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossValidation_GetCrossValidatorCounts", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add("@CrossMTDEnteredCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@CrossMTDValidatedCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@CrossTodayValidatedCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    CrossValidatorCount.Add((int)cmd.Parameters["@CrossMTDEnteredCount"].Value);
                    CrossValidatorCount.Add((int)cmd.Parameters["@CrossMTDValidatedCount"].Value);
                    if ((Convert.ToInt32(ConfigurationManager.AppSettings["CrossValidationLimit"]) - (int)cmd.Parameters["@CrossTodayValidatedCount"].Value) < 0)
                    {
                        CrossValidatorCount.Add(0);
                    }
                    else
                    {
                        CrossValidatorCount.Add(Convert.ToInt32(ConfigurationManager.AppSettings["CrossValidationLimit"]) - (int)cmd.Parameters["@CrossTodayValidatedCount"].Value);
                    }

                }
            }
            return CrossValidatorCount;
        }

        /// <summary>
        /// Method to get the Check Locked Validation Part For User from DB
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CheckLockedValidationPartForUser(string userId)
        {
            int result = 0;
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossValidation_CheckLockedValidationPartForUser", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    result = (int)cmd.Parameters["@Result"].Value;
                }
            }
            return result;
        }

        /// <summary>
        /// Method to get the part for the user for validiation from database
        /// </summary>
        /// <param name = "userId" ></ param >
        /// < returns ></ returns >
        public BidPartEntity GetPartForValidation(string userId)
        {
            BidPartEntity bidPart = new BidPartEntity();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossValidation_GetPartForValidation", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    conn.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        bidPart.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
                        bidPart.CustomerPartNumber = Convert.ToString(rdr["CustomerPartNumber"]);
                        bidPart.PartDescription = Convert.ToString(rdr["PartDescription"] == DBNull.Value ? "" : rdr["PartDescription"]);
                        bidPart.Manufacturer = Convert.ToString(rdr["Manufacturer"] == DBNull.Value ? "" : rdr["Manufacturer"]);
                        bidPart.Note = Convert.ToString(rdr["Note"] == DBNull.Value ? "" : rdr["Note"]);
                    }
                    rdr.Close();
                }
            }

            return bidPart;
        }

        /// <summary>
        /// Method to load the crosses for the part validation from database
        /// </summary>
        /// <param name = "crossValidationPartId" ></ param >
        /// < param name="userId"></param>
        /// <returns></returns>
        public List<CrossEntity> GetCrossesForValidation(int crossValidationPartId, string userId)
        {
            List<CrossEntity> lstCrossForValidation = new List<CrossEntity>();
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossValidation_GetCrossesForValidation", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrossValidationPartId", SqlDbType.Int)).Value = crossValidationPartId;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    conn.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        CrossEntity crosses = new CrossEntity();
                        crosses.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
                        crosses.PartNumber = Convert.ToString(rdr["PartNumber"] == DBNull.Value ? "" : rdr["PartNumber"]);
                        crosses.PoolNumber = Convert.ToString(rdr["PoolNumber"] == DBNull.Value ? "" : rdr["PoolNumber"]);
                        crosses.PartDescription = Convert.ToString(rdr["PartDescription"] == DBNull.Value ? "" : rdr["PartDescription"]);
                        crosses.PartCategory = Convert.ToString(rdr["PartCategory"] == DBNull.Value ? "" : rdr["PartCategory"]);
                        crosses.ValidationStatus = Convert.ToString(rdr["ValidationStatus"] == DBNull.Value ? "" : rdr["ValidationStatus"]);
                        crosses.IsNonFP = Convert.ToBoolean(rdr["IsNonFP"] == DBNull.Value ? "" : rdr["IsNonFP"]);
                        crosses.IsFlip = Convert.ToBoolean(rdr["IsFlip"] == DBNull.Value ? 0 : rdr["IsFlip"]);
                        lstCrossForValidation.Add(crosses);
                    }
                    rdr.Close();
                }
            }

            return lstCrossForValidation;
        }

        /// <summary>
        /// Method to approve/reject a cross of a part in database
        /// </summary>
        /// <param name="crossPartId"></param>
        /// <param name="status"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int UpdateCrossValidationStatus(int crossPartId, string status, string userId)
        {
            int rowAffected = 0;

            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossValidation_UpdateCrossValidationStatus", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CrossPartId", SqlDbType.Int)).Value = crossPartId;
                    cmd.Parameters.Add(new SqlParameter("@CrossValidationStatus", SqlDbType.Char)).Value = status;
                    cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userId;
                    cmd.Parameters.Add("@AffectedRowId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteScalar();
                    rowAffected = (int)cmd.Parameters["@AffectedRowId"].Value;
                }
            }
            return rowAffected;
        }

        /// <summary>
        /// Method to submit the part from validator in database
        /// </summary>
        /// <param name = "ValidatedCrossPartId" ></ param >
        /// < param name="userId"></param>
        public void SubmitValidatedPart(int validatedCrossPartId, string userId)
        {
            using (SqlConnection conn = new SqlConnection(_bmConnString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.CrossValidation_SubmitValidatedPart", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ValidatedCrossPartId", SqlDbType.Int)).Value = validatedCrossPartId;
                    cmd.Parameters.Add(new SqlParameter("@ValidatedUser", SqlDbType.NVarChar)).Value = userId;
                    cmd.Parameters.Add(new SqlParameter("@IsValidated", SqlDbType.Bit)).Value = true;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}