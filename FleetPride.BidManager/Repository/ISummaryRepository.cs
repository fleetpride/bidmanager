﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FleetPride.BidManager.Utilities;

namespace FleetPride.BidManager.Repository
{
    public interface ISummaryRepository
    {
        List<CrowdsourcingSummaryEntity> GetCrowdsourcingSummary(string userRole, string userId);

        List<ProgressSummaryEntity> GetProgressSummary(string userRole, string userId);
    }
}

