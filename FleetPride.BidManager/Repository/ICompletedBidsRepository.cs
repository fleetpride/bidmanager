﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FleetPride.BidManager.Models;
using static FleetPride.BidManager.Models.CrossEntity;

namespace FleetPride.BidManager.Repository
{
    public interface ICompletedBidsRepository
    {
        List<BidEntity> GetAllBids(string userRole, string userId);

        int MarkWonSelectedCross(int bidId, int bidPartId, int crossPartId);

        void MarkAllPrimaryWon(int bidId);

        DataSet ExportFinalCrosses(int bidPartId);

        int BulkInsertPartMarkWonData(DataTable partMarkWonData, int bidId);

        DataSet GetSupplyChainData(int bidId, string userId, string userRole);

    }
}
