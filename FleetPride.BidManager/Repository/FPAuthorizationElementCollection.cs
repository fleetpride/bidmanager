﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace FleetPride.BidManager.Repository
{
    [ConfigurationCollection(typeof(FPAuthorizationElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
    public class FPAuthorizationElementCollection : ConfigurationElementCollection
    {
        #region Constructor
        public FPAuthorizationElementCollection()
        {
        }
        #endregion

        #region Properties
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }
        protected override string ElementName
        {
            get
            {
                return "module";
            }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return new ConfigurationPropertyCollection();
            }
        }
        #endregion

        #region Indexers
        public FPAuthorizationElement this[int index]
        {
            get
            {
                return (FPAuthorizationElement)base.BaseGet(index);
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                base.BaseAdd(index, value);
            }
        }

        public FPAuthorizationElement this[string name]
        {
            get
            {
                return (FPAuthorizationElement)base.BaseGet(name);
            }
        }
        #endregion

        #region Methods
        public void Add(FPAuthorizationElement item)
        {
            base.BaseAdd(item);
        }

        public void Remove(FPAuthorizationElement item)
        {
            base.BaseRemove(item);
        }

        public void RemoveAt(int index)
        {
            base.BaseRemoveAt(index);
        }
        #endregion

        #region Overrides
        protected override ConfigurationElement CreateNewElement()
        {
            return new FPAuthorizationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as FPAuthorizationElement).Name;
        }
        #endregion
    }
}