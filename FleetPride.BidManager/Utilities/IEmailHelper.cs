﻿using FleetPride.BidManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    public interface IEmailHelper
    {
        /// <summary>
        /// Send an email out of BidManager
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        void SendEmail(string toEmail, string subject, string body, string cc = "");

        /// <summary>
        /// Create the body of the message to be sent to the BidValidators after crosses has been generated for the bid.
        /// </summary>
        /// <param name="bidName"></param>
        /// <returns></returns>
        void SendNotificationAfterBidCreated(BidEntity bid);

        /// <summary>
        /// Get email body for finalize cross
        /// </summary>
        /// <param name="bidName"></param>
        /// <param name="bidId"></param>
        /// <param name="emailId"></param>
        void SendNotificationToFinalizeCrosses(string bidName, int bidId, List<CMUserEntity> cms);

        /// <summary>
        /// Get email body for pricing team
        /// </summary>
        /// <param name="bidName"></param>
        /// <param name="bidId"></param>
        /// <param name="emailId"></param>
        void SendNotificationToPricing(string bidName, int bidId);

        void SendNotificationToDataTeam(string bidName, int bidId);

        void SendNotificationToDemandTeam(string bidName, int bidId);

        void SendNotificationToCategoryDirector(string bidName, int bidId);

        void SendNotificationToCategoryVP(string bidName, int bidId);

        void SendNotificationToCM(string bidName, int bidId, List<CMUserEntity> cms);
        void SendRevisedPartsNotification(string bidName, int bidId, string bidType);
        void SendNotificationToNAMOrBidCreator(string bidName, string emails);

        void SendSupplyChainNotification(int bidId, string bidName, string comment, string path);
    }
}