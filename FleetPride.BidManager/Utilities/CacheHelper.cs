﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;
using System.Configuration;

namespace FleetPride.BidManager.Utilities
{
    public static class CacheHelper
    {
        private static MemoryCache _cache = new MemoryCache("BidManagementCache");
        public static T Get<T>(string cacheKey) where T: class
        {

            if (_cache.Contains(cacheKey))
                return (T)_cache.Get(cacheKey);
            else
            {              
               return null;
            }
        }
        public static void Add<T>(string cacheKey, T data) where T: class
        {
            // Store data in the cache    

            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            int hours = Convert.ToInt32(ConfigurationManager.AppSettings["CacheDuration"]);
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(hours);
            _cache.Add(cacheKey, data, cacheItemPolicy);
            
        }

        public static void Remove(string key)
        {
            ObjectCache _cache = MemoryCache.Default;
            if (_cache.Contains(key))
                _cache.Remove(key);
        }
    }
   
}