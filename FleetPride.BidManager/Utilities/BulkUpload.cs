﻿using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Web.Mvc;
using FleetPride.BidManager.Models;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Configuration;

namespace FleetPride.BidManager.Utilities
{
    public class BulkUploadHelper
    {
        private string _filePath;
        private Dictionary<string, int> _columnIndexMap;
        private DataTable _resultsDataTable;
        private DataTable _resultsDataTableToSaveInDB;
        private IBidRepository _bidRepository;
        private const int _requiredColumnCount = 6;

        public BulkUploadHelper(string filePath, IBidRepository bidRepository, string userID, string userRole)
        {
            _filePath = filePath;
            _bidRepository = bidRepository;
            _resultsDataTable = ResultsTable();
            _resultsDataTableToSaveInDB = ResultsTableToSaveInDB();
            _columnIndexMap = new Dictionary<string, int>()
                        {
                            { Constants.ColumnName.CustomerPartNumber,-1 },
                            { Constants.ColumnName.PartDescription,-1 },
                            { Constants.ColumnName.Manufacturer,-1 },
                            { Constants.ColumnName.ManufacturerPartNumber,-1 },
                            { Constants.ColumnName.EstimatedAnnualUsage,-1 },
                            { Constants.ColumnName.Note,-1 },
                        };
        }


        /// <summary>
        /// Upload the excel file to db
        /// </summary>
        /// <returns></returns>
        public int Upload(int bidId)
        {
            int totalErrors = 0;

            using (var package = new ExcelPackage(new System.IO.FileInfo(_filePath)))
            {
                var workSheet = package.Workbook.Worksheets.First();
                if (workSheet.Dimension == null)
                {
                    throw new Exception("File is not valid, required column(s) not present in the sheet. <br /> Please download the template.");
                }
                object[,] sheetVector = new object[workSheet.Dimension.End.Row, workSheet.Dimension.End.Column];
                sheetVector = (object[,])workSheet.Cells.Value;

                //Method to check if all columns present in uploaded excel
                InitColumnIndices(sheetVector, workSheet.Dimension.End.Column);

                //if (workSheet.Dimension.End.Row > 101)
                //{
                //    throw new Exception("Max 100 records allowed per file.");
                //}

                //prepare the results datatables
                if (workSheet.Dimension.End.Row > 1)
                {
                    for (int rowIndex = 1; rowIndex < workSheet.Dimension.End.Row; rowIndex++)
                    {
                        // Validate the uploaded excel data and get the error count
                        totalErrors += ValidateAndConvertDataRow(sheetVector, rowIndex, bidId);
                    }
                }
                else
                {
                    throw new Exception("Data not present in the file.");
                }

                //totalErrors += MarkDuplicates();

                // Removed extra columns and added from userId column in data table to send to save in db

                //_resultsDataTableToSaveInDB = _resultsDataTable;

                for (int i = _resultsDataTableToSaveInDB.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = _resultsDataTableToSaveInDB.Rows[i];
                    if (dr[Constants.ColumnName.UploadStatus].ToString() == "Error")
                        dr.Delete();
                }
                _resultsDataTableToSaveInDB.Columns.Remove(Constants.ColumnName.UploadStatus);
                _resultsDataTableToSaveInDB.Columns.Remove(Constants.ColumnName.Remarks);
                _resultsDataTableToSaveInDB.AcceptChanges();

                _bidRepository.BulkInsertBidParts(_resultsDataTableToSaveInDB, bidId);

                _resultsDataTable.Columns.Remove(Constants.ColumnName.BidId);
                _resultsDataTable.Columns.Remove(Constants.ColumnName.UserId);

                if (package.Workbook.Worksheets.Any(x => x.Name == "Upload Result"))
                {
                    package.Workbook.Worksheets.Delete(package.Workbook.Worksheets.Where(x => x.Name == "Upload Result").First());
                }

                ExcelWorksheet resultSheet = package.Workbook.Worksheets.Add("Upload Result");
                resultSheet.Cells["A1"].LoadFromDataTable(_resultsDataTable, true);
                package.Save();
            }

            return totalErrors;
        }

        //private int MarkDuplicates()
        //{
        //    int duplicates = 0;
        //    for (int rowIndex = 0; rowIndex < _resultsDataTable.Rows.Count; rowIndex++)
        //    {
        //        // Validate the uploaded excel data and get the error count
        //        DataRow dr1 = _resultsDataTable.Rows[rowIndex];
        //        if (Convert.ToString(dr1[Constants.ColumnName.UploadStatus]) == Constants.Success)
        //        {
        //            for (int i = rowIndex + 1; i < _resultsDataTable.Rows.Count; i++)
        //            {
        //                DataRow dr2 = _resultsDataTable.Rows[i];
        //                if (Convert.ToString(dr2[Constants.ColumnName.UploadStatus]) == Constants.Success
        //                        && Convert.ToString(dr2[Constants.ColumnName.CustomerPartNumber]) == Convert.ToString(dr1[Constants.ColumnName.CustomerPartNumber])
        //                        && Convert.ToString(dr2[Constants.ColumnName.PartDescription]) == Convert.ToString(dr1[Constants.ColumnName.PartDescription])
        //                        && Convert.ToString(dr2[Constants.ColumnName.Manufacturer]) == Convert.ToString(dr1[Constants.ColumnName.Manufacturer])
        //                        && Convert.ToString(dr2[Constants.ColumnName.EstimatedAnnualUsage]) == Convert.ToString(dr1[Constants.ColumnName.EstimatedAnnualUsage])
        //                        && Convert.ToString(dr2[Constants.ColumnName.ManufacturerPartNumber]) == Convert.ToString(dr1[Constants.ColumnName.ManufacturerPartNumber])
        //                        && Convert.ToString(dr2[Constants.ColumnName.Note]) == Convert.ToString(dr1[Constants.ColumnName.Note])
        //                        && Convert.ToInt32(dr2[Constants.ColumnName.EstimatedAnnualUsage]) == Convert.ToInt32(dr1[Constants.ColumnName.EstimatedAnnualUsage]))
        //                {
        //                    duplicates++;
        //                    dr2[Constants.ColumnName.UploadStatus] = Constants.Error;
        //                    dr2[Constants.ColumnName.Remarks] = "Duplicate";
        //                }
        //            }
        //        }
        //    }
        //    return duplicates;
        //}

        private DataTable ResultsTable()
        {
            var dt = new DataTable();
            dt.Columns.Add(Constants.ColumnName.BidId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.CustomerPartNumber, typeof(string));
            dt.Columns.Add(Constants.ColumnName.PartDescription, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Manufacturer, typeof(string));
            dt.Columns.Add(Constants.ColumnName.ManufacturerPartNumber, typeof(string));
            dt.Columns.Add(Constants.ColumnName.EstimatedAnnualUsage, typeof(int));
            dt.Columns.Add(Constants.ColumnName.Note, typeof(string));
            dt.Columns.Add(Constants.ColumnName.UserId, typeof(string));
            dt.Columns.Add(Constants.ColumnName.UploadStatus, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Remarks, typeof(string));

            return dt;
        }

        private DataTable ResultsTableToSaveInDB()
        {
            var dt = new DataTable();
            dt.Columns.Add(Constants.ColumnName.BidId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.CustomerPartNumber, typeof(string));
            dt.Columns.Add(Constants.ColumnName.PartDescription, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Manufacturer, typeof(string));
            dt.Columns.Add(Constants.ColumnName.ManufacturerPartNumber, typeof(string));
            dt.Columns.Add(Constants.ColumnName.EstimatedAnnualUsage, typeof(int));
            dt.Columns.Add(Constants.ColumnName.Note, typeof(string));
            dt.Columns.Add(Constants.ColumnName.UserId, typeof(string));
            dt.Columns.Add(Constants.ColumnName.UploadStatus, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Remarks, typeof(string));

            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="sheetVector"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private int ValidateAndConvertDataRow(object[,] sheetVector, int rowIndex, int bidId)
        {
            DataRow resultsDataRow = _resultsDataTable.NewRow();
            DataRow resultsDataRowToSaveInDB = _resultsDataTableToSaveInDB.NewRow(); ;

            bool errorFlag = false;
            bool finalErrorFlag = false;
            StringBuilder allErrMessages = new StringBuilder();
            string errMessages = "";
            int errorCount = 0;

            string customerPartNumber = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.CustomerPartNumber]]).Trim();
            string partDescription = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.PartDescription]]).Trim();
            string manufacturer = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.Manufacturer]]).Trim();
            string manufacturerPartNumber = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.ManufacturerPartNumber]]).Trim();
            string estimatedAnnualUsage = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.EstimatedAnnualUsage]]).Trim();
            string note = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.Note]]).ToUpper().Trim();

            resultsDataRow[Constants.ColumnName.BidId] = bidId;
            resultsDataRow[Constants.ColumnName.UserId] = System.Web.HttpContext.Current.User.Identity.Name;
            resultsDataRowToSaveInDB[Constants.ColumnName.BidId] = bidId;
            resultsDataRowToSaveInDB[Constants.ColumnName.UserId] = System.Web.HttpContext.Current.User.Identity.Name;

            if (customerPartNumber == "" && manufacturerPartNumber == "")
            {
                finalErrorFlag = true;
                Append(allErrMessages, "Please enter Manufacturer Part Number or Customer Part Number.");
                errorCount = 1;
            }

            //Customer Part Number
            if (!ExcelValidationHelper.IsValidStringLengthColumn(Constants.ColumnName.CustomerPartNumber, customerPartNumber, 50, out errMessages, out errorFlag))
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }
            else
            {
                resultsDataRow[Constants.ColumnName.CustomerPartNumber] = customerPartNumber;
                resultsDataRowToSaveInDB[Constants.ColumnName.CustomerPartNumber] = customerPartNumber;

            }

            //Manufacturer Part Number
            if (!ExcelValidationHelper.IsValidStringLengthColumn(Constants.ColumnName.ManufacturerPartNumber, manufacturerPartNumber, 50, out errMessages, out errorFlag))
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }
            else
            {
                resultsDataRow[Constants.ColumnName.ManufacturerPartNumber] = manufacturerPartNumber;
                resultsDataRowToSaveInDB[Constants.ColumnName.ManufacturerPartNumber] = manufacturerPartNumber;
            }

            //Part Description
            if (!ExcelValidationHelper.IsValidStringLengthColumn(Constants.ColumnName.PartDescription, partDescription, 300, out errMessages, out errorFlag))
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }
            else
            {
                resultsDataRow[Constants.ColumnName.PartDescription] = partDescription;
                resultsDataRowToSaveInDB[Constants.ColumnName.PartDescription] = partDescription;
            }

            //Manufacturer
            if (!ExcelValidationHelper.IsValidStringLengthColumn(Constants.ColumnName.Manufacturer, manufacturer, 50, out errMessages, out errorFlag))
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }
            else
            {
                resultsDataRow[Constants.ColumnName.Manufacturer] = manufacturer;
                resultsDataRowToSaveInDB[Constants.ColumnName.Manufacturer] = manufacturer;
            }


            try
            {
                //Estimated Annual Usage
                if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.EstimatedAnnualUsage, estimatedAnnualUsage, out errMessages, out errorFlag))
                {
                    decimal decEstimatedAnnualUsage = 0;
                    if (!Decimal.TryParse(estimatedAnnualUsage, out decEstimatedAnnualUsage))
                    {
                        finalErrorFlag = true;
                        Append(allErrMessages, Constants.ColumnName.EstimatedAnnualUsage + " is not valid.");
                        errorCount = 1;
                    }
                    else if (!ExcelValidationHelper.IsNumberInRange(Constants.ColumnName.EstimatedAnnualUsage, (int)Math.Round(decEstimatedAnnualUsage, 0, MidpointRounding.AwayFromZero), 1, 99999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        //round up the decimal value to the nearest integer
                        resultsDataRow[Constants.ColumnName.EstimatedAnnualUsage] = (int)Math.Round(decEstimatedAnnualUsage, 0, MidpointRounding.AwayFromZero);
                        resultsDataRowToSaveInDB[Constants.ColumnName.EstimatedAnnualUsage] = (int)Math.Round(decEstimatedAnnualUsage, 0, MidpointRounding.AwayFromZero);
                    }
                }
            }
            catch
            {
                finalErrorFlag = true;
                Append(allErrMessages, Constants.ColumnName.EstimatedAnnualUsage + " is not valid.");
                errorCount = 1;
            }

            //Note
            if (!ExcelValidationHelper.IsValidStringLengthColumn(Constants.ColumnName.Note, note, 100, out errMessages, out errorFlag))
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }
            else
            {
                resultsDataRow[Constants.ColumnName.Note] = note;
                resultsDataRowToSaveInDB[Constants.ColumnName.Note] = note;
            }

            if (finalErrorFlag)
            {
                resultsDataRow[Constants.ColumnName.UploadStatus] = Constants.Error;
                resultsDataRow[Constants.ColumnName.Remarks] = allErrMessages.ToString();
                resultsDataRowToSaveInDB[Constants.ColumnName.UploadStatus] = Constants.Error;
                resultsDataRowToSaveInDB[Constants.ColumnName.Remarks] = allErrMessages.ToString();
            }
            else
            {
                resultsDataRow[Constants.ColumnName.UploadStatus] = Constants.Success;
                resultsDataRowToSaveInDB[Constants.ColumnName.UploadStatus] = Constants.Success;
            }

            _resultsDataTable.Rows.Add(resultsDataRow);
            _resultsDataTableToSaveInDB.Rows.Add(resultsDataRowToSaveInDB);
            return errorCount;
        }

        private void Append(StringBuilder sb, string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (sb.Length > 0)
                sb.Append(", ");

            sb.Append(message);
        }

        /// <summary>
        /// Init Column Indices
        /// </summary>
        /// <param name="sheetVector"></param>
        /// <param name="workSheet"></param>
        private void InitColumnIndices(object[,] sheetVector, int columnCount)
        {
            if (columnCount < _requiredColumnCount)
            {
                throw new Exception("File is not valid, required column(s) not present in the sheet:");
            }

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
            {
                string columnName = Convert.ToString(sheetVector[0, columnIndex]);
                if (columnName == Constants.ColumnName.CustomerPartNumber) _columnIndexMap[Constants.ColumnName.CustomerPartNumber] = columnIndex;
                if (columnName == Constants.ColumnName.PartDescription) _columnIndexMap[Constants.ColumnName.PartDescription] = columnIndex;
                if (columnName == Constants.ColumnName.Manufacturer) _columnIndexMap[Constants.ColumnName.Manufacturer] = columnIndex;
                if (columnName == Constants.ColumnName.ManufacturerPartNumber) _columnIndexMap[Constants.ColumnName.ManufacturerPartNumber] = columnIndex;
                if (columnName == Constants.ColumnName.EstimatedAnnualUsage) _columnIndexMap[Constants.ColumnName.EstimatedAnnualUsage] = columnIndex;
                if (columnName == Constants.ColumnName.Note) _columnIndexMap[Constants.ColumnName.Note] = columnIndex;
            }

            if (_columnIndexMap.ContainsValue(-1))
            {
                throw new Exception("File is not valid, required column(s) not present in the sheet. <br /> Please download the template.");
            }
        }

    }

    public class BulkUploadPricingDataHelper
    {
        private string _filePath;
        private Dictionary<string, int> _columnIndexMap;
        private DataTable _resultsDataTable;
        private DataTable _resultsDataTableToSaveInDB;
        private IPriceValidationRepository _priceValidationRepository;
        private const int _requiredColumnCount = 8;

        public BulkUploadPricingDataHelper(string filePath, IPriceValidationRepository priceValidationRepository, string userID, string userRole)
        {
            _filePath = filePath;
            _priceValidationRepository = priceValidationRepository;
            _resultsDataTable = ResultsTable();
            _resultsDataTableToSaveInDB = ResultsTableToSaveInDB();
            _columnIndexMap = new Dictionary<string, int>()
                        {
                            { Constants.ColumnName.BidPartId,-1 },
                            { Constants.ColumnName.CrossPartId,-1 },
                            { Constants.ColumnName.PriceToQuote,-1 },
                            { Constants.ColumnName.SuggestedPrice,-1 },
                            { Constants.ColumnName.ICOST,-1 },
                            { Constants.ColumnName.Margin,-1 },
                            { Constants.ColumnName.AdjustedICost,-1 },
                            { Constants.ColumnName.AdjustedMargin,-1 },
                        };
        }

        /// <summary>
        /// Upload Pricing Data excel file to db
        /// </summary>
        /// <returns></returns>
        public int UploadPricingData(int bidId)
        {
            int totalErrors = 0;

            using (var package = new ExcelPackage(new System.IO.FileInfo(_filePath)))
            {
                var workSheet = package.Workbook.Worksheets.First();
                if (workSheet.Dimension == null)
                {
                    throw new Exception("File is not valid, required column(s) not present in the sheet. <br /> Please download the template.");
                }
                object[,] sheetVector = new object[workSheet.Dimension.End.Row, workSheet.Dimension.End.Column];
                sheetVector = (object[,])workSheet.Cells.Value;

                //Method to check if all columns present in uploaded excel
                InitColumnIndices(sheetVector, workSheet.Dimension.End.Column);

                //prepare the results datatables
                if (workSheet.Dimension.End.Row > 1)
                {
                    for (int rowIndex = 1; rowIndex < workSheet.Dimension.End.Row; rowIndex++)
                    {
                        // Validate the uploaded excel data and get the error count
                        totalErrors += ValidateAndConvertPricingDataRow(sheetVector, rowIndex, bidId);
                    }
                }
                else
                {
                    throw new Exception("Data not present in the file.");
                }

                //totalErrors += MarkDuplicates();

                // Removed extra columns from data table to send to save in db

                for (int i = _resultsDataTableToSaveInDB.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = _resultsDataTableToSaveInDB.Rows[i];
                    if (dr[Constants.ColumnName.UploadStatus].ToString() == "Error")
                        dr.Delete();
                }
                _resultsDataTableToSaveInDB.Columns.Remove(Constants.ColumnName.UploadStatus);
                _resultsDataTableToSaveInDB.Columns.Remove(Constants.ColumnName.Remarks);

                _resultsDataTableToSaveInDB.AcceptChanges();

                _priceValidationRepository.BulkInsertPricingData(_resultsDataTableToSaveInDB, bidId);

                if (package.Workbook.Worksheets.Any(x => x.Name == "Upload Result"))
                {
                    package.Workbook.Worksheets.Delete(package.Workbook.Worksheets.Where(x => x.Name == "Upload Result").First());
                }

                ExcelWorksheet resultSheet = package.Workbook.Worksheets.Add("Upload Result");
                resultSheet.Cells["A1"].LoadFromDataTable(_resultsDataTable, true);
                package.Save();
            }

            return totalErrors;
        }

        private DataTable ResultsTable()
        {
            var dt = new DataTable();
            dt.Columns.Add(Constants.ColumnName.BidPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.CrossPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.PriceToQuote, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.SuggestedPrice, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.ICOST, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.Margin, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.AdjustedICost, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.AdjustedMargin, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.UploadStatus, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Remarks, typeof(string));

            return dt;
        }

        private DataTable ResultsTableToSaveInDB()
        {
            var dt = new DataTable();
            dt.Columns.Add(Constants.ColumnName.BidPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.CrossPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.PriceToQuote, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.SuggestedPrice, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.ICOST, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.Margin, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.AdjustedICost, typeof(decimal));
            dt.Columns.Add(Constants.ColumnName.AdjustedMargin, typeof(string));
            dt.Columns.Add(Constants.ColumnName.UploadStatus, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Remarks, typeof(string));

            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="sheetVector"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private int ValidateAndConvertPricingDataRow(object[,] sheetVector, int rowIndex, int bidId)
        {
            DataRow resultsDataRow = _resultsDataTable.NewRow();
            DataRow resultsDataRowToSaveInDB = _resultsDataTableToSaveInDB.NewRow(); ;

            bool errorFlag = false;
            bool finalErrorFlag = false;
            StringBuilder allErrMessages = new StringBuilder();
            string errMessages = "";
            int errorCount = 0;

            string bidPartId = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.BidPartId]]).Trim();

            string crossPartId = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.CrossPartId]]).Trim();

            string priceToQuote = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.PriceToQuote]]).Trim();

            string suggestedPrice = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.SuggestedPrice]]).Trim();

            string iCost = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.ICOST]]).Trim();

            string margin = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.Margin]]).Trim();

            string adjustedICost = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.AdjustedICost]]).Trim();

            string adjustedMargin = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.AdjustedMargin]]).Trim();


            //Bid Part Id
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.BidPartId, bidPartId, out errMessages, out errorFlag))
            {
                if (!ExcelValidationHelper.IsAllDigits(Constants.ColumnName.BidPartId, bidPartId, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else if (!ExcelValidationHelper.IsNumberInRange(Constants.ColumnName.BidPartId, Convert.ToInt32(bidPartId), 1, 2147483647, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else
                {
                    resultsDataRow[Constants.ColumnName.BidPartId] = bidPartId;
                    resultsDataRowToSaveInDB[Constants.ColumnName.BidPartId] = bidPartId;
                }
            }
            else
            {
                finalErrorFlag = true;
                Append(allErrMessages, Constants.ColumnName.BidPartId + " is not valid.");
                errorCount = 1;
            }

            //Cross Part Id
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.CrossPartId, crossPartId, out errMessages, out errorFlag))
            {
                if (!ExcelValidationHelper.IsAllDigits(Constants.ColumnName.CrossPartId, crossPartId, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else if (!ExcelValidationHelper.IsNumberInRange(Constants.ColumnName.BidPartId, Convert.ToInt32(crossPartId), 1, 2147483647, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else
                {
                    resultsDataRow[Constants.ColumnName.CrossPartId] = crossPartId;
                    resultsDataRowToSaveInDB[Constants.ColumnName.CrossPartId] = crossPartId;
                }
            }
            else
            {

                finalErrorFlag = true;
                Append(allErrMessages, Constants.ColumnName.CrossPartId + " is not valid.");
                errorCount = 1;
            }

            //price to quote
            if (!ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.PriceToQuote, priceToQuote, out errMessages, out errorFlag))
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }
            else
            {
                try
                {
                    if (!ExcelValidationHelper.IsDecimalInRange(Constants.ColumnName.PriceToQuote, Convert.ToDecimal(priceToQuote), (decimal)0.0001, 9999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        resultsDataRow[Constants.ColumnName.PriceToQuote] = priceToQuote;
                        resultsDataRowToSaveInDB[Constants.ColumnName.PriceToQuote] = priceToQuote;
                    }
                }
                catch
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, Constants.ColumnName.PriceToQuote + " is not valid.");
                    errorCount = 1;
                }
            }

            //suggested price
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.SuggestedPrice, suggestedPrice, out errMessages, out errorFlag))
            {
                try
                {
                    if (!ExcelValidationHelper.IsDecimalInRange(Constants.ColumnName.SuggestedPrice, Convert.ToDecimal(suggestedPrice), (decimal)0.0001, 9999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        resultsDataRow[Constants.ColumnName.SuggestedPrice] = suggestedPrice;
                        resultsDataRowToSaveInDB[Constants.ColumnName.SuggestedPrice] = suggestedPrice;
                    }
                }
                catch
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, Constants.ColumnName.SuggestedPrice + " is not valid.");
                    errorCount = 1;
                }
            }

            //ICost
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.ICOST, iCost, out errMessages, out errorFlag))
            {
                try
                {
                    if (!ExcelValidationHelper.IsDecimalInRange(Constants.ColumnName.ICOST, Convert.ToDecimal(iCost), (decimal)0.0001, 9999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        resultsDataRow[Constants.ColumnName.ICOST] = iCost;
                        resultsDataRowToSaveInDB[Constants.ColumnName.ICOST] = iCost;
                    }
                }
                catch
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, Constants.ColumnName.ICOST + " is not valid.");
                    errorCount = 1;
                }
            }


            //Margin
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.Margin, margin, out errMessages, out errorFlag))
            {
                try
                {
                    if (!ExcelValidationHelper.IsDecimalInRange(Constants.ColumnName.Margin, Convert.ToDecimal(margin), (decimal)-9999999, 9999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        resultsDataRow[Constants.ColumnName.Margin] = margin;
                        resultsDataRowToSaveInDB[Constants.ColumnName.Margin] = margin;
                    }
                }
                catch
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, Constants.ColumnName.Margin + " is not valid.");
                    errorCount = 1;
                }
            }

            //Adjusted ICost
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.AdjustedICost, adjustedICost, out errMessages, out errorFlag))
            {
                try
                {
                    if (!ExcelValidationHelper.IsDecimalInRange(Constants.ColumnName.AdjustedICost, Convert.ToDecimal(adjustedICost), (decimal)0.0001, 9999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        resultsDataRow[Constants.ColumnName.AdjustedICost] = adjustedICost;
                        resultsDataRowToSaveInDB[Constants.ColumnName.AdjustedICost] = adjustedICost;
                    }
                }
                catch
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, Constants.ColumnName.AdjustedICost + " is not valid.");
                    errorCount = 1;
                }
            }

            //Adjusted Margin
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.AdjustedMargin, adjustedMargin, out errMessages, out errorFlag))
            {
                try
                {
                    if (!ExcelValidationHelper.IsDecimalInRange(Constants.ColumnName.AdjustedMargin, Convert.ToDecimal(adjustedMargin), (decimal)-9999999, 9999999, out errMessages, out errorFlag))
                    {
                        finalErrorFlag = errorFlag;
                        Append(allErrMessages, errMessages);
                        errorCount = 1;
                    }
                    else
                    {
                        resultsDataRow[Constants.ColumnName.AdjustedMargin] = adjustedMargin;
                        resultsDataRowToSaveInDB[Constants.ColumnName.AdjustedMargin] = adjustedMargin;
                    }
                }
                catch
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, Constants.ColumnName.AdjustedMargin + " is not valid.");
                    errorCount = 1;
                }
            }

            if (finalErrorFlag)
            {
                resultsDataRow[Constants.ColumnName.UploadStatus] = Constants.Error;
                resultsDataRow[Constants.ColumnName.Remarks] = allErrMessages.ToString();

                resultsDataRowToSaveInDB[Constants.ColumnName.UploadStatus] = Constants.Error;
                resultsDataRowToSaveInDB[Constants.ColumnName.Remarks] = allErrMessages.ToString();
            }
            else
            {
                resultsDataRow[Constants.ColumnName.UploadStatus] = Constants.Success;
                resultsDataRowToSaveInDB[Constants.ColumnName.UploadStatus] = Constants.Success;
            }

            _resultsDataTable.Rows.Add(resultsDataRow);
            _resultsDataTableToSaveInDB.Rows.Add(resultsDataRowToSaveInDB);
            return errorCount;
        }

        private void Append(StringBuilder sb, string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (sb.Length > 0)
                sb.Append(", ");

            sb.Append(message);
        }

        /// <summary>
        /// Init Column Indices
        /// </summary>
        /// <param name="sheetVector"></param>
        /// <param name="workSheet"></param>
        private void InitColumnIndices(object[,] sheetVector, int columnCount)
        {
            if (columnCount < _requiredColumnCount)
            {
                throw new Exception("File is not valid, required column(s) not present in the sheet:");
            }

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
            {
                string columnName = Convert.ToString(sheetVector[0, columnIndex]);

                if (columnName == Constants.ColumnName.BidPartId) _columnIndexMap[Constants.ColumnName.BidPartId] = columnIndex;

                if (columnName == Constants.ColumnName.CrossPartId) _columnIndexMap[Constants.ColumnName.CrossPartId] = columnIndex;

                if (columnName == Constants.ColumnName.PriceToQuote) _columnIndexMap[Constants.ColumnName.PriceToQuote] = columnIndex;

                if (columnName == Constants.ColumnName.SuggestedPrice) _columnIndexMap[Constants.ColumnName.SuggestedPrice] = columnIndex;

                if (columnName == Constants.ColumnName.ICOST) _columnIndexMap[Constants.ColumnName.ICOST] = columnIndex;

                if (columnName == Constants.ColumnName.Margin) _columnIndexMap[Constants.ColumnName.Margin] = columnIndex;

                if (columnName == Constants.ColumnName.AdjustedICost) _columnIndexMap[Constants.ColumnName.AdjustedICost] = columnIndex;

                if (columnName == Constants.ColumnName.AdjustedMargin) _columnIndexMap[Constants.ColumnName.AdjustedMargin] = columnIndex;


            }

            if (_columnIndexMap.ContainsValue(-1))
            {
                throw new Exception("File is not valid, required column(s) not present in the sheet. <br /> Please download the pricing data excel sheet.");
            }
        }

    }

    public class BulkUploadPartMarkWonHelper
    {
        private string _filePath;
        private Dictionary<string, int> _columnIndexMap;
        private DataTable _resultsDataTable;
        private DataTable _resultsDataTableToSaveInDB;
        private ICompletedBidsRepository _completedBidsRepository;
        private const int _requiredColumnCount = 3;

        public BulkUploadPartMarkWonHelper(string filePath, ICompletedBidsRepository completedBidsRepository, string userID, string userRole)
        {
            _filePath = filePath;
            _completedBidsRepository = completedBidsRepository;
            _resultsDataTable = ResultsTable();
            _resultsDataTableToSaveInDB = ResultsTableToSaveInDB();
            _columnIndexMap = new Dictionary<string, int>()
                        {
                            { Constants.ColumnName.IsPartWon,-1 },
                            { Constants.ColumnName.BidPartId,-1 },
                            { Constants.ColumnName.CrossPartId,-1 },

                        };
        }

        /// <summary>
        /// Upload Pricing Data excel file to db
        /// </summary>
        /// <returns></returns>
        public int UploadPartMarkWonData(int bidId)
        {
            int totalErrors = 0;

            using (var package = new ExcelPackage(new System.IO.FileInfo(_filePath)))
            {
                var workSheet = package.Workbook.Worksheets.First();
                if (workSheet.Dimension == null)
                {
                    throw new Exception("File is not valid, required column(s) not present in the sheet. <br /> Please download the template.");
                }
                object[,] sheetVector = new object[workSheet.Dimension.End.Row, workSheet.Dimension.End.Column];
                sheetVector = (object[,])workSheet.Cells.Value;

                //Method to check if all columns present in uploaded excel
                InitColumnIndices(sheetVector, workSheet.Dimension.End.Column);

                //prepare the results datatables
                if (workSheet.Dimension.End.Row > 1)
                {
                    for (int rowIndex = 1; rowIndex < workSheet.Dimension.End.Row; rowIndex++)
                    {
                        // Validate the uploaded excel data and get the error count
                        totalErrors += ValidateAndConvertPartMarkWonDataRow(sheetVector, rowIndex, bidId);
                    }
                }
                else
                {
                    throw new Exception("Data not present in the file.");
                }

                //totalErrors += MarkDuplicates();

                // Removed extra columns from data table to send to save in db
                for (int i = _resultsDataTableToSaveInDB.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = _resultsDataTableToSaveInDB.Rows[i];
                    if (dr[Constants.ColumnName.UploadStatus].ToString() == "Error")
                        dr.Delete();
                }
                _resultsDataTableToSaveInDB.Columns.Remove(Constants.ColumnName.UploadStatus);
                _resultsDataTableToSaveInDB.Columns.Remove(Constants.ColumnName.Remarks);

                _resultsDataTableToSaveInDB.AcceptChanges();

                _completedBidsRepository.BulkInsertPartMarkWonData(_resultsDataTableToSaveInDB, bidId);

                if (package.Workbook.Worksheets.Any(x => x.Name == "Upload Result"))
                {
                    package.Workbook.Worksheets.Delete(package.Workbook.Worksheets.Where(x => x.Name == "Upload Result").First());
                }

                ExcelWorksheet resultSheet = package.Workbook.Worksheets.Add("Upload Result");
                resultSheet.Cells["A1"].LoadFromDataTable(_resultsDataTable, true);
                package.Save();
            }

            return totalErrors;
        }

        private DataTable ResultsTable()
        {
            var dt = new DataTable();
            dt.Columns.Add(Constants.ColumnName.IsPartWon, typeof(string));
            dt.Columns.Add(Constants.ColumnName.BidPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.CrossPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.UploadStatus, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Remarks, typeof(string));

            return dt;
        }

        private DataTable ResultsTableToSaveInDB()
        {
            var dt = new DataTable();
            dt.Columns.Add(Constants.ColumnName.IsPartWon, typeof(bool));
            dt.Columns.Add(Constants.ColumnName.BidPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.CrossPartId, typeof(int));
            dt.Columns.Add(Constants.ColumnName.UploadStatus, typeof(string));
            dt.Columns.Add(Constants.ColumnName.Remarks, typeof(string));

            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="sheetVector"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private int ValidateAndConvertPartMarkWonDataRow(object[,] sheetVector, int rowIndex, int bidId)
        {
            DataRow resultsDataRow = _resultsDataTable.NewRow();
            DataRow resultsDataRowToSaveInDB = _resultsDataTableToSaveInDB.NewRow(); ;

            bool errorFlag = false;
            bool finalErrorFlag = false;
            StringBuilder allErrMessages = new StringBuilder();
            string errMessages = "";
            int errorCount = 0;

            string isPartWon = Convert.ToString(sheetVector[rowIndex, _columnIndexMap
               [Constants.ColumnName.IsPartWon]]).ToUpper().Trim();

            string bidPartId = Convert.ToString(sheetVector[rowIndex, _columnIndexMap
                [Constants.ColumnName.BidPartId]]).Trim();

            string crossPartId = Convert.ToString(sheetVector[rowIndex, _columnIndexMap[Constants.ColumnName.CrossPartId]]).Trim();


            //Is Part Won
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.IsPartWon, isPartWon, out errMessages, out errorFlag))
            {
                if (isPartWon.Equals("YES"))
                {
                    resultsDataRow[Constants.ColumnName.IsPartWon] = isPartWon;
                    resultsDataRowToSaveInDB[Constants.ColumnName.IsPartWon] = true;
                }
                else if (isPartWon.Equals("NO"))
                {
                    resultsDataRow[Constants.ColumnName.IsPartWon] = isPartWon;
                    resultsDataRowToSaveInDB[Constants.ColumnName.IsPartWon] = false;
                }
                else if (isPartWon.Equals(""))
                {
                    resultsDataRow[Constants.ColumnName.IsPartWon] = isPartWon;
                    resultsDataRowToSaveInDB[Constants.ColumnName.IsPartWon] = false;
                }
                else
                {
                    finalErrorFlag = true;
                    Append(allErrMessages, "Value should be Yes or No only.");
                    errorCount = 1;
                }
            }

            //Bid Part Id
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.BidPartId, bidPartId, out errMessages, out errorFlag))
            {
                if (!ExcelValidationHelper.IsAllDigits(Constants.ColumnName.BidPartId, bidPartId, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else if (!ExcelValidationHelper.IsNumberInRange(Constants.ColumnName.BidPartId, Convert.ToInt32(bidPartId), 1, 2147483647, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else
                {
                    resultsDataRow[Constants.ColumnName.BidPartId] = bidPartId;
                    resultsDataRowToSaveInDB[Constants.ColumnName.BidPartId] = bidPartId;
                }
            }
            else
            {
                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }

            //Cross Part Id
            if (ExcelValidationHelper.IsNullOrWhiteSpaceColumn(Constants.ColumnName.CrossPartId, crossPartId, out errMessages, out errorFlag))
            {
                if (!ExcelValidationHelper.IsAllDigits(Constants.ColumnName.CrossPartId, crossPartId, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else if (!ExcelValidationHelper.IsNumberInRange(Constants.ColumnName.BidPartId, Convert.ToInt32(crossPartId), 1, 2147483647, out errMessages, out errorFlag))
                {
                    finalErrorFlag = errorFlag;
                    Append(allErrMessages, errMessages);
                    errorCount = 1;
                }
                else
                {
                    resultsDataRow[Constants.ColumnName.CrossPartId] = crossPartId;
                    resultsDataRowToSaveInDB[Constants.ColumnName.CrossPartId] = crossPartId;
                }
            }
            else
            {

                finalErrorFlag = errorFlag;
                Append(allErrMessages, errMessages);
                errorCount = 1;
            }


            if (finalErrorFlag)
            {
                resultsDataRow[Constants.ColumnName.UploadStatus] = Constants.Error;
                resultsDataRow[Constants.ColumnName.Remarks] = allErrMessages.ToString();

                resultsDataRowToSaveInDB[Constants.ColumnName.UploadStatus] = Constants.Error;
                resultsDataRowToSaveInDB[Constants.ColumnName.Remarks] = allErrMessages.ToString();
            }
            else
            {
                resultsDataRow[Constants.ColumnName.UploadStatus] = Constants.Success;
                resultsDataRowToSaveInDB[Constants.ColumnName.UploadStatus] = Constants.Success;
            }

            _resultsDataTable.Rows.Add(resultsDataRow);
            _resultsDataTableToSaveInDB.Rows.Add(resultsDataRowToSaveInDB);
            return errorCount;
        }

        private void Append(StringBuilder sb, string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (sb.Length > 0)
                sb.Append(", ");

            sb.Append(message);
        }

        /// <summary>
        /// Init Column Indices
        /// </summary>
        /// <param name="sheetVector"></param>
        /// <param name="workSheet"></param>
        private void InitColumnIndices(object[,] sheetVector, int columnCount)
        {
            if (columnCount < _requiredColumnCount)
            {
                throw new Exception("File is not valid, required column(s) not present in the sheet:");
            }

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
            {
                string columnName = Convert.ToString(sheetVector[0, columnIndex]);

                if (columnName == Constants.ColumnName.IsPartWon) _columnIndexMap[Constants.ColumnName.IsPartWon] = columnIndex;

                if (columnName == Constants.ColumnName.BidPartId) _columnIndexMap[Constants.ColumnName.BidPartId] = columnIndex;

                if (columnName == Constants.ColumnName.CrossPartId) _columnIndexMap[Constants.ColumnName.CrossPartId] = columnIndex;

            }

            if (_columnIndexMap.ContainsValue(-1))
            {
                throw new Exception("File is not valid, required column(s) not present in the sheet. <br /> Please download the completed bid pricing data excel sheet.");
            }
        }

    }
}