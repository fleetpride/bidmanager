﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    public class Logger : ILogger
    {
        NLog.Logger logger;
        private string _bmConnString;
        private bool _throwLoggingErrors = true;

        /// <summary>
        /// Default constructer
        /// </summary>
        public Logger()
        {
            try
            {
                logger = NLog.LogManager.GetCurrentClassLogger();
                _bmConnString = ConfigurationManager.ConnectionStrings["BM"].ConnectionString;
                _throwLoggingErrors = Convert.ToBoolean(ConfigurationManager.AppSettings["ThrowLoggingErrors"]);
            }
            catch
            {
                if (_throwLoggingErrors)
                    throw;
            }
        }

        /// <summary>
        /// Helper Method to insert error/exception in database
        /// </summary>
        /// <param name="errorDateTime"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorStackTrace"></param>
        /// <returns></returns>
        private void LogToDB(string errorMessage, string errorStackTrace)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["DBErrorLoggingAllowed"]))
            {
                System.Security.Principal.IPrincipal iuser = System.Web.HttpContext.Current.User;
                using (SqlConnection conn = new SqlConnection(_bmConnString))
                {
                    using (SqlCommand cmd = new SqlCommand("BM.ErrorLogging_Insert", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@vErrorDateTime", SqlDbType.VarChar)).Value = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss ");
                        cmd.Parameters.Add(new SqlParameter("@vErrorMessage", SqlDbType.VarChar)).Value = errorMessage;
                        cmd.Parameters.Add(new SqlParameter("@vErrorStackTrace", SqlDbType.VarChar)).Value = errorStackTrace;
                        cmd.Parameters.Add(new SqlParameter("@vDeployedEnvironment", SqlDbType.VarChar)).Value = ConfigurationManager.AppSettings["DeployedEnvironment"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@vUserId", SqlDbType.VarChar)).Value = System.Web.HttpContext.Current.User.Identity.Name;
                        conn.Open();
                        cmd.ExecuteScalar();
                    }
                }
            }
        }

        public void Exception(Exception exp)
        {
            try
            {
                logger.Error(exp, "Excption occured.");
                this.LogToDB(exp.Message, exp.StackTrace);
            }
            catch
            {
                if (_throwLoggingErrors)
                    throw;
            }
        }

        public void Message(string msg)
        {
            try
            {
                logger.Info(msg);
                this.LogToDB(msg, "Info");
            }
            catch
            {
                if (_throwLoggingErrors)
                    throw;
            }
        }

        public void DebugMessage(string msg)
        {
            try
            {
                logger.Debug(msg);
                this.LogToDB(msg, "Debug");
            }
            catch
            {
                if (_throwLoggingErrors)
                    throw;
            }
        }

        public void FatalError(string msg)
        {
            try
            {
                logger.Fatal(msg);
                this.LogToDB(msg, "Fatal");
            }
            catch
            {
                if (_throwLoggingErrors)
                    throw;
            }
        }

        public static void QuickLog(string msg)
        {
            Logger logger = new Logger();
            logger.Message(msg);
        }
    }
}