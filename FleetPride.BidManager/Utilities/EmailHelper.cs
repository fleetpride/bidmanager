﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Net.Mail;
using FleetPride.BidManager.Models;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mime;
using System.IO;

namespace FleetPride.BidManager.Utilities
{
    public class EmailHelper : IEmailHelper
    {
        /// <summary>
        /// helper method to send email with all email details
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public void SendEmail(string toEmail, string subject, string body, string cc = "")
        {
            if (toEmail == null) return;
            toEmail = toEmail.Trim().Trim(',');
            if (toEmail.Trim().Length == 0)
            {
                throw new Exception("Email with subject : \"" + subject + "\" could not be delivered as the recipient address is blank. Please contact the admin.");
            }
            if (cc == null) return;
            cc = cc.Trim().Trim(',');
            Logger.QuickLog("Sending email to: " + toEmail + "CC List:" + cc);
            //Setup the email objects and send email
            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage();
            message.To.Add(toEmail);
            if (cc.Trim().Length > 0)
            {
                message.CC.Add(cc);
            }
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            client.Send(message);
        }

        public void SendEmailWithAttachment(string toEmail, string subject, string body, string path, string cc = "")
        {
            if (toEmail == null) return;
            toEmail = toEmail.Trim().Trim(',');
            if (toEmail.Trim().Length == 0)
            {
                throw new Exception("Email with subject : \"" + subject + "\" could not be delivered as the recipient address is blank. Please contact the admin.");
            }
            if (cc == null) return;
            cc = cc.Trim().Trim(',');
            Logger.QuickLog("Sending email to: " + toEmail + "CC List:" + cc);
            //Setup the email objects and send email
            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage();
            message.To.Add(toEmail);
            if (cc.Trim().Length > 0)
            {
                message.CC.Add(cc);
            }

            if (!string.IsNullOrWhiteSpace(path))
            {
                Attachment attachment = new Attachment(path);
                ContentDisposition disposition = attachment.ContentDisposition;
                disposition.CreationDate = File.GetCreationTime(path);
                disposition.ModificationDate = File.GetLastWriteTime(path);
                disposition.ReadDate = File.GetLastAccessTime(path);
                disposition.FileName = Path.GetFileName(path);
                disposition.Size = new FileInfo(path).Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                message.Attachments.Add(attachment);
            }
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            client.Send(message);
        }

        public void SendNotificationAfterBidCreated(BidEntity bid)
        {
            AuthorizationRepository _authRepo = new AuthorizationRepository();
            //send the email to the NAM Validators and Supervisors

            //both national and regional bids send notifications to super users.
            string to = CollectEmails(_authRepo.GetSuperUsers());

            if (bid.BidType == Constants.National)
            {
                //in case of national bids send notifications to NAM Validators and Supervisors
                to = AppendEmails(to, CollectEmails(_authRepo.GetNAMValidators()));
                to = AppendEmails(to, CollectEmails(_authRepo.GetNAMSupervisors()));
            }
            else if (bid.BidType == Constants.Regional)
            {
                //in case of regional bids send notifications to RAM Validators and Supervisors
                to = AppendEmails(to, CollectEmails(_authRepo.GetRAMValidators()));
                to = AppendEmails(to, CollectEmails(_authRepo.GetRAMSupervisors()));
            }
            else
            {
                //in case of regional bid send notifications to the user who created the Bid
                to = AppendEmails(to, _authRepo.GetUserEmail(bid.CreatedBy));
            }

            if (to.Length > 1)
            {
                SendNotificationAfterBidCreated(bid.BidId, bid.BidName, to, bid.BidType);
            }
            else
            {
                Logger.QuickLog("SendNotificationAfterBidCreated BidID=" + bid.BidId + "No addresses found to send the notification.");
            }
        }

        public string AppendEmails(string listA, string listB)
        {
            if (!string.IsNullOrEmpty(listA))
            {
                if (!string.IsNullOrEmpty(listB))
                    return listA + "," + listB;
                else
                    return listA;
            }
            else
            {
                return listB;
            }
        }

        public void SendTATMissNotification(List<BidEmailEntity> bees)
        {
            AuthorizationRepository authRepo = new AuthorizationRepository();
            string superUserEmail = this.CollectEmails(authRepo.GetSuperUsers());
            string NAMValidatorEmail = this.CollectEmails(authRepo.GetNAMValidators());
            string NAMSupervisorsEmail = this.CollectEmails(authRepo.GetNAMSupervisors());
            string RAMValidatorEmail = this.CollectEmails(authRepo.GetRAMValidators());
            string RAMSupervisorsEmail = this.CollectEmails(authRepo.GetRAMSupervisors());
            string PricingTeamEmail = this.GetUsers("PricingTeam");//ConfigurationManager.AppSettings["PricingTeamEmailGroup"];
            foreach (BidEmailEntity bee in bees)
            {
                string to = "";
                string cc = "";
                string message = "";
                string link = "";
                string subject = "ETA Missed || " + bee.BidName;

                switch (bee.BidStatus)
                {
                    case Constants.CrossFinalization:
                        link = GetUrl("CrossReport", bee.BidId, bee.BidName);
                        message = string.Format("The crosses for the bid <b>\"{0}\"</b> need to be finalized without further delay. Please finalize the crosses or contact the bid manager to extend the due date", bee.BidName);
                        if (bee.BidType == Constants.National)
                        {
                            to = NAMValidatorEmail;
                            cc = string.Format("{0},{1},{2}", superUserEmail, bee.CreatorEmail, NAMSupervisorsEmail);
                        }
                        if (bee.BidType == Constants.Regional)
                        {
                            to = RAMValidatorEmail;
                            cc = string.Format("{0},{1},{2}", superUserEmail, bee.CreatorEmail, RAMSupervisorsEmail);
                        }
                        else
                        {
                            to = string.Format("{0},{1}", superUserEmail, bee.CreatorEmail);
                            cc = bee.RVPEmail;
                        }
                        break;
                    case Constants.CategoryManager:
                        link = GetUrl("CrossReport", bee.BidId, bee.BidName);
                        message = string.Format("The bid <b>\"{0}\"</b> needs to be verified by the Category Managers without further delay. Please verify the bid or contact the bid manager to extend the due date", bee.BidName);
                        to = bee.CMEmail;

                        if (bee.BidType == Constants.National)
                        {
                            cc = string.Format("{0},{1},{2}", superUserEmail, NAMSupervisorsEmail, bee.CreatorEmail);
                        }
                        else if (bee.BidType == Constants.Regional)
                        {
                            cc = string.Format("{0},{1},{2}", superUserEmail, RAMSupervisorsEmail, bee.CreatorEmail);
                        }
                        else
                        {
                            cc = string.Format("{0},{1},{2}", superUserEmail, bee.CreatorEmail, bee.RVPEmail);
                        }
                        break;
                    case Constants.PricingTeam:
                        link = GetUrl("PriceValidation", bee.BidId, bee.BidName);
                        message = string.Format("The pricing for the bid <b>\"{0}\"</b> needs to be finalized without further delay. Please finalize the pricing for the bid or contact the bid manager to extend the due date", bee.BidName);
                        to = PricingTeamEmail;
                        if (bee.BidType == Constants.National)
                        {
                            cc = string.Format("{0},{1},{2}", superUserEmail, NAMSupervisorsEmail, bee.CreatorEmail);
                        }
                        else if (bee.BidType == Constants.Regional)
                        {
                            cc = string.Format("{0},{1},{2}", superUserEmail, RAMSupervisorsEmail, bee.CreatorEmail);
                        }
                        else
                        {
                            cc = string.Format("{0},{1},{2}", superUserEmail, bee.CreatorEmail, bee.RVPEmail);
                        }
                        break;
                    case Constants.CrowdSourcing:
                        link = Convert.ToString(ConfigurationManager.AppSettings["BMUrl"]);
                        message = string.Format("The crowdsourcing for the bid <b>\"{0}\"</b> needs to be completed without further delay. Please get the crowdsourcing completed or contact the bid manager to extend the due date", bee.BidName);
                        if (bee.BidType == Constants.National)
                        {
                            to = string.Format("{0},{1}", superUserEmail, NAMSupervisorsEmail);
                        }
                        else if (bee.BidType == Constants.Regional)
                        {
                            to = string.Format("{0},{1}", superUserEmail, RAMSupervisorsEmail);
                        }
                        else
                        {
                            to = string.Format("{0},{1},{2}", superUserEmail, bee.CreatorEmail, bee.RVPEmail);
                        }
                        break;
                    case Constants.LineReview_CM:
                        link = GetUrl(Constants.PageUrls.InventoryVerificationUrl, bee.BidId, bee.BidName);
                        message = string.Format("Inventory review for the bid <b>\"{0}\"</b> needs to be completed without further delay.", bee.BidName);
                        to = bee.CMEmail;
                        break;
                    case Constants.LineReview_VP:
                        link = GetUrl(Constants.PageUrls.InventoryVerificationUrl, bee.BidId, bee.BidName);
                        message = string.Format("Inventory review for the bid <b>\"{0}\"</b> needs to be completed without further delay.", bee.BidName);
                        to = bee.CmoEmail;
                        break;

                }

                if (!string.IsNullOrEmpty(to))
                    SendEmail(to, subject, GetNotificationBody(message, bee.BidType, link), cc);
            }
        }

        private bool IsValidateEmail(string email)
        {
            if (!string.IsNullOrWhiteSpace(email))
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                return match.Success;
            }

            return false;
        }

        public string CollectEmails(List<UserEntity> users)
        {
            string emails = "";

            StringBuilder sb = new StringBuilder();
            foreach (UserEntity user in users)
            {
                string email = user.UserEmail;
                if (IsValidateEmail(email))
                {
                    sb.Append(email);
                    sb.Append(",");
                }
                else
                {
                    Logger.QuickLog("Invalid email for the user: " + user.UserId + ", Role: " + user.UserRole + ", Email: " + email);
                }
            }

            if (sb.Length > 1)
            {
                emails = sb.ToString(0, sb.Length - 1);
            }

            return emails;
        }

        public string CollectEmails(List<CMUserEntity> users)
        {
            string emails = "";

            StringBuilder sb = new StringBuilder();
            foreach (CMUserEntity user in users)
            {
                string email = user.CMEmail;
                if (!string.IsNullOrEmpty(email))
                {
                    sb.Append(email);
                    sb.Append(",");
                }
                else
                {
                    Logger.QuickLog("Email address not found for the user: " + user.CMUserId + ", Role: CategoryManager");
                }
            }

            if (sb.Length > 1)
            {
                emails = sb.ToString(0, sb.Length - 1);
            }

            return emails;
        }

        private void SendNotificationAfterBidCreated(int bidId, string bidName, string toEmail, string bidType)
        {
            string subject = "Crosses Generated || " + bidName;

            string BMurl = "";
            if (bidType == Constants.National || bidType == Constants.Regional)
            {
                BMurl = GetUrl("CrossReport", bidId, bidName);
            }
            else
            {
                BMurl = GetUrl("Home/AvailableCrosses", bidId, bidName);
            }

            string msg = "Crosses for bid <b>" + bidName + "</b> has been created and waiting for action.";

            this.SendEmail(toEmail, subject, GetNotificationBody(msg, bidType, BMurl));
        }

        public string GetNotificationBody(string message, string bidType, string BMurl)
        {
            StringBuilder emailbodyBuilder = new StringBuilder();
            emailbodyBuilder.Append("<HTML><BODY><TABLE><TR><TD style='font-family:Arial, Helvetica, sans-serif' >");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append(message);
            if (!string.IsNullOrEmpty(bidType))
            {
                emailbodyBuilder.Append("<BR/><BR/>");
                emailbodyBuilder.Append("Account Type: " + bidType);
            }
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("Click <a href=" + BMurl + ">here</a> to go to the Bid.");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("***This is an automated message. Please do not reply***");
            emailbodyBuilder.Append("</TD><TR></TABLE></BODY></HTML>");
            return emailbodyBuilder.ToString();
        }

        public void SendNotificationToFinalizeCrosses(string bidName, int bidId, List<CMUserEntity> cms)
        {
            string subject = "Finalize Crosses || " + bidName;
            string BMurl = GetUrl("CrossReport", bidId, bidName);
            string message = "You are requested to finalize the crosses for the bid: <B> " + bidName + " </B>";
            string emailBod = GetNotificationBody(message, "", BMurl);
            this.SendEmail(this.CollectEmails(cms), subject, emailBod);
        }

        public void SendNotificationToPricing(string bidName, int bidId)
        {
            //AuthorizationRepository authRepo = new AuthorizationRepository();
            // string PricingTeamEmail = this.CollectEmails(authRepo.GetPricingTeam());
            string PricingTeamEmail = this.GetUsers("PricingTeam");//ConfigurationManager.AppSettings["PricingTeamEmailGroup"];
            string subject = "Pricing || " + bidName;
            string BMurl = GetUrl("PriceValidation", bidId, bidName);
            string message = "You are requested to provide pricing information for the bid: <B>" + bidName + "</B>.";
            string emailBody = GetNotificationBody(message, "", BMurl);
            this.SendEmail(PricingTeamEmail, subject, emailBody);
        }

        public string GetUrl(string pageName, int bidId, string bidName)
        {
            return string.Format("{0}/{1}?BidId={2}&BidName={3}", ConfigurationManager.AppSettings["BMUrl"], pageName, bidId, Uri.EscapeUriString(bidName));
        }

        public void SendNotificationToDataTeam(string bidName, int bidId)
        {
            string dataTeamEmail = this.GetUsers("DataTeam");//ConfigurationManager.AppSettings["DataTeamEmailGroup"];
            string subject = "Data Team Report || " + bidName;
            string dataTeamUrl = GetUrl("Reports/DataTeamReport", bidId, bidName);
            string bidChangeUrl = GetUrl("Reports/BidChangeReport", bidId, bidName);
            StringBuilder emailbodyBuilder = new StringBuilder();
            emailbodyBuilder.Append("<HTML><BODY><TABLE><TR><TD style='font-family:Arial, Helvetica, sans-serif' >");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("You are requested to setup the parts for the bid: <B>" + bidName + "</B>");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("Click <a href=" + dataTeamUrl + ">here</a> to go to the Data Team Report.");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("Click <a href=" + bidChangeUrl + ">here</a> to go to the Bid Change Report.");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("***This is an automated message. Please do not reply***");
            emailbodyBuilder.Append("</TD><TR></TABLE></BODY></HTML>");

            if (string.IsNullOrWhiteSpace(dataTeamEmail))
                Logger.QuickLog("No emails found of the Category Directors.");
            else
                this.SendEmail(dataTeamEmail, subject, emailbodyBuilder.ToString());
        }

        public void SendNotificationToDemandTeam(string bidName, int bidId)
        {
            string demandTeamEmail = this.GetUsers("DemandTeam");//ConfigurationManager.AppSettings["DemandTeamEmailGroup"];
            string subject = "Demand Team Report || " + bidName;
            string demandTeamUrl = GetUrl("Reports/DemandTeamReport", bidId, bidName);
            string bidChangeUrl = GetUrl("Reports/BidChangeReport", bidId, bidName);

            StringBuilder emailbodyBuilder = new StringBuilder();
            emailbodyBuilder.Append("<HTML><BODY><TABLE><TR><TD style='font-family:Arial, Helvetica, sans-serif' >");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("You are requested to setup the parts for the bid: <B>" + bidName + "</B>");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("Click <a href=" + demandTeamUrl + ">here</a> to go to the Demand Team Report.");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("Click <a href=" + bidChangeUrl + ">here</a> to go to the Bid Change Report.");
            emailbodyBuilder.Append("<BR/><BR/>");
            emailbodyBuilder.Append("***This is an automated message. Please do not reply***");
            emailbodyBuilder.Append("</TD><TR></TABLE></BODY></HTML>");

            if (string.IsNullOrWhiteSpace(demandTeamEmail))
                Logger.QuickLog("No emails found of the Category Directors.");
            else
                this.SendEmail(demandTeamEmail, subject, emailbodyBuilder.ToString());
        }

        public void SendNotificationToCategoryDirector(string bidName, int bidId)
        {
            AuthorizationRepository authRepo = new AuthorizationRepository();
            string catDirectoryEmail = this.CollectEmails(authRepo.GetCategoryDirector());
            string subject = "Inventory Verification || " + bidName;
            string BMurl = GetUrl("Inventory/InventoryVerification", bidId, bidName);
            string message = "You are requested to perform inventory verification for the bid: <B>" + bidName + "</B>.";
            string emailBody = GetNotificationBody(message, "", BMurl);

            if (string.IsNullOrWhiteSpace(catDirectoryEmail))
                Logger.QuickLog("No emails found of the Category Directors.");
            else
                this.SendEmail(catDirectoryEmail, subject, emailBody);
        }

        public void SendNotificationToCategoryVP(string bidName, int bidId)
        {
            AuthorizationRepository authRepo = new AuthorizationRepository();
            string catVPEmail = this.CollectEmails(authRepo.GetCategoryVP());
            string subject = "Inventory Verification || " + bidName;
            string BMurl = GetUrl("Inventory/InventoryVerification", bidId, bidName);
            string message = "You are requested to perform inventory verification for the bid: <B>" + bidName + "</B>.";
            string emailBody = GetNotificationBody(message, "", BMurl);

            if (string.IsNullOrWhiteSpace(catVPEmail))
                Logger.QuickLog("No emails found of the Category VPs.");
            else
                this.SendEmail(catVPEmail, subject, emailBody);
        }

        public void SendNotificationToCM(string bidName, int bidId, List<CMUserEntity> cms)
        {
            string subject = "Inventory Verification || " + bidName;
            string BMurl = GetUrl("Inventory/InventoryVerification", bidId, bidName);
            string message = "You are requested to perform inventory verification for the bid: <B>" + bidName + "</B>.";
            string emailBody = GetNotificationBody(message, "", BMurl);
            string emails = this.CollectEmails(cms);

            if (string.IsNullOrWhiteSpace(emails))
                Logger.QuickLog("No emails found of the CMs Supervisors.");
            else
                this.SendEmail(emails, subject, emailBody);
        }

        public void SendNotificationToNAMOrBidCreator(string bidName, string emails)
        {
            string subject = "Bid Completed || " + bidName;
            StringBuilder sb = new StringBuilder();
            sb.Append("<HTML><BODY><TABLE><TR><TD style='font-family:Arial, Helvetica, sans-serif' >");
            sb.Append("<BR/><BR/>");
            sb.Append("<BR/><BR/>");
            sb.Append("Hello,");
            sb.Append("<BR/><BR/>");
            sb.Append("The bid: <B>" + bidName + "</B> is marked completed. ");
            sb.Append("<BR/><BR/>");
            sb.Append("<BR/><BR/>");
            sb.Append("Thanks");
            sb.Append("<BR/><BR/>");
            sb.Append("***This is an automated message. Please do not reply***");
            sb.Append("</TD><TR></TABLE></BODY></HTML>");

            if (string.IsNullOrWhiteSpace(emails))
                Logger.QuickLog(DateTime.Now.ToString() + " -- " + "No emails found for NAM.");
            else
                this.SendEmail(emails, subject, sb.ToString());
        }

        public void SendRevisedPartsNotification(string bidName, int bidId, string bidType)
        {
            string subject = "Revised Parts || " + bidName;
            string BMurl = GetUrl("Reports/RevisePartsReport", bidId, bidName);
            string message = "Some of the parts have been marked as Revised by reviewers in the bid: <B>" + bidName + "</B>.";
            string emailBody = GetNotificationBody(message, "", BMurl);
            AuthorizationRepository authRepo = new AuthorizationRepository();
            string emails = this.CollectEmails(authRepo.GetNAMSupervisors());
            if (string.IsNullOrWhiteSpace(emails))
                Logger.QuickLog("No emails found of NAM Supervisors.");
            else
                this.SendEmail(emails, subject, emailBody);
        }

        public void SendSupplyChainNotification(int bidId, string bidName, string comment, string path)
        {
            string subject = "Supply Chain || " + bidName;
            string BMurl = ConfigurationManager.AppSettings["BMUrl"].ToString() + "/CompletedBids/DownloadSupplyChainData?BidId=" + bidId + "&BidName=" + bidName;
            string message = "You have a message for bid: <B>" + bidName + "</B>";
            message = message + "<br/><br/>" + "\""+ comment + "\"";
            StringBuilder sb = new StringBuilder();
            sb.Append("<HTML><BODY><TABLE><TR><TD style='font-family:Arial, Helvetica, sans-serif' >");
            sb.Append("Hello,");
            sb.Append("<BR/>");            
            sb.Append(message);
            sb.Append("<BR/><BR/>");
            sb.Append("<BR/><BR/>");
            sb.Append("Thanks");
            sb.Append("<BR/><BR/>");
            sb.Append("***This is an automated message. Please do not reply***");
            sb.Append("</TD><TR></TABLE></BODY></HTML>");

            string emails = GetUsers("SupplyChainTeam");
            if (string.IsNullOrWhiteSpace(emails))
                Logger.QuickLog("No emails found of Supply Chain");
            else
                this.SendEmailWithAttachment(emails, subject, sb.ToString(), path);

        }

        private string GetUsers(string groupType)
        {
            string lstEmail = string.Empty;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["BM"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("BM.GetEmailIdFromGroup", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@groupType", SqlDbType.VarChar)).Value = groupType;
                    cmd.CommandTimeout = 300;
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            
                            lstEmail = Convert.ToString(rdr["Email"]);
                        }
                    }
                }
            }
            return lstEmail;
        }
    }
}