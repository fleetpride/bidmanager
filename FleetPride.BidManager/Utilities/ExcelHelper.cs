﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    public class ExcelHelper : IExcelHelper
    {
        public void ReturnExcel(DataSet dsCross, string fileName, HttpResponseBase Response)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                foreach (DataTable dt in dsCross.Tables)
                {
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add(dt.TableName);
                    worksheet.Row(1).Height = 20;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Cells["A1"].LoadFromDataTable(dt, true);
                    worksheet.View.FreezePanes(2, 1);
                    worksheet.Cells.AutoFitColumns();
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.AddHeader("status", "210");
                    Response.AddHeader("X-Requested-With", "XMLHttpRequest");
                    pck.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public void ReturnExcel(DataTable dt, string tableName, string fileName, HttpResponseBase Response)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add(tableName);
                worksheet.Row(1).Height = 20;
                worksheet.Row(1).Style.Font.Bold = true;
                worksheet.Cells["A1"].LoadFromDataTable(dt, true);

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.AddHeader("status", "210");
                    Response.AddHeader("X-Requested-With", "XMLHttpRequest");
                    pck.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public void SaveExcel(DataSet dsCross, string path, string fileName, HttpResponseBase Response)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                foreach (DataTable dt in dsCross.Tables)
                {
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add(dt.TableName);
                    worksheet.Row(1).Height = 20;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Cells["A1"].LoadFromDataTable(dt, true);
                    worksheet.View.FreezePanes(2, 1);
                    worksheet.Cells.AutoFitColumns();
                }

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BufferOutput = true;
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.AddHeader("status", "210");
                    Response.AddHeader("X-Requested-With", "XMLHttpRequest");
                    pck.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }                    
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}