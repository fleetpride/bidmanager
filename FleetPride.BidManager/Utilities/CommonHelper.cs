﻿using FleetPride.BidManager.Utilities;
using System;
using System.Configuration;
using System.Linq;

namespace FleetPride.BidManager.Utilities
{
    public class CommonHelper
    {

        /// <summary>
        /// Helper method to make first letter capital of a string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        /// <summary>
        /// Helper method to trim domain Id
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string TrimDoaminId(string s)
        {
            if (s.Length <= 10)
            {
                return s;
            }
            else
            {
                return s = s.Substring(0, 10);
            }
        }        

        public static object IsDBNull(object value, object alternate)
        {
            return DBNull.Value == value ? alternate : value;
        }

        /// <summary>
        /// Replace ~ with the base url or simplay add the base url to the begening of the relateivePath
        /// ~\
        /// </summary>
        /// <param name="relativePath"></param>
        /// <returns></returns>
        internal static string GetUrl(string relativePath)
        {
            //replace all back slash ith forward slash
            relativePath = relativePath.Replace('\\', '/').TrimStart('/');
            string baseUrl = ConfigurationManager.AppSettings["BMUrl"].TrimEnd('/') + "/";

            if (relativePath[0] == '~')
                return relativePath.Replace("~", baseUrl);
            else
                return baseUrl + relativePath;
        }
    }
}
