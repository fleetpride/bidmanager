﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    /// <summary>
    /// Helper enums for user roles
    /// </summary>
    public enum UserRole
    {
        SuperUser,
        LocalUser,        
        NAMSupervisor,
        NAMValidator,
        NAM,
        PricingTeam,
        CategoryManager,
        CrossValidator,
        CrossEntryUser,
        CategoryDirector,
        CategoryVP,
        DataTeam,
        DemandTeam,
        RAM,
        RAMSupervisor,
        RAMValidator
    }
}