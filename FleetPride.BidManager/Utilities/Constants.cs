﻿
using System.Collections.Generic;

namespace FleetPride.BidManager.Utilities
{
    public static class Constants
    {
        //Message types or status
        public readonly static string Error = "Error";
        public readonly static string Success = "Success";

        //Cross Validation Status
        public readonly static string ValidationAwaited = "ValidationAwaited";
        public readonly static string Validated = "Validated";
        public readonly static string Rejected = "Rejected";
        public readonly static string Finalized = "Finalized";

        //Bid types
        public readonly static string National = "National";
        public readonly static string Regional = "Regional";
        public readonly static string Local = "Local";

        //Bid Status
        public const string Created = "Created";
        public const string GeneratingCrosses = "GeneratingCrosses";
        public const string CrossesGenerated = "CrossesGenerated";
        public const string CrowdSourcing = "CrowdSourcing";
        public const string CrossFinalization = "CrossFinalization";
        public const string CategoryManager = "CategoryManager";
        public const string PricingTeam = "PricingTeam";         
        public const string Completed = "Completed";       
        public const string Won = "Won";
        public const string Lost = "Lost";
        public const string InventoryUpdate = "InventoryUpdate";
        public const string InventoryReUpdate = "InventoryReUpdate";
        public const string LineReview_CM = "LineReview_CM";
        public const string LineReview_Dir = "LineReview_Dir";
        public const string LineReview_VP = "LineReview_VP";        
        //public const string InventoryApproved = "InventoryApproved";
        public const string DataTeam = "DataTeam";
        public const string DemandTeam = "DemandTeam";
        public const string LoadingInventoryReview = "LoadingInventoryReview";

        public const string NotFound = "NotFound";
        public const string BidLaunched = "BidLaunched";

        public struct ColumnName
        {
            public const string BidId = "Bid Id";
            public const string CustomerPartNumber = "Customer Part Number";
            public const string CleanCustomerPartNumber = "Clean Customer Part Number";
            public const string PartDescription = "Description";
            public const string Manufacturer = "Manufacturer";
            public const string ManufacturerPartNumber = "Manufacturer Part Number";
            public const string CleanManufacturerPartNumber = "Clean Manufacturer Part Number";
            public const string Note = "Note";
            public const string EstimatedAnnualUsage = "Estimated Annual Usage";
            public const string UploadStatus = "Upload Status";
            public const string Remarks = "Remarks";
            public const string UserId = "UserId";
            public const string UserRole = "UserRole";

            public const string IsPartWon = "Is Part Won (Yes/No)";
            public const string BidPartId = "Bid Part Id";
            public const string CrossPartId = "Cross Part Id";
            public const string PartNumber = "Part Number";
            public const string Source = "Source";
            public const string ReferenceTable = "Reference Table";
            public const string CrossPart = "Cross Part";
            public const string CrossPool = "Cross Pool";
            public const string CrossDescription = "Cross Description";
            public const string CrossCategory = "Cross Category";
            public const string VendorName = "Vendor Name";
            public const string VendorStatus = "Vendor Status";
            public const string CrossPreference = "Cross Preference";
            public const string PriceToQuote = "Price To Quote";
            public const string SuggestedPrice = "Suggested Price";
            public const string ICOST = "ICOST";
            public const string Margin = "Margin";
            public const string AdjustedICost = "Adjusted ICost";
            public const string AdjustedMargin = "Adjusted Margin";
        }

        public struct Page
        {
            public const string Home = "Home";
            public const string InventoryUpdate = "InventoryUpdate";
            public const string InventoryVerification = "InventoryVerification";
            public const string CrossValidation = "CrossValidation";
            public const string CrossEntry = "CrossEntry";
            public const string DataTeamReport = "DataTeamReport";
            public const string UploadBidParts = "UploadBidParts";
            public const string AvailableCrosses = "AvailableCrosses";
            public const string PriceValidation = "PriceValidation";
            public const string CrossReport = "CrossReport";
            public const string CrowdsourcingSummary = "CrowdsourcingSummary";
            public const string ProgressSummary = "ProgressSummary";
            public const string CompletedBids = "CompletedBids";
            public const string InventoryDetails = "InventoryDetails";
            public const string DemandTeamReport = "DemandTeamReport";
            public const string BidStatusReport = "BidStatusReport";
            public const string BidChangeReport = "BidChangeReport";
            public const string RevisePartsReport = "RevisePartsReport";
            public const string BidStatusReportForDataTeam = "BidStatusReportForDataTeam";
            public const string InventoryReport = "InventoryReport";
            public const string VendorGrade = "VendorGrade";
        }

        public struct PageUrls
        {
            public const string InventoryVerificationUrl = "Inventory/InventoryVerification";
        }

        public static CustomEnum BidStatusEnum;

        public static void Init()
        {
            BidStatusEnum = new CustomEnum();
            //                "Key",             "Short Description",       "Long Description"));
            BidStatusEnum.Add(Created,           "Upload Parts",            "Bid has been created and waiting for the parts to be uploaded.");
            BidStatusEnum.Add(GeneratingCrosses, "Creating Crosses",        "Parts uploaded. Now cross generation is in progress for the Bid.");
            BidStatusEnum.Add(CrossesGenerated,  "Crosses Created",         "System is done finding the crosses and ready for validating.");
            BidStatusEnum.Add(CrowdSourcing,     "Crowdsourcing",           "Crowd sourcing users are currently working on creating and validating the crosses.");
            BidStatusEnum.Add(CrossFinalization, "Cross Finalization",      "Bid validaters are working on finalizing the crosses.");
            BidStatusEnum.Add(CategoryManager,   "Cross Review by CM",        "Category managers are working on finalizing the crosses.");
            BidStatusEnum.Add(PricingTeam,       "Pricing Team",            "Pricing team hs working on finalizing the pricing.");
            BidStatusEnum.Add(Completed,         "Completed",               "Bid has been marked as complete and ready to be sent to the customer.");
            BidStatusEnum.Add(Won,               "Won",                     "Bid was won.");
            BidStatusEnum.Add(Lost,              "Lost",                    "Bid was lost.");
            BidStatusEnum.Add(InventoryUpdate,   "Inventory Update",        "Inventory for locations update");
            BidStatusEnum.Add(InventoryReUpdate, "Inventory Re-Update",     "Bid has been pulled back for inventory re-update");
            BidStatusEnum.Add(LineReview_CM,     "Inventory Review by CM",        "LineReview by CM");
            BidStatusEnum.Add(LineReview_Dir,    "Inventory Review by Director",  "LineReview by Director");
            BidStatusEnum.Add(LineReview_VP,     "Inventory Review by VP",        "LineReview by VP");
            //BidStatusEnum.Add(InventoryApproved, "Inventory Approved",      "Inventory Approved");
            BidStatusEnum.Add(DataTeam,          "Data Team",               "Sent to the Data Team");
            BidStatusEnum.Add(DemandTeam,        "Demand Team",             "Sent to the Demand Team");
            BidStatusEnum.Add(NotFound,          "Not Found",               "Bid not found.");
            BidStatusEnum.Add(LoadingInventoryReview, "Generating Inventory Review", "Generating Inventory Review");
            BidStatusEnum.Add(BidLaunched, "Bid Launched", "Bid is Launched");
        }        

        public struct OpportunityStage
        {
            public const string New = "01-New";
            public const string Won = "50-Closed Won";
            public const string Lost = "90-Closed Lost";
        }
    }
} 