﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    public interface IExcelHelper
    {
        void ReturnExcel(DataSet ds, string fileName, HttpResponseBase Response);
        void ReturnExcel(DataTable dt, string tableName, string fileName, HttpResponseBase Response);
        void SaveExcel(DataSet dsCross, string path, string fileName, HttpResponseBase Response);
    }
}
