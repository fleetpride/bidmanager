﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    public interface ILogger
    {
        void Exception(Exception exp);
        void Message(string msg);
        void DebugMessage(string msg);
        void FatalError(string msg);
    }
}