﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    /// <summary>
    /// contains the Bid Status
    /// </summary>
    public class CustomEnum
    {
        public readonly string Key;
        public readonly string ShortDescription;
        public readonly string LongDescription;
        private Dictionary<string, CustomEnum> _collection;

        public CustomEnum()
        {
            _collection = new Dictionary<string, CustomEnum>();
        }
        private CustomEnum(string key, string shortDescription, string longDescription)
        {
            this.Key = key;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
        }

        public void Add(string key, string shortDescription, string longDescription)
        {
            _collection.Add(key, new CustomEnum(key, shortDescription, longDescription));
        }

        public CustomEnum Get(string key)
        {
            if (!_collection.Keys.Contains(key))
                _collection.Add(key, new CustomEnum(key, "Invalid Status", "Invalid Bid Status"));
            return _collection[key];
        }
    }
}