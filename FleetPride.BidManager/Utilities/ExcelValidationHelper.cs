﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace FleetPride.BidManager.Utilities
{
    public class ExcelValidationHelper
    {
        /// <summary>
        /// Helper method to check the length, null/whitespace of string 
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="maxLength"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsValidStringLengthAndNotNullColumn(string columnName, string columnValue, int maxLength, out string errMessage, out bool errorFlag)
        {

            if (string.IsNullOrWhiteSpace(columnValue))
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else if (columnValue.Length > maxLength)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }

        }

        /// <summary>
        /// Method to check the length of string column
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="maxLength"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsValidStringLengthColumn(string columnName, string columnValue, int maxLength, out string errMessage, out bool errorFlag)
        {

            if (columnValue.Length > maxLength)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }

        }

        /// <summary>
        /// Helper method to check if the value is whole number/digit
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsAllDigits(string columnName, string columnValue, out string errMessage, out bool errorFlag)
        {
            foreach (char c in columnValue)
            {
                if (!Char.IsDigit(c))
                {
                    errMessage = columnName += " is not valid.";
                    errorFlag = true;
                    return false;
                }
                else
                {
                    errMessage = "";
                    errorFlag = false;
                    //return true;
                }
            }
            errMessage = "";
            errorFlag = false;
            return true;
        }

        /// <summary>
        /// Helper method to check if the value contains letters only
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsAllLetters(string columnName, string columnValue, out string errMessage, out bool errorFlag)
        {
            foreach (char c in columnValue)
            {
                if (!char.IsLetter(c))
                {
                    errMessage = columnName += " is not valid.";
                    errorFlag = true;
                    return false;
                }
                else
                {
                    errMessage = "";
                    errorFlag = false;
                    //return true;
                }
            }
            errMessage = "";
            errorFlag = false;
            return true;
        }

        /// <summary>
        /// Helper method to check null/emplty/whitespace of value
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpaceColumn(string columnName, string columnValue, out string errMessage, out bool errorFlag)
        {
            if (string.IsNullOrWhiteSpace(columnValue))
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }

        /// <summary>
        /// Helper method to check if the whole number value is in range with min and max
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="minRange"></param>
        /// <param name="maxRange"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsNumberInRange(string columnName, int? columnValue, int minRange, int maxRange, out string errMessage, out bool errorFlag)
        {
            if (columnValue < minRange)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else if (columnValue > maxRange)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }

        /// <summary>
        /// Helper method to check if the decimal value is in range with min and max
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="minRange"></param>
        /// <param name="maxRange"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsDecimalInRange(string columnName, decimal? columnValue, decimal minRange, decimal maxRange, out string errMessage, out bool errorFlag)
        {
            if (columnValue < minRange)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else if (columnValue > maxRange)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }

        /// <summary>
        /// Helper method to check to compare the value
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="comparisonValue"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool NumberCompare(string columnName, int columnValue, int comparisonValue, out string errMessage, out bool errorFlag)
        {
            if (columnValue < comparisonValue)
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;

            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }

        /// <summary>
        /// Helper method to check the decimal number with precision
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="numberFormat"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsValidNumberWithPrecisionColumn(string columnName, string columnValue, string numberFormat, out string errMessage, out bool errorFlag)
        {
            string numberToCheck = "1";
            string[] precision = numberFormat.Split(',');
            string[] arrNumberFormat = Convert.ToString(columnValue).Split('.');

            numberToCheck = numberToCheck.PadRight(Convert.ToInt32(precision[0]) + 1, '0');

            if (string.IsNullOrWhiteSpace(columnValue))
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else if (Convert.ToDecimal(columnValue) <= Convert.ToDecimal(numberToCheck))
            {
                if (precision.Count() == 1 && arrNumberFormat.Count() == 1)
                {
                    if (precision[0] != null && arrNumberFormat[0] != null)
                    {
                        if (Convert.ToInt32(precision[0]) >= arrNumberFormat[0].Length)
                        {
                            errMessage = "";
                            errorFlag = false;
                            return true;
                        }
                        else
                        {
                            errMessage = columnName += " is not valid.";
                            errorFlag = true;
                            return false;
                        }
                    }
                }
                else if (precision.Count() == 2 && arrNumberFormat.Count() == 1)
                {
                    if (precision[0] != null && arrNumberFormat[0] != null)
                    {
                        if (Convert.ToInt32(precision[0]) >= arrNumberFormat[0].Length)
                        {
                            if (Convert.ToInt32(precision[1]) == 0 || arrNumberFormat.Count() == 1)
                            {
                                errMessage = "";
                                errorFlag = false;
                                return true;
                            }
                            else
                            {
                                errMessage = columnName += " is not valid.";
                                errorFlag = true;
                                return false;
                            }
                        }
                    }
                }
                else if (precision.Count() == 1 && arrNumberFormat.Count() == 2)
                {
                    if (precision[0] != null && arrNumberFormat[0] != null)
                    {
                        if (Convert.ToInt32(precision[0]) >= arrNumberFormat[0].Length)
                        {
                            if (Convert.ToInt32(arrNumberFormat[1]) == 0)
                            {
                                errMessage = "";
                                errorFlag = false;
                                return true;
                            }
                            else
                            {
                                errMessage = columnName += " is not valid.";
                                errorFlag = true;
                                return false;
                            }
                        }
                    }
                }
                else if (precision.Count() == 2 && arrNumberFormat.Count() == 2)
                {
                    if (precision[0] != null && arrNumberFormat[0] != null)
                    {
                        if (Convert.ToInt32(precision[0]) >= arrNumberFormat[0].Length)
                        {
                            if (Convert.ToInt32(precision[1]) >= arrNumberFormat[1].Length)
                            {
                                errMessage = "";
                                errorFlag = false;
                                return true;
                            }
                            else
                            {
                                errMessage = columnName += " is not valid.";
                                errorFlag = true;
                                return false;
                            }
                        }
                    }
                }
                errMessage = "";
                errorFlag = false;
                return true;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }

        /// <summary>
        /// Helper method to check if the decimal number is in invalid format
        /// </summary>
        /// <param name="InputNumber"></param>
        /// <param name="NumberFormat"></param>
        /// <returns></returns>
        public static bool NumberIsInvalidFormat(decimal InputNumber, string NumberFormat)
        {
            try
            {
                if (!string.IsNullOrEmpty(NumberFormat.Trim()) && !string.IsNullOrEmpty(Convert.ToString(InputNumber).Trim()))
                {
                    string numberToCheck = "1";

                    string[] precision = NumberFormat.Split(',');
                    string[] numberFormat = Convert.ToString(InputNumber).Split('.');

                    numberToCheck = numberToCheck.PadRight(Convert.ToInt32(precision[0]) + 1, '0');

                    if (InputNumber <= Convert.ToDecimal(numberToCheck))
                    {
                        if (precision.Count() == 1 && numberFormat.Count() == 1)
                        {
                            if (precision[0] != null && numberFormat[0] != null)
                            {
                                if (Convert.ToInt32(precision[0]) >= numberFormat[0].Length)
                                {
                                    return false;
                                }
                            }
                        }
                        else if (precision.Count() == 2 && numberFormat.Count() == 1)
                        {
                            if (precision[0] != null && numberFormat[0] != null)
                            {
                                if (Convert.ToInt32(precision[0]) >= numberFormat[0].Length)
                                {
                                    if (Convert.ToInt32(precision[1]) == 0 || numberFormat.Count() == 1)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                        else if (precision.Count() == 1 && numberFormat.Count() == 2)
                        {
                            if (precision[0] != null && numberFormat[0] != null)
                            {
                                if (Convert.ToInt32(precision[0]) >= numberFormat[0].Length)
                                {
                                    if (Convert.ToInt32(numberFormat[1]) == 0)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                        else if (precision.Count() == 2 && numberFormat.Count() == 2)
                        {
                            if (precision[0] != null && numberFormat[0] != null)
                            {
                                if (Convert.ToInt32(precision[0]) >= numberFormat[0].Length)
                                {
                                    if (Convert.ToInt32(precision[1]) >= numberFormat[1].Length)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                return true;
            }
            return true;
        }

        /// <summary>
        /// Helper method to get the number from string
        /// </summary>
        /// <param name="InputString"></param>
        /// <returns></returns>
        public static string ExtractNumberFromString(string InputString)
        {
            try
            {
                Regex ex = new Regex(@"\p{Sc}");
                return InputString.Replace(ex.Match(InputString).Value, "");
            }
            catch
            {
                return "Error occured";
            }
        }

        /// <summary>
        /// Helper method to check the current year
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsCurrentYear(string columnName, string columnValue, out string errMessage, out bool errorFlag)
        {
            if (Convert.ToInt32(columnValue) != Convert.ToInt32(DateTime.Today.Year))
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }

        /// <summary>
        /// Helper method to check the current month
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="errMessage"></param>
        /// <param name="errorFlag"></param>
        /// <returns></returns>
        public static bool IsCurrentMonth(string columnName, string columnValue, out string errMessage, out bool errorFlag)
        {
            if (Convert.ToInt32(columnValue) != Convert.ToInt32(DateTime.Today.Month - 1))
            {
                errMessage = columnName += " is not valid.";
                errorFlag = true;
                return false;
            }
            else
            {
                errMessage = "";
                errorFlag = false;
                return true;
            }
        }
    }

}