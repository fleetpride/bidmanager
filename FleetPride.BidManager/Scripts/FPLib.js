﻿Array.prototype.getIndex = function(func)
{
    for (var i = 0; i < this.length; i += 1)
    {
        if (func(this[i]))
        {
            return i;
        }
    }
    return -1;
}