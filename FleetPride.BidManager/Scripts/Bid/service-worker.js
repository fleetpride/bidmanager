﻿//display notification
function FPNotify(title, desc, url) {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    }
    else {
        var notification = new Notification(title, {
            icon: '~/img/header-fleetlogo.png',
            body: desc,
        });

        /* Remove the notification from Notification Center when clicked.*/
        notification.onclick = function () {
            window.open(url);
        };

        /* Callback function when the notification is closed. */
        notification.onclose = function () {
            console.log('Notification closed');
        };
    }
}