﻿using FleetPride.BidManager.App_Start;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FleetPride.BidManager
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DevExtremeBundleConfig.RegisterBundles(BundleTable.Bundles);

            ExcelRepository irep = new ExcelRepository();
            string localExcelFolder = Server.MapPath(ConfigurationManager.AppSettings["LocalUploadExcelPath"]);
            irep.ClearCache(localExcelFolder);

            Constants.Init();

            FleetPride.BidManager.Utilities.Logger.QuickLog("Application started");
        }
        void Session_End(Object sender, EventArgs E)
        {
            ExcelRepository irep = new ExcelRepository();
            string localExcelFolder = Server.MapPath(ConfigurationManager.AppSettings["LocalUploadExcelPath"]);
            irep.ClearCache(localExcelFolder);
            FleetPride.BidManager.Utilities.Logger.QuickLog("Session ended");
        }
    }
}
