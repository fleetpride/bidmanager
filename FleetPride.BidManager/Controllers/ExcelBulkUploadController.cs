﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using FleetPride.Excel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class ExcelBulkUploadController : Controller
    {
        protected Utilities.Logger logger = new Utilities.Logger();
        IAuthorizationRepository _userInfoRepository;
        IExcelBulkUploadRepository _excelBulkUploadRepository;
        IExcelHelper _excelHelper;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public ExcelBulkUploadController(IAuthorizationRepository userInfoRepository, IExcelHelper excelHelper, IExcelBulkUploadRepository excelBulkUploadRepository)
        {
             _userInfoRepository = userInfoRepository;
            _excelHelper = excelHelper;
            _excelBulkUploadRepository = excelBulkUploadRepository;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult VendorCMRelation()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.CrossEntry))
            {
                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "CrossEntry");
            }
        }

        /// <summary>
        /// get all vendor cm relations
        /// </summary>
        /// <returns></returns>
        public JsonResult GetVendorCMRelations()
        {
            var vendorCMRelationsList = _excelBulkUploadRepository.GetVendorCMRelations();

            return new JsonResult()
            {
                Data = vendorCMRelationsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Upload vendor CM Relations file
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadVendorCMRelationFile()
        {
            return UploadVendorCMRelationData<VendorCMRelationEntity>();
        }

        /// <summary>
        /// Upload Vendor CM Relation File Data
        /// </summary>
        /// <typeparam name="ModelType"></typeparam>
        /// <returns></returns>
        protected JsonResult UploadVendorCMRelationData<ModelType>() where ModelType : class
        {
            int errorCount = 0;
            if (Request.Files.Count == 1)
            {
                try
                {
                    //save the uploaded file to the App_Data folder
                    string fileName = "~/App_Data/VendorCMRelation_UploadFile_" + System.Web.HttpContext.Current.User.Identity.Name + "_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xls";
                    Request.Files[0].SaveAs(Server.MapPath(fileName));

                    using (var package = new ExcelPackage(new System.IO.FileInfo(Server.MapPath(fileName))))
                    {
                        //read the data from the excel sheet
                        var workSheet = package.Workbook.Worksheets.First();
                        List<ModelType> records = workSheet.GetRecords<ModelType>();

                        //execute the data annotation based validations
                        GenericValidator.TryValidate(records);

                        //count the failed records
                        errorCount = records.Where(x => !((x as BaseExcelModel).IsValid)).Count();

                        //if there are some failed records then create a sheet in the original file with the status and error messages.
                        if (errorCount > 0)
                        {
                            ExcelWorksheet resultSheet = package.Workbook.Worksheets.AddOrReplace("Upload Result");
                            resultSheet.Cells["A1"].LoadFromCollection<ModelType>(records, true);
                            package.Save();
                        }

                        //save all the valid records to session
                        Session["ValidVendorCMRelationData"] = null;
                        Session["ValidVendorCMRelationData"] = records.FindAll(x => (x as BaseExcelModel).IsValid);
                        SaveValidVendorCMRelationData(Session["ValidVendorCMRelationData"] as List<VendorCMRelationEntity>);
                    }

                    return Json(new { error = false, totalError = errorCount, uploadStatus = (errorCount == 0), path = fileName });
                }
                catch (System.Reflection.TargetInvocationException ex)
                {
                    //ExceptionManager.LogExceptionDetails(ex);
                    logger.Exception(ex);
                    string msg = ex.Message;
                    if (ex.InnerException != null)
                        msg = ex.InnerException.Message;

                    return Json(new { errorText = msg, error = true, uploadStatus = false });
                }
                catch (Exception ex)
                {
                    logger.Exception(ex);
                    return Json(new { errorText = ex.Message, error = true, uploadStatus = false });
                }
            }

            return Json(new { error = true, uploadStatus = false });
        }

        /// <summary>
        /// Save valid records of Vendor CM Relation in bulk
        /// </summary>
        /// <param name="records"></param>
        private void SaveValidVendorCMRelationData(List<VendorCMRelationEntity> records)
        {
            _excelBulkUploadRepository.BulkSaveVendorCMRelations(records, System.Web.HttpContext.Current.User.Identity.Name);
        }

        /// <summary>
        /// Method to download the uploaded Vendor CM Relation excel of errors
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadVendorCMRelationErrorDataFile(string fileName)
        {
            return File(Server.MapPath(fileName), "excel", "VendorCMRelation_ErrorFile.xlsx");
        }
    }
}