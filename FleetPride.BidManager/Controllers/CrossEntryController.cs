﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class CrossEntryController : Controller
    {
        IAuthorizationRepository _authRepo;
        ICrowdSourcingRepository _crowdSourcingRepo;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public CrossEntryController(IAuthorizationRepository authRepo, ICrowdSourcingRepository crowdSourcingRepo)
        {
            _authRepo = authRepo;
            _crowdSourcingRepo = crowdSourcingRepo;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_authRepo.HasAccess(Constants.Page.CrossEntry))
            {
                Session["BidName"] = null;
                Session["BidId"] = null;
                ViewBag.Role = _authRepo.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Method to get the count/statistics for cross entry User
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCrossEntryUserCounts()
        {
            var entryCount = _crowdSourcingRepo.GetCrossEntryUserCounts(System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = entryCount,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get the Check Locked Entry Part For User
        /// </summary>
        /// <returns></returns>
        public JsonResult CheckLockedEntryPartForUser()
        {
            var result = _crowdSourcingRepo.CheckLockedEntryPartForUser(System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get the crowd sourcing part for the user
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPartForCrowdSourcing()
        {
            var crowdSourcePart = _crowdSourcingRepo.GetPartForCrowdSourcing(System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = crowdSourcePart,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to load the crosses for the bid part
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <returns></returns>
        public JsonResult GetCrossesForEntry(string bidPartId)
        {
            var crossesList = _crowdSourcingRepo.GetCrossesForEntry(Convert.ToInt32(bidPartId));
            return new JsonResult()
            {
                Data = crossesList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to check the part number in INMNPM table
        /// </summary>
        /// <param name="partNumber"></param>
        /// <returns></returns>
        public JsonResult CheckPartNumber(string partNumber, string bidId, bool matchPartial)
        {
            var validatedParts = _crowdSourcingRepo.CheckPartNumber(partNumber, Convert.ToInt32(bidId), matchPartial);
            return new JsonResult()
            {
                Data = validatedParts,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get all parts info
        /// </summary>
        /// <param name="bidType"></param>
        /// <returns></returns>
        public JsonResult GetAllPartInfo(string bidId, string filter)
        {
            string cacheKey = "partsList";
            var parts = CacheHelper.Get<List<Part>>(cacheKey);
            if (parts == null)
            {
                parts = _crowdSourcingRepo.GetAllPartInfo(Convert.ToInt32(bidId), _authRepo.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);
                CacheHelper.Add(cacheKey, parts);
            }

            filter = filter.ToUpper();
            var partInfoResult = parts.FindAll((a) => a.PartInfo.ToUpper().Contains(filter));


            return Json(new
            {
                Data = partInfoResult,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            });
        }

        /// <summary>
        /// Method to save the part numbers from DimProduct table
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <param name="partNo"></param>
        /// <param name="ssflip"></param>
        /// <param name="pools"></param>
        /// <param name="isCrossReport"></param>
        /// <returns></returns>
        public JsonResult SaveDbFoundPoolPart(int crowdSourcingPartId, string partNo, bool ssflip, List<int> pools, List<int> productKeys, int isCrossReport, string source)
        {
            int insertedId = _crowdSourcingRepo.SaveDbFoundPoolPart(crowdSourcingPartId, partNo, ssflip, pools, productKeys, isCrossReport, source, System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = insertedId,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to save the part numbers from DimProduct table using part or desc
        /// </summary>
        /// <param name="selectedRows"></param>
        /// <returns></returns>
        public JsonResult SaveSearchedDbFoundPoolPart(int crowdSourcingPartId, int isCrossReport, string source, List<Part> selectedRows)
        {
            _crowdSourcingRepo.SaveSearchedDbFoundPoolPart(crowdSourcingPartId, isCrossReport, source, selectedRows, System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to save user entered part details
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <param name="crossPartNo"></param>
        /// <param name="crossPoolNo"></param>
        /// <param name="crossPartDescription"></param>
        /// <param name="crossPartCategory"></param>
        /// <param name="isCrossReport"></param>
        /// <returns></returns>
        public JsonResult SaveManualPoolPart(int crowdSourcingPartId, string crossPartNo, int? crossPoolNo, string crossPartDescription, string crossPartCategory, int isCrossReport)
        {
            if (crossPartDescription != null)
                crossPartDescription = crossPartDescription.Trim().ToUpper();
            if (crossPartCategory != null)
                crossPartCategory = crossPartCategory.Trim().ToUpper();

            int insertedId = _crowdSourcingRepo.SaveManualPoolPart(crowdSourcingPartId, crossPartNo.ToUpper(), crossPoolNo, crossPartDescription, crossPartCategory, isCrossReport, System.Web.HttpContext.Current.User.Identity.Name);
            if (insertedId > 0)
            {
                return Json(new { status = true });
            }
            else
            {
                return Json(new { status = false });
            }
        }

        /// <summary>
        /// Method to submit the part from user
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <returns></returns>
        public JsonResult SubmitCrowdSourcingPart(string crowdSourcingPartId)
        {
            _crowdSourcingRepo.SubmitCrowdSourcingPart(Convert.ToInt32(crowdSourcingPartId), System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to release the part from user
        /// </summary>
        /// <param name="crowdSourcingPartId"></param>
        /// <returns></returns>
        public JsonResult ReleaseCrowdSourcingPart(string crowdSourcingPartId)
        {
            int insertedId = _crowdSourcingRepo.ReleaseCrowdSourcingPart(Convert.ToInt32(crowdSourcingPartId), System.Web.HttpContext.Current.User.Identity.Name);
            if (insertedId > 0)
            {
                return Json(new { status = true });
            }
            else
            {
                return Json(new { status = false });
            }

        }

        /// <summary>
        /// Method to delete the entered cross
        /// </summary>
        /// <param name="crossPartId"></param>
        /// <returns></returns>
        public JsonResult DeleteCross(string crossPartId)
        {
            _crowdSourcingRepo.DeleteCross(Convert.ToInt32(crossPartId));
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}