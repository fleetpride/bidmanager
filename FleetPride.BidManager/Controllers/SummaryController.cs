﻿using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class SummaryController : Controller
    {
        IAuthorizationRepository _userInfoRepository;
        ISummaryRepository _summaryRepository;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public SummaryController(IAuthorizationRepository userInfoRepository, ISummaryRepository summaryRepository)
        {
            _userInfoRepository = userInfoRepository;
            _summaryRepository = summaryRepository;
        }

        /// <summary>
        /// CrowdsourcingSummary action method
        /// </summary>
        /// <returns></returns>
        public ActionResult CrowdsourcingSummary()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.CrowdsourcingSummary))
            {
                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "CrossEntry");
            }
        }

        /// <summary>
        /// BidReviewSummary action method
        /// </summary>
        /// <returns></returns>
        public ActionResult ProgressSummary()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.ProgressSummary))
            {
                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "CrossEntry");
            }
        }

        public JsonResult GetCrowdsourcingSummary()
        {
            var crowdsourcingSummaryList = _summaryRepository.GetCrowdsourcingSummary(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = crowdsourcingSummaryList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetProgressSummary()
        {
            var progressSummaryList = _summaryRepository.GetProgressSummary(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = progressSummaryList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}