﻿using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class ReportsController : Controller
    {
        protected Utilities.Logger logger = new Utilities.Logger();

        IAuthorizationRepository _userInfoRepository;
        IReportsRepository _reportsRepository;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public ReportsController(IAuthorizationRepository userInfoRepository, IReportsRepository reportsRepository)
        {
            _userInfoRepository = userInfoRepository;
            _reportsRepository = reportsRepository;
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public ActionResult DataTeamReport()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.DataTeamReport))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Default action method for Demand Team Report
        /// </summary>
        /// <returns></returns>
        public ActionResult DemandTeamReport()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.DemandTeamReport))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Default action method for Demand Team Report
        /// </summary>
        /// <returns></returns>
        public ActionResult BidChangeReport()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.DemandTeamReport))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        public ActionResult InventoryReport()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.InventoryReport))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Default action method for Bid Status Report
        /// </summary>
        /// <returns></returns>
        public ActionResult BidStatusReport()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.BidStatusReport))
            {
                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        public ActionResult BidStatusReportForDataTeam()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.BidStatusReportForDataTeam))
            {
                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Get Demand Team Report Bids
        /// </summary>
        /// <returns></returns>
        public JsonResult DemandTeamReportBids()
        {
            var bidsList = _reportsRepository.DemandTeamReportBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);
            ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult RevisePartsReport()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.RevisePartsReport))
            {
                if (Request.QueryString["BidId"] != null )
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }
        public JsonResult RevisePartsBids()
        {
            var bidsList = _reportsRepository.RevisePartsBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetRevisePartsReport(int bidId)
        {
            var revisePartsReport = _reportsRepository.GetRevisePartsReport(bidId);
            ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();

            return new JsonResult()
            {
                Data = revisePartsReport,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get demand team report
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public JsonResult GetDemandTeamReport(int bidId)
        {
            string approvalType = "Approved";
            var demandTeamReport = _reportsRepository.GetDemandTeamReport(bidId, approvalType, System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());
            ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();

            return new JsonResult()
            {
                Data = demandTeamReport,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult LaunchBid(int bidId)
        {
            bool result = false;
            if (bidId <= 0)
            {
                result = false;
            }
            else
            {
                result = _reportsRepository.LaunchBid(bidId, System.Web.HttpContext.Current.User.Identity.Name);
            }

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        /// <summary>
        /// Method to get the Bid Status Report
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public JsonResult GetBidStatusReport()
        {
            var result = _reportsRepository.GetBidStatusReport(System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());
            ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        /// <summary>
        /// Method to get the Bid Status Report
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public JsonResult GetBidChangeHistory(int bidId)
        {
            var result = _reportsRepository.GetBidChangeReport(bidId);

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


    }
}