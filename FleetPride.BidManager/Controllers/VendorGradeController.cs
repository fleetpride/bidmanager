﻿using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using FleetPride.Excel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class VendorGradeController : Controller
    {
        protected Utilities.Logger logger = new Utilities.Logger();
        IAuthorizationRepository _userInfoRepository;
        IVendorGradeRepository _vendorGradeRepository;
        IExcelHelper _excelHelper;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public VendorGradeController(IAuthorizationRepository userInfoRepository, IExcelHelper excelHelper, IVendorGradeRepository vendorGradeRepository)
        {
            _userInfoRepository = userInfoRepository;
            _excelHelper = excelHelper;
            _vendorGradeRepository = vendorGradeRepository;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.VendorGrade))
            {
                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "CrossEntry");
            }
        }

        /// <summary>
        /// get all vendor grades
        /// </summary>
        /// <returns></returns>
        public JsonResult GetVendorGrades()
        {
            var vendorGaresList = _vendorGradeRepository.GetVendorGrades();

            return new JsonResult()
            {
                Data = vendorGaresList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Upload Vendor Grade File
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadVendorGradeFile()
        {
            return UploadVendorGradeData<VendorGradeEntity>();
        }

        /// <summary>
        /// Upload Vendor Grade File Data
        /// </summary>
        /// <typeparam name="ModelType"></typeparam>
        /// <returns></returns>
        protected JsonResult UploadVendorGradeData<ModelType>() where ModelType : class
        {
            int errorCount = 0;
            if (Request.Files.Count == 1)
            {
                try
                {
                    //save the uploaded file to the App_Data folder
                    string fileName = "~/App_Data/VendorGrade_UploadFile_" + System.Web.HttpContext.Current.User.Identity.Name + "_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xls";
                    Request.Files[0].SaveAs(Server.MapPath(fileName));

                    using (var package = new ExcelPackage(new System.IO.FileInfo(Server.MapPath(fileName))))
                    {
                        //read the data from the excel sheet
                        var workSheet = package.Workbook.Worksheets.First();
                        List<ModelType> records = workSheet.GetRecords<ModelType>();

                        //execute the data annotation based validations
                        GenericValidator.TryValidate(records);

                        //count the failed records
                        errorCount = records.Where(x => !((x as BaseExcelModel).IsValid)).Count();

                        //if there are some failed records then create a sheet in the original file with the status and error messages.
                        if (errorCount > 0)
                        {
                            ExcelWorksheet resultSheet = package.Workbook.Worksheets.AddOrReplace("Upload Result");
                            resultSheet.Cells["A1"].LoadFromCollection<ModelType>(records, true);
                            package.Save();
                        }

                        //save all the valid records to session
                        Session["ValidVendorGradeData"] = null;
                        Session["ValidVendorGradeData"] = records.FindAll(x => (x as BaseExcelModel).IsValid);
                        SaveValidVendorGradeData(Session["ValidVendorGradeData"] as List<VendorGradeEntity>);
                    }

                    return Json(new { error = false, totalError = errorCount, uploadStatus = (errorCount == 0), path = fileName });
                }
                catch (System.Reflection.TargetInvocationException ex)
                {
                    //ExceptionManager.LogExceptionDetails(ex);
                    logger.Exception(ex);
                    string msg = ex.Message;
                    if (ex.InnerException != null)
                        msg = ex.InnerException.Message;

                    return Json(new { errorText = msg, error = true, uploadStatus = false });
                }
                catch (Exception ex)
                {
                    logger.Exception(ex);
                    return Json(new { errorText = ex.Message, error = true, uploadStatus = false });
                }
            }

            return Json(new { error = true, uploadStatus = false });
        }

        /// <summary>
        /// Save valid records of vendor grade in bulk
        /// </summary>
        /// <param name="records"></param>
        private void SaveValidVendorGradeData(List<VendorGradeEntity> records)
        {
            _vendorGradeRepository.BulkSaveVendorGrades(records, System.Web.HttpContext.Current.User.Identity.Name);
        }

        /// <summary>
        /// Method to download the uploaded vendor grade excel of errors
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadVendorGradeErrorDataFile(string fileName)
        {
            return File(Server.MapPath(fileName), "excel", "VendorGrade_ErrorFile.xlsx");
        }
    }
}