﻿using FleetPride.BidManager.App_Start;
using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class PriceValidationController : Controller
    {
        IAuthorizationRepository _userInfoRepository;
        ICrossRepository _crossRepository;
        IPriceValidationRepository _priceValidationRepository;
        IBidRepository _bidRepository;
        IExcelHelper _excelHelper;
        IEmailHelper _emmailHelper;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public PriceValidationController(IAuthorizationRepository userInfoRepository, IExcelHelper excelHelper, ICrossRepository crossRepository, IPriceValidationRepository priceValidationRepository, IBidRepository bidRepository, IEmailHelper emmailHelper)
        {
            _userInfoRepository = userInfoRepository;
            _excelHelper = excelHelper;
            _crossRepository = crossRepository;
            _priceValidationRepository = priceValidationRepository;
            _bidRepository = bidRepository;
            _emmailHelper = emmailHelper;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.PriceValidation))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "CrossEntry");
            }
        }

        /// <summary>
        /// get all bids as per role
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllBids()
        {
            var bidsList = _priceValidationRepository.GetAllBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [JsonActionFilter(Parameter = "selectedData", JsonDataType = typeof(List<BidPartEntity>))]
        public JsonResult GetFinalBidParts(int bidId)
        {
            Session["BidId"] = bidId;

            var bidDataList = _priceValidationRepository.GetFinalBidParts(bidId, System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// method to get the final crosses
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <returns></returns>
        public JsonResult GetFinalCrosses(int bidPartId)
        {
            var bidDataList = _priceValidationRepository.GetFinalCrosses(bidPartId);

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Upload Pricing Data
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadPricingData()
        {
            if (Request.Files.Count == 1)
            {
                try
                {
                    string fileName = "~/App_Data/PricingDataUploadFile_" + System.Web.HttpContext.Current.User.Identity.Name + "_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xls";
                    string filePath = Server.MapPath(fileName);
                    Request.Files[0].SaveAs(filePath);

                    BulkUploadPricingDataHelper bulkUpload = new BulkUploadPricingDataHelper(
                        filePath,
                        _priceValidationRepository,
                        System.Web.HttpContext.Current.User.Identity.Name,
                        _userInfoRepository.GetAuthorisedUserRole());
                    int errorCount = bulkUpload.UploadPricingData(Convert.ToInt32(Session["BidId"]));

                    return Json(new
                    {
                        error = false,
                        totalError = errorCount,
                        path = fileName
                    });
                }
                catch (Exception ex)
                {
                    //_logger.Exception(ex);
                    return Json(new { errorText = ex.Message, error = true, uploadStatus = false });
                }
            }

            return Json(new { error = true, uploadStatus = false });
        }

        /// <summary>
        /// Method to download the uploaded Pricing Data File excel
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadPricingDataFile(string fileName)
        {
            return File(Server.MapPath(fileName), "excel", "PricingData_Updated.xlsx");
        }

        /// <summary>
        /// download excel with all bid part, cross part and pricing details
        /// </summary>
        /// <param name="sourceExcelFileName"></param>
        /// <returns></returns>
        public ActionResult DownloadPricingDetails(int bidId, string bidName)
        {
            string fileName = bidName + "_Pricing_Download_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            DataSet dsCross = _priceValidationRepository.ExportFinalCrosses(bidId);
            _excelHelper.ReturnExcel(dsCross, fileName, Response);
            return File(Server.MapPath(fileName), "excel");
        }

        /// <summary>
        /// method to update cross price
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <param name="crossPartId"></param>
        /// <param name="priceToQuote"></param>
        /// <param name="suggestedPrice"></param>
        /// <param name="iCost"></param>
        /// <param name="margin"></param>
        /// <returns></returns>
        public JsonResult UpdateCrossPrice(int bidPartId, int crossPartId, string priceToQuote, string suggestedPrice, string iCost, string margin, string adjustedICost, string adjustedMargin)
        {
            //Logging the inline UpdateCrossPrice values to check the divide by zero issue happening intermittently
            Logger.QuickLog($"PriceValidationController.UpdateCrossPrice -- bidPartId={bidPartId}, crossPartId={crossPartId}, priceToQuote={priceToQuote}, suggestedPrice={suggestedPrice}, iCost={iCost}, margin={margin}, adjustedICost={adjustedICost}, adjustedMargin={adjustedMargin} ");

            var updatedCrossPartId = _priceValidationRepository.UpdateCrossPrice(bidPartId, crossPartId, priceToQuote, suggestedPrice, iCost, margin, adjustedICost, adjustedMargin);

            if (updatedCrossPartId > 0)
            {
                return Json(new { Status = true });
            }
            else
            {
                return Json(new { Status = false });
            }

        }

        public JsonResult CompleteBidStatus(int bidId, string status)
        {
            var result = 1;
            _bidRepository.Complete(bidId, status, "");
            if (status.ToLower() == Constants.Completed.ToLower())
            {
                var bid = _bidRepository.GetBid(bidId);
                var bidCreatorDetails = _bidRepository.GetBidCreatorDetails(bidId);
                string emails = string.Join(",", bidCreatorDetails.Select(x => x.UserEmail));
                _emmailHelper.SendNotificationToNAMOrBidCreator(bid.BidName, emails); //send notification in case of bid completed.
                
            }            
            
            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}