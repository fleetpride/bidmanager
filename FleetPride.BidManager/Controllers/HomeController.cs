﻿using FleetPride.BidManager.App_Start;
using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class HomeController : Controller
    {
        IBidRepository _bidRepository;
        ICrossRepository _crossRepository;
        ILogger _logger;
        IEmailHelper _emmailHelper;
        IAuthorizationRepository _authRepo;
        IExcelRepository _excelRepository;
        IOpportunityRepository _oppRepo;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public HomeController(IBidRepository bidRepository, ICrossRepository crossRepository, IEmailHelper emailHelper, IAuthorizationRepository authRepo, ILogger logger, IExcelRepository excelRepository, IOpportunityRepository oppRepo)
        {
            _bidRepository = bidRepository;
            _crossRepository = crossRepository;
            _logger = logger;
            _emmailHelper = emailHelper;
            _authRepo = authRepo;
            _excelRepository = excelRepository;
            _oppRepo = oppRepo;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_authRepo.HasAccess(Constants.Page.Home))
            {
                ViewBag.Role = _authRepo.GetAuthorisedUserRole();
                return View();
            }
            else if (_authRepo.HasAccess(Constants.Page.CrossValidation))
            {
                ViewBag.Role = _authRepo.GetAuthorisedUserRole();
                return RedirectToAction("Index", Constants.Page.CrossValidation);
            }
            else if (_authRepo.HasAccess(Constants.Page.CrossEntry))
            {
                return RedirectToAction("Index", Constants.Page.CrossEntry);
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// UploadBidParts default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult UploadBidParts()
        {
            if (_authRepo.HasAccess(Constants.Page.UploadBidParts))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);
                    Session["BidName"] = bidName;
                    Session["BidId"] = bidId;
                    ViewBag.Role = _authRepo.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    Response.StatusCode = 500;
                    return Json("SessionEmpty", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        public ActionResult AvailableCrosses()
        {
            if (_authRepo.HasAccess(Constants.Page.AvailableCrosses))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    Session["BidName"] = Convert.ToString(Request.QueryString["BidName"]);
                    Session["BidId"] = bidId;
                    ViewBag.BidStatus = _bidRepository.GetBidStatus(bidId);
                    if (ViewBag.BidStatus.Key == Constants.Created)
                    {
                        ViewBag.BidStatus = Constants.BidStatusEnum.Get(Constants.GeneratingCrosses);
                    }

                    ViewBag.Role = _authRepo.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    return new HttpNotFoundResult();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Method to get all bids
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllBids()
        {
            var bidsList = _bidRepository.GetAllBids(_authRepo.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get all customer info
        /// </summary>
        /// <param name="bidType"></param>
        /// <returns></returns>
        public JsonResult GetAllCustInfo(string bidType, int company, string filter)
        {
            var custInfoResult = new List<Customer>();

            try
            {
                string role = _authRepo.GetAuthorisedUserRole();
                string userName = System.Web.HttpContext.Current.User.Identity.Name;

                //cache key is same for SuperUser or all regional users
                string cacheKey = "customersList" + "_" + bidType + "_" + company;
                if (bidType == Constants.National)
                {
                    //for national account users, add the role as well
                    cacheKey += "_" + role;

                    //if the user is neither a SuperUser or a NamSupervisor (he is a NAM) then add the userId to it as well
                    if (role != UserRole.SuperUser.ToString() && role != UserRole.NAMSupervisor.ToString())
                        cacheKey += "_" + userName;
                }

                Logger.QuickLog($"HomeController.GetAllCustInof -- bidType={bidType}, company={company}, filter={filter}, cacheyKey={cacheKey}");

                var customers = CacheHelper.Get<List<Customer>>(cacheKey);
                if (customers == null)
                {
                    Logger.QuickLog($"HomeController.GetAllCustInof --Fetching data from db-- bidType={bidType}, company={company}, filter={filter}, cacheyKey={cacheKey}, role={role}, userName={userName}");

                    customers = _bidRepository.GetAllCustInfo(bidType, company, role, userName);

                    Logger.QuickLog($"HomeController.GetAllCustInof --Founct {customers.Count} Customers -- bidType={bidType}, company={company}, filter={filter}, cacheyKey={cacheKey}, role={role}, userName={userName}");

                    CacheHelper.Add(cacheKey, customers);
                }

                filter = filter.ToUpper();
                custInfoResult = customers.FindAll((a) => a.CustomerInfo.ToUpper().Contains(filter));
            }
            catch (Exception ex)
            {
                Logger.QuickLog(ex.ToString());
                throw ex;
            }

            return Json(new
            {
                Data = custInfoResult,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            });
        }
        /// <summary>
        /// Exposing this method so that we can clear the cache on demand if needed
        ///
        /// </summary>
        /// <param name="cacheKey"></param>
        public void ClearCache(string cacheKey)
        {
            CacheHelper.Remove(cacheKey);
        }

        /// <summary>
        /// Method to create a Bid
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public JsonResult CreateBid(BidEntity record)
        {
            int bidId = _bidRepository.CreateBid(record, System.Web.HttpContext.Current.User.Identity.Name, _authRepo.GetAuthorisedUserRole());
            if (bidId > 0)
            {
                //try
                //{
                //    OpportunityEntity oppEntity = new OpportunityEntity();
                //    oppEntity.OpportunityAmount = Convert.ToDecimal(record.TotalValueOfBid);
                //    oppEntity.OpportunityName = record.BidName;
                //    oppEntity.OpportunityStage = Constants.OpportunityStage.New;
                //    oppEntity.AccountGroup = record.GroupId;
                //    oppEntity.AccountIseriesCode = record.CustCorpId;
                //    oppEntity.closeDate = Convert.ToDateTime(record.DueDate);
                //    string oppId = _oppRepo.Insert(oppEntity);
                //    _bidRepository.SaveOpportunityID(bidId, oppId);
                //}
                //catch (Exception ex)
                //{
                //    _logger.Exception(ex);
                //    throw new Exception("Bid is saved but opportunity could not be created.");
                //}

                Session["BidId"] = bidId;
                Session["BidName"] = record.BidName;
            }

            return new JsonResult()
            {
                Data = bidId,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        /// <summary>
        /// Method to validate excel upload
        /// </summary>
        /// <returns></returns>
        public JsonResult ValidateExcelUpload()
        {
            int result = this.ValidateExcel(this.HttpContext);

            return Json(new
            {
                Status = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            });
        }

        /// <summary>
        /// Method to check whether original excel file is uploaded
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public int ValidateExcel(HttpContextBase context)
        {
            int fileUploaded = 0;

            if (context.Request.Files.Count != 0 && context.Request.Files[0] != null)
            {
                HttpPostedFileBase file = context.Request.Files[0];
                if (file.ContentLength != 0)
                {
                    fileUploaded = 1;

                    string fileName = string.Empty;
                    if (context.Request.Browser.Browser.Contains("InternetExplorer"))
                        fileName = System.IO.Path.GetFileName(file.FileName);
                    else
                        fileName = file.FileName;
                    string format = fileName.Split('.').Last();

                }
            }
            return fileUploaded;
        }

        /// <summary>
        /// Method to upload excel file over http
        /// </summary>
        /// <returns></returns>
        public JsonResult ExcelUpload(int bidId, string bidName)
        {
            this.SaveExcel(this.HttpContext, bidId, bidName);

            return Json(new
            {
                Status = 1,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            });
        }

        /// <summary>
        /// Method to save excel file in destination folder
        /// </summary>
        /// <param name="context"></param>
        public void SaveExcel(HttpContextBase context, int bidId, string bidName)
        {
            if (context.Request.Files.Count != 0 && context.Request.Files[0] != null)
            {
                HttpPostedFileBase file = context.Request.Files[0];
                if (file.ContentLength != 0)
                {
                    string fileName = string.Empty;
                    if (context.Request.Browser.Browser.Contains("InternetExplorer"))
                        fileName = System.IO.Path.GetFileName(file.FileName);
                    else
                        fileName = file.FileName;

                    string dbFileName = bidName + "__" + fileName;

                    _bidRepository.SaveUploadedExcel(dbFileName, bidId);

                    string localExcelFolder = Server.MapPath(ConfigurationManager.AppSettings["LocalUploadExcelPath"]);
                    _excelRepository.SaveExcel(file, dbFileName, localExcelFolder);
                }
            }
        }

        /// <summary>
        /// Method to download source excel file
        /// </summary>
        /// <returns></returns>
        public ActionResult DownloadSourceExcel(string sourceExcelFileName)
        {
            string localExcelFolder = Server.MapPath(ConfigurationManager.AppSettings["LocalUploadExcelPath"]);
            string filePath = _excelRepository.GetExcel(sourceExcelFileName, localExcelFolder);
            return File(filePath, "excel", sourceExcelFileName);
        }

        /// <summary>
        /// Method to update rank of a bid
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="oldRank"></param>
        /// <param name="newRank"></param>
        /// <returns></returns>
        public JsonResult UpdateBid(string bidId, string oldRank, string newRank, string dueDate, string phaseDueDate, string totalValueOfBid, string projectedGPDollar, string projectedGPPerc)
        {
            int updateResult = _bidRepository.UpdateBid(bidId, oldRank, newRank, dueDate, phaseDueDate, totalValueOfBid, projectedGPDollar, projectedGPPerc, System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = updateResult,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        /// <summary>
        /// Upload bid parts
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadBidPartData(int bidId)
        {
            if (Request.Files.Count == 1)
            {
                try
                {
                    string fileName = "~/App_Data/BidPartsUploadFile_" + System.Web.HttpContext.Current.User.Identity.Name + "_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xls";
                    string filePath = Server.MapPath(fileName);
                    Request.Files[0].SaveAs(filePath);

                    BulkUploadHelper bulkUpload = new BulkUploadHelper(
                        filePath,
                        _bidRepository,
                        System.Web.HttpContext.Current.User.Identity.Name,
                        _authRepo.GetAuthorisedUserRole());
                    int errorCount = bulkUpload.Upload(Convert.ToInt32(Session["BidId"]));

                    return Json(new
                    {
                        error = false,
                        totalError = errorCount,
                        path = fileName
                    });
                }
                catch (Exception ex)
                {
                    _logger.Exception(ex);
                    return Json(new { errorText = ex.Message, error = true, uploadStatus = false });
                }
            }

            return Json(new { error = true, uploadStatus = false });
        }

        /// <summary>
        /// Method to get all the parts in the bid
        /// </summary>
        /// <returns></returns>
        [JsonActionFilter(Parameter = "selectedData", JsonDataType = typeof(List<BidPartEntity>))]
        public JsonResult GetBidPartsList()
        {
            if (Session["BidId"] != null)
            {
                var bidDataList = _bidRepository.GetBidPartsList(Convert.ToInt32(Session["BidId"]), System.Web.HttpContext.Current.User.Identity.Name, _authRepo.GetAuthorisedUserRole());

                return new JsonResult()
                {
                    Data = bidDataList,
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                Response.StatusCode = 500;
                return Json("SessionEmpty", JsonRequestBehavior.AllowGet);
            }
        }

        [JsonActionFilter(Parameter = "selectedData", JsonDataType = typeof(List<BidPartEntity>))]
        public JsonResult GetBidParts(int bidId)
        {
            var bidDataList = _bidRepository.GetBidPartsList(bidId, System.Web.HttpContext.Current.User.Identity.Name, _authRepo.GetAuthorisedUserRole());

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetCrosses(int bidPartId)
        {
            var bidDataList = _crossRepository.GetCrosses(bidPartId, false);

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetFinalCrosses(int bidId)
        {
            var bidDataList = _crossRepository.GetFinalCrosses(bidId);

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to insert part in the bid
        /// </summary>
        /// <param name="partNumber"></param>
        /// <param name="description"></param>
        /// <param name="brand"></param>
        /// <param name="displayPart"></param>
        /// <param name="note"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public JsonResult InsertBidPart(string customerPartNumber, string partDescription, string manufacturer, string manufacturerPartNumber, string note, int? estimatedAnnualUsage)
        {

            if (Session["BidId"] != null)
            {
                int insertedBidPartId = _bidRepository.InsertBidPart(Convert.ToInt32(Session["BidId"]), customerPartNumber != null ? customerPartNumber.ToUpper() : customerPartNumber, partDescription != null ? partDescription.ToUpper() : partDescription, manufacturer != null ? manufacturer.ToUpper() : manufacturer, manufacturerPartNumber != null ? manufacturerPartNumber.ToUpper() : manufacturerPartNumber, note != null ? note.ToUpper() : note, estimatedAnnualUsage, System.Web.HttpContext.Current.User.Identity.Name);

                if (insertedBidPartId > 0)
                {
                    return Json(new { Status = true, BidPartId = insertedBidPartId });
                }
                else
                {
                    return Json(new { Status = false, BidPartId = insertedBidPartId });
                }
            }
            else
            {
                Response.StatusCode = 500;
                return Json("SessionEmpty", JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Method to update part in the bid
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <param name="partNumber"></param>
        /// <param name="description"></param>
        /// <param name="brand"></param>
        /// <param name="displayPart"></param>
        /// <param name="note"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public JsonResult UpdateBidPart(int bidPartId, string customerPartNumber, string partDescription, string manufacturer, string manufacturerPartNumber, string note, int? estimatedAnnualUsage)
        {

            if (Session["BidId"] != null)
            {
                var updatedBidPartId = _bidRepository.UpdateBidPart(Convert.ToInt32(bidPartId), customerPartNumber.ToUpper(), partDescription != null ? partDescription.ToUpper() : partDescription, manufacturer != null ? manufacturer.ToUpper() : manufacturerPartNumber, manufacturerPartNumber != null ? manufacturerPartNumber.ToUpper() : manufacturerPartNumber, note != null ? note.ToUpper() : note, estimatedAnnualUsage, System.Web.HttpContext.Current.User.Identity.Name);

                if (updatedBidPartId > 0)
                {
                    return Json(new { Status = true, BidPartId = updatedBidPartId });
                }
                else
                {
                    return Json(new { Status = false, BidPartId = updatedBidPartId });
                }
            }
            else
            {
                Response.StatusCode = 500;
                return Json("SessionEmpty", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Method to delete part in the bid
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <returns></returns>
        public JsonResult DeleteBidPart(string bidPartId)
        {
            var deletedBidPartId = _bidRepository.DeleteBidPart(Convert.ToInt32(bidPartId));
            return new JsonResult()
            {
                Data = deletedBidPartId,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to download Template
        /// </summary>
        /// <returns></returns>
        public ActionResult DownloadTemplateFile()
        {
            return File(Server.MapPath("~/Views/Home/UploadBidParts_Template.xlsx"), "excel", "UploadBidParts_Template.xlsx");
        }

        /// <summary>
        /// Method to download the uploaded excel
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadBidDataFile(string fileName)
        {
            return File(Server.MapPath(fileName), "excel", "BidParts_Updated.xlsx");
        }

        /// <summary>
        /// Method to generate crosses for the bid parts
        /// </summary>
        /// <returns></returns>
        public JsonResult GenerateCrossesForBidParts(int BidId)
        {
            HostingEnvironment.QueueBackgroundWorkItem(ct => GenerateCrossesForBidParts(BidId, "system"));
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Submit(List<BidPartEntity> selectedRows)
        {
            if (selectedRows != null && selectedRows.Count > 0)
            {
                _bidRepository.SubmitForCrowdSourcing(selectedRows);
                _bidRepository.Submit(Convert.ToInt32(Session["BidId"]), Constants.CrowdSourcing, "");
            }
            else
            {
                _bidRepository.Submit(Convert.ToInt32(Session["BidId"]), Constants.CrossFinalization, "");
            }

            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// To mark the bid invalid and to pull the bid from inventory update to won.
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="oldStatus"></param>
        /// <param name="newStatus"></param>
        /// <returns></returns>
        public JsonResult AdminBidStatusUpdate(int bidId, string oldStatus, string newStatus)
        {
            int result = 0;
            if (bidId > 0 && !string.IsNullOrWhiteSpace(newStatus) && !string.IsNullOrWhiteSpace(oldStatus))
            {
                if (newStatus.ToLower() == "invalid")
                    result = _bidRepository.AdminBidStatusUpdate(bidId, oldStatus, newStatus);
                if ((oldStatus.ToLower() == "inventoryupdate" || oldStatus.ToLower() == "inventory update") && newStatus.ToLower() == "won")
                    result = _bidRepository.AdminBidStatusUpdate(bidId, oldStatus.Replace(" ",""), newStatus);
            }
            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        private void GenerateCrossesForBidParts(int bidId, string userName)
        {
            try
            {
                _logger.Message("Starting cross generation for bidId: " + bidId);
                _bidRepository.GenerateCrossesForBidParts(bidId, userName);
                _logger.Message("Finished cross generation for bidId: " + bidId);

                BidEntity bid = _bidRepository.GetBid(bidId);
                if (bid != null)
                {
                    if (bid.BidStatus == Constants.CrossesGenerated)
                    {
                        if (bid.BidType == Constants.National)
                        {
                            _logger.Message("Automated, National Bid: " + bidId + ", created by: " + bid.CreatedBy);
                            //auto submit the bid for National accounts
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["EnableCrowdSourcingForNATAccs"]))
                                _bidRepository.Submit(bidId, Constants.CrossFinalization, "Auto submitted");
                        }
                        else if (bid.BidType == Constants.Regional)
                        {
                            _logger.Message("Automated, Regional Bid: " + bidId + ", created by: " + bid.CreatedBy);
                            //auto submit the bid for Reg accounts
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["EnableCrowdSourcingForRegAccs"]))
                                _bidRepository.Submit(bidId, Constants.CrossFinalization, "Auto submitted");
                        }
                        else
                        {
                            _logger.Message("Local Bid: " + bidId + ", created by: " + bid.CreatedBy);
                        }

                        _emmailHelper.SendNotificationAfterBidCreated(bid);
                    }
                    else
                    {
                        _logger.Message("Cross Generation did not finish properly. BidId: " + bidId);
                    }
                }
                else
                {
                    _logger.Message("Bid not found. BidId: " + bidId);
                }
            }
            catch (Exception exp)
            {
                _logger.Exception(exp);
            }
        }
    }
}