﻿using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class CompletedBidsController : Controller
    {
        IAuthorizationRepository _userInfoRepository;
        ICompletedBidsRepository _completedBidsRepository;
        IExcelHelper _excelHelper;
        IEmailHelper _emailHelper;
        IBidRepository _bidRepository;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public CompletedBidsController(IAuthorizationRepository userInfoRepository, ICompletedBidsRepository completedBidsRepository, IBidRepository bidRepository, IExcelHelper excelHelper, IEmailHelper emailHelper)
        {
            _userInfoRepository = userInfoRepository;
            _completedBidsRepository = completedBidsRepository;
            _excelHelper = excelHelper;
            _emailHelper = emailHelper;
            _bidRepository = bidRepository;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.CompletedBids))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// get all bids as per role
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllBids()
        {
            var bidsList = _completedBidsRepository.GetAllBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to mark won cross part
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="bidPartId"></param>
        /// <param name="crossPartId"></param>
        /// <returns></returns>
        public JsonResult MarkWonSelectedCross(int bidId, int bidPartId, int crossPartId)
        {
            var updatedCrossId = _completedBidsRepository.MarkWonSelectedCross(bidId, bidPartId, crossPartId);
            return new JsonResult()
            {
                Data = updatedCrossId,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Mark all primary parts as won
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public JsonResult MarkAllPrimaryWon(int bidId)
        {
            var result = 1;
           _completedBidsRepository.MarkAllPrimaryWon(bidId);
            var bid = _bidRepository.GetBid(bidId);
            var bidCreatorDetails = _bidRepository.GetBidCreatorDetails(bidId);
            string emails = string.Join(",", bidCreatorDetails.Select(x => x.UserEmail));

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Upload Pricing Data
        /// </summary>
        /// <returns></returns>
        public JsonResult UploadPartsMarkWonData()
        {
            if (Request.Files.Count == 1)
            {
                try
                {
                    string fileName = "~/App_Data/PartsMarkWonDataUploadFile_" + System.Web.HttpContext.Current.User.Identity.Name + "_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xls";
                    string filePath = Server.MapPath(fileName);
                    Request.Files[0].SaveAs(filePath);

                    BulkUploadPartMarkWonHelper bulkUpload = new BulkUploadPartMarkWonHelper(
                        filePath,
                        _completedBidsRepository,
                        System.Web.HttpContext.Current.User.Identity.Name,
                        _userInfoRepository.GetAuthorisedUserRole());
                    int errorCount = bulkUpload.UploadPartMarkWonData(Convert.ToInt32(Session["BidId"]));

                    return Json(new
                    {
                        error = false,
                        totalError = errorCount,
                        path = fileName
                    });
                }
                catch (Exception ex)
                {
                    return Json(new { errorText = ex.Message, error = true, uploadStatus = false });
                }
            }

            return Json(new { error = true, uploadStatus = false });
        }

        /// <summary>
        /// Method to download the uploaded Parts Mark Won File excel
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadPartsMarkWonDataFile(string fileName)
        {
            return File(Server.MapPath(fileName), "excel", "CompletedBid_PartsMarkWon_Updated.xlsx");
        }

        /// <summary>
        /// download excel with all bid part, cross part and pricing details
        /// </summary>
        /// <param name="sourceExcelFileName"></param>
        /// <returns></returns>
        public ActionResult DownloadCompletedBidPartsMarkWonDetails(int bidId, string bidName)
        {
            string fileName = bidName + "_CompletedBid_PartsMarkWon_Download_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            DataSet dsCross = _completedBidsRepository.ExportFinalCrosses(bidId);
            _excelHelper.ReturnExcel(dsCross, fileName, Response);
            return File(Server.MapPath(fileName), "excel");
        }

        public JsonResult NotifySupplyChain(int bidId, string bidName, string comment)
        {
            var result = 1;
            string fileName = bidName + "_Supply_Chain_Data_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            var ds = _completedBidsRepository.GetSupplyChainData(bidId, System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());
            string path = Server.MapPath("~/App_Data/" + fileName);
            _excelHelper.SaveExcel(ds, path, fileName, Response);
            _emailHelper.SendSupplyChainNotification(bidId, bidName, comment, path);

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

       

    }
}