﻿using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class CrossValidationController : Controller
    {
        IAuthorizationRepository _authRepo;
        ICrowdSourcingRepository _crowdSourcingRepo;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public CrossValidationController(IAuthorizationRepository authRepo, ICrowdSourcingRepository crowdSourcingRepo)
        {
            _authRepo = authRepo;
            _crowdSourcingRepo = crowdSourcingRepo;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_authRepo.HasAccess(Constants.Page.CrossValidation))
            {
                Session["BidName"] = null;
                Session["BidId"] = null;
                if (_authRepo.HasAccess(Constants.Page.CrossValidation))
                {
                    ViewBag.Role = _authRepo.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "CrossEntry");
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// Method to get the count/statistics for cross validation User
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCrossValidatorCounts()
        {
            var entryCount = _crowdSourcingRepo.GetCrossValidatorCounts(System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = entryCount,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get the Check Locked Validation Part For User
        /// </summary>
        /// <returns></returns>
        public JsonResult CheckLockedValidationPartForUser()
        {
            var result = _crowdSourcingRepo.CheckLockedValidationPartForUser(System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get the part for the user for validiation
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPartForValidation()
        {
            var crossValidationPart = _crowdSourcingRepo.GetPartForValidation(System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = crossValidationPart,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to load the crosses for the part validation
        /// </summary>
        /// <param name="crossValidationPartId"></param>
        /// <returns></returns>
        public JsonResult GetCrossesForValidation(string crossValidationPartId)
        {
            var crossesList = _crowdSourcingRepo.GetCrossesForValidation(Convert.ToInt32(crossValidationPartId), System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = crossesList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to approve/reject a cross of a part
        /// </summary>
        /// <param name="CrossPartId"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public JsonResult UpdateCrossValidationStatus(int crossPartId, string status)
        {
            int result = _crowdSourcingRepo.UpdateCrossValidationStatus(crossPartId, status, System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        /// <summary>
        /// Method to submit the part from validator
        /// </summary>
        /// <param name="ValidatedCrossPartId"></param>
        /// <returns></returns>
        public JsonResult SubmitValidatedPart(string validatedCrossPartId)
        {
            _crowdSourcingRepo.SubmitValidatedPart(Convert.ToInt32(validatedCrossPartId), System.Web.HttpContext.Current.User.Identity.Name);
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}