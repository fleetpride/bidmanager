﻿using FleetPride.BidManager.App_Start;
using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using FleetPride.Excel;
using NLog;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class InventoryController : Controller
    {
        protected int errorCount = 0;
        protected Utilities.Logger logger = new Utilities.Logger();

        IAuthorizationRepository _userInfoRepository;
        IInventoryRepository _inventoryRepository;
        IExcelHelper _excelHelper;
        IExcelRepository _excelRepository;
        IBidRepository _bidRepository;
        IEmailHelper _emailHelper;

        /// <summary>
        /// create a Location Key to store the locations in the cache
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        private string LocationsKey(int bidId)
        {
            return "LK" + System.Web.HttpContext.Current.User.Identity.Name + bidId;
        }

        /// <summary>
        /// create a unique key to store the progress status in the cache
        /// </summary>
        /// <param name="bidid"></param>
        /// <returns></returns>
        private string ProgressStatuskey(int bidid)
        {
            return "PS" + System.Web.HttpContext.Current.User.Identity.Name + bidid;
        }

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public InventoryController(IAuthorizationRepository userInfoRepository, IInventoryRepository inventoryRepository, IBidRepository bidRepository, IExcelHelper excelHelper, IExcelRepository excelRepository, IEmailHelper emailHelper)
        {
            _userInfoRepository = userInfoRepository;
            _inventoryRepository = inventoryRepository;
            _excelHelper = excelHelper;
            _excelRepository = excelRepository;
            _bidRepository = bidRepository;
            _emailHelper = emailHelper;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.InventoryUpdate) || _userInfoRepository.HasAccess(Constants.Page.InventoryDetails))
            {
                ViewBag.ReadOnly = !_userInfoRepository.HasAccess(Constants.Page.InventoryUpdate) && _userInfoRepository.HasAccess(Constants.Page.InventoryDetails);
                ViewBag.HasAccessToVerification = _userInfoRepository.HasAccess(Constants.Page.InventoryVerification);
                ViewBag.IsUploading = false;

                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);
                    ViewBag.InventoryReviewURL = GetUrl(Constants.PageUrls.InventoryVerificationUrl, bidId, bidName);
                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    //ViewBag.IsUploading = (HttpContext.Cache[ProgressStatuskey(bidId)] != null);
                }

                ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                return View();
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        public string GetUrl(string pageName, int bidId, string bidName)
        {
            return string.Format("{0}/{1}?BidId={2}&BidName={3}", ConfigurationManager.AppSettings["BMUrl"], pageName, bidId, Uri.EscapeUriString(bidName));
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult InventoryVerification()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.InventoryVerification))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }



        public JsonResult GetVerificationBids()
        {
            var bidsList = _inventoryRepository.GetVerificationBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// get all bids as per role
        /// </summary>
        /// <returns></returns>
        public JsonResult GetInventoryUpdateBids()
        {
            var bidsList = _inventoryRepository.GetInventoryUpdateBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetDataTeamBids()
        {
            var bidsList = _inventoryRepository.GetDataTeamBids();

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCrossPartLineReview(int bidId)
        {
            var crossPartLineReviewItems = _inventoryRepository.GetCrossPartLineReview(bidId, _userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = crossPartLineReviewItems,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetPartsNotSetup(int bidId)
        {
            var partsNotSetup = _inventoryRepository.GetPartsNotSetup(bidId);

            return new JsonResult()
            {
                Data = partsNotSetup,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SaveInventoryApproval(List<int> selectedIDs)
        {
            var rowsUpdated = _inventoryRepository.SaveInventoryApproval(true, selectedIDs, _userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = rowsUpdated,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult UpdateCrossPartInventoryReview(int ID, int revisedPool, string revisedPartNumber, string comments)
        {
            var rowsUpdated = _inventoryRepository.UpdateCrossPartInventoryReview(ID, revisedPool, revisedPartNumber, comments, System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());

            return new JsonResult()
            {
                Data = rowsUpdated,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SendToDemandTeam(int bidId, string bidName)
        {
            string userRole = _userInfoRepository.GetAuthorisedUserRole();
            var nextStatus = _inventoryRepository.SendToDemandTeam(bidId, userRole, System.Web.HttpContext.Current.User.Identity.Name);

            _emailHelper.SendNotificationToDemandTeam(bidName, bidId);

            return new JsonResult()
            {
                Data = nextStatus,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SubmitInventoryReview(int bidId, string bidName)
        {
            string userRole = _userInfoRepository.GetAuthorisedUserRole();
            var nextStatus = _inventoryRepository.SubmitInventoryReview(bidId, userRole, System.Web.HttpContext.Current.User.Identity.Name);

            //Send notifications to the next user and set the message
            string message = "Your review has been submitted for the bid.";
            switch (nextStatus)
            {
                case Constants.LineReview_Dir:
                    _emailHelper.SendNotificationToCategoryDirector(bidName, bidId);
                    message = "Bid has been assigned to the Category Director for review.";
                    break;
                case Constants.LineReview_VP:
                    _emailHelper.SendNotificationToCategoryVP(bidName, bidId);
                    message = "Bid has been assigned to the Category Vice President for review.";
                    break;
                case Constants.DataTeam:
                    _emailHelper.SendNotificationToDataTeam(bidName, bidId);
                    message = "Bid has been assigned to the Data Team as there are some parts that need to be setup/stocked.";
                    break;
                case Constants.DemandTeam:
                    _emailHelper.SendNotificationToDemandTeam(bidName, bidId);
                    message = "Bid has been assigned to the Demand Team.";
                    break;
            }

            var bid = _bidRepository.GetBid(bidId);
            //if the bid has any revised parts notify the NAM Supervisro.
            if (_inventoryRepository.HasRevisedParts(bidId))
            {
                _emailHelper.SendRevisedPartsNotification(bidName, bidId, bid.BidType);
            }

            return new JsonResult()
            {
                Data = message,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SaveInventoryRejection(List<int> selectedIDs)
        {
            var rowsUpdated = _inventoryRepository.SaveInventoryApproval(false, selectedIDs, _userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = rowsUpdated,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetLineReviewAction(int bidId)
        {
            bool actionTaken = _inventoryRepository.GetLineReviewAction(bidId, _userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = actionTaken,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get cross part inventory
        /// </summary>
        /// <returns></returns>
        //[JsonActionFilter(Parameter = "selectedData", JsonDataType = typeof(List<CrossPartInventoryEntity>))]
        public JsonResult GetCrossPartsInventoryList(int bidId)
        {
            var crossPartsInventoryList = _inventoryRepository.GetCrossPartsInventoryList(bidId, System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());
            ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();

            return new JsonResult()
            {
                Data = crossPartsInventoryList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetInventoryUploadProgress(int bidId)
        {
            if (HttpContext.Cache[ProgressStatuskey(bidId)] != null)
                return HttpContext.Cache[ProgressStatuskey(bidId)] as JsonResult;

            return Json(new { progressStatus = 0, error = true, errorText = "Progress status not found." });
        }

        /// <summary>
        /// Method to download Template
        /// </summary>
        /// <returns></returns>
        public ActionResult DownloadTemplateFile()
        {
            return File(Server.MapPath("~/Views/Inventory/InventoryUpdate_PartsWon_Template.xlsx"), "excel", "UploadBidParts_Template.xlsx");
        }

        public JsonResult UploadCrossPartsInventoryFile(int bidId)
        {
            string fileName = "~/App_Data/InventoryUpdate_PartsWon_LocationWise_UploadFile_" + System.Web.HttpContext.Current.User.Identity.Name + "_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xls";
            Request.Files[0].SaveAs(Server.MapPath(fileName));
            string statusKey = ProgressStatuskey(bidId);
            int errorCount = 0;
            var status = Json(new { progressStatus = 0, error = false, totalError = errorCount, path = fileName });
            HttpContext.Cache[statusKey] = status;

            if (Request.Files.Count == 1)
            {
                try
                {
                    //save the uploaded file to the App_Data folder
                    using (var package = new ExcelPackage(new System.IO.FileInfo(Server.MapPath(fileName))))
                    {
                        //read the data from the excel sheet
                        var workSheet = package.Workbook.Worksheets.First();
                        List<CrossPartInventoryEntity> records = workSheet.GetRecords<CrossPartInventoryEntity>();

                        //execute the data annotation based validations
                        GenericValidator.TryValidate(records);

                        string userName = System.Web.HttpContext.Current.User.Identity.Name;
                        decimal i = 0;
                        var parallelOps = new ParallelOptions();

                        //commented the hard coded value of 4, now getting it from web.config
                        //parallelOps.MaxDegreeOfParallelism = 4;
                        parallelOps.MaxDegreeOfParallelism = Convert.ToInt32(ConfigurationManager.AppSettings["MaxDegreeOfParallelism"]);
                        Object obj = new object();
                        List<CrossPartInventoryEntity> savedRecords = new List<CrossPartInventoryEntity>();
                        Parallel.ForEach(records, parallelOps, record =>
                        {
                            CustomValidate(record, bidId);

                            //check for duplicate rows
                            //check for duplicate rows
                            if (savedRecords.Find(r => r.CrossPoolNumber == record.CrossPoolNumber
                                && r.CrossPartNumber.ToLower() == record.CrossPartNumber.ToLower()
                                && r.Location.ToLower() == record.Location.ToLower()) == null)
                            {
                                savedRecords.Add(record);
                                if (record.IsValid)
                                {
                                    int result = _inventoryRepository.UpdateCrossPartInventory(record, bidId, userName);
                                    if (result == -1)
                                    {
                                        record.ErrorMessage += "Location is not valid; ";
                                        errorCount++;
                                    }
                                    else if (result == -2)
                                    {
                                        record.ErrorMessage += "Loction already added with the part; ";
                                        errorCount++;
                                    }
                                    else if (result == 0)
                                    {
                                        record.ErrorMessage += "An error occured while saving the record; ";
                                        errorCount++;
                                    }
                                }
                                else
                                {
                                    record.ErrorMessage += "An error occured while saving the record; ";
                                    errorCount++;
                                }
                            }
                            else
                            {
                                record.ErrorMessage += "Loction already added with the part; ";
                                errorCount++;
                            }

                            //update the progress status
                            lock (obj)
                            {
                                i = i + 1;
                                int progressStatus = (int)Math.Ceiling((i / records.Count) * 100);
                                status = Json(new { progressStatus = progressStatus, error = false, totalError = errorCount, path = fileName });
                                HttpContext.Cache[statusKey] = status;
                            }
                        });

                        //foreach (var record in records)
                        //{
                        //    //valid the record, save if valid else record in the error count
                        //    CustomValidate(record, bidId);
                        //    if (record.IsValid)
                        //        _inventoryRepository.UpdateCrossPartInventory(record, bidId, System.Web.HttpContext.Current.User.Identity.Name);
                        //    else
                        //        errorCount++;

                        //    //update the progress status
                        //    i = i + 1;
                        //    int progressStatus = (int)Math.Ceiling((i / records.Count) * 100);
                        //    status = Json(new { progressStatus = progressStatus, error = false, totalError = errorCount, path = fileName });
                        //    HttpContext.Cache[statusKey] = status;
                        //}

                        //if there are some failed records then create a sheet in the original file with the status and error messages.
                        if (errorCount > 0)
                        {
                            ExcelWorksheet resultSheet = package.Workbook.Worksheets.AddOrReplace("Upload Result");
                            resultSheet.Cells["A1"].LoadFromCollection<CrossPartInventoryEntity>(records, true);
                            package.Save();
                        }
                    }

                    //update the progress status
                    status = Json(new { progressStatus = 100, error = false, totalError = errorCount, path = fileName });
                    HttpContext.Cache[statusKey] = status;
                }
                catch (System.Reflection.TargetInvocationException ex)
                {
                    logger.Exception(ex);
                    string msg = ex.Message;
                    if (ex.InnerException != null)
                        msg = ex.InnerException.Message;

                    status = Json(new { errorText = msg, error = true });
                }
                catch (Exception ex)
                {
                    logger.Exception(ex);
                    status = Json(new { errorText = ex.Message, error = true });
                }
            }

            HttpContext.Cache.Remove(statusKey);
            return status;
        }

        private bool CustomValidate(CrossPartInventoryEntity imodel, int bidId)
        {
            //if (!_inventoryRepository.ValidateLocation(imodel.Location.ToUpper(), bidId))
            //{
            //    imodel.IsValid = false;
            //    imodel.ErrorMessage += "Location is not valid; ";
            //}

            imodel.EstimatedAnnualUsage = Math.Round(imodel.EstimatedAnnualUsage, 0, MidpointRounding.AwayFromZero);
            if (imodel.EstimatedAnnualUsage <= 0)
            {
                imodel.IsValid = false;
                imodel.ErrorMessage += "Invalid Estimated Annual Usage, should be between 1 to 99999999; ";
            }

            return imodel.IsValid;
        }

        public JsonResult DeleteCrossPartsInventory(List<int> crossPartInventoryIds)
        {
            _inventoryRepository.DeleteCrossPartsInventory(crossPartInventoryIds);

            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult UpdateCrossPartInventory(CrossPartInventoryEntity crossPartInventoryEntity, string bidId)
        {
            //validate location
            //bool isLocationValid = _inventoryRepository.ValidateLocation(crossPartInventoryEntity.CrossPartInventoryId, crossPartInventoryEntity.Location.ToUpper(), Convert.ToInt32(bidId));
            //if (isLocationValid)
            //{
            var updatedCrossPartInventoryId = _inventoryRepository.UpdateCrossPartInventory(crossPartInventoryEntity, Convert.ToInt32(bidId), System.Web.HttpContext.Current.User.Identity.Name);

            return Json(new
            {
                IsLocationInValid = (updatedCrossPartInventoryId == -1),
                IsLocationDuplicate = (updatedCrossPartInventoryId == -2),
                CrossPartInventoryId = updatedCrossPartInventoryId
            });
            //}
            //else
            //{
            //    return Json(new { IsLocationValid = isLocationValid });
            //}
        }

        public JsonResult GetCMList(int bidId)
        {
            var cmList = _inventoryRepository.GetCMList(bidId);

            return new JsonResult()
            {
                Data = cmList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetLocationsList(int bidId)
        {
            var locations = _inventoryRepository.GetLocationsList(bidId);

            return new JsonResult()
            {
                Data = locations,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SendEmailToCM(List<CMUserEntity> dataToPost, string bidName, int bidId, string bidLaunchDate)
        {
            HostingEnvironment.QueueBackgroundWorkItem(ct => LoadInventoryReview(dataToPost, bidName, bidId, bidLaunchDate));
            return new JsonResult()
            {
                Data = 1,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public void LoadInventoryReview(List<CMUserEntity> dataToPost, string bidName, int bidId, string bidLaunchDate)
        {
            _bidRepository.Complete(bidId, Constants.LineReview_CM, "");
            _inventoryRepository.AddCMAction(dataToPost, bidId, bidLaunchDate);
            _emailHelper.SendNotificationToCM(bidName, bidId, dataToPost);
        }

        public JsonResult PullbackBidFromInventoryReview(int bidId)
        {
            _inventoryRepository.PullbackBidFromInventoryReview(bidId);
            return new JsonResult()
            {
                Data = true,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }



        /// <summary>
        /// Method to download the uploaded inventory File excel
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ActionResult DownloadCrossPartsInventoryDataFile(string fileName)
        {
            return File(Server.MapPath(fileName), "excel", "InventoryUpdate_PartsWon_Updated.xlsx");
        }

        /// <summary>
        /// download excel with all Cross Parts Inventory
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="bidName"></param>
        /// <returns></returns>
        public ActionResult DownloadCrossPartsInventoryDetails(int bidId, string bidName)
        {
            string fileName = bidName + "_InventoryUpdate_PartsWon_Download_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            DataSet dsCross = _inventoryRepository.ExportCrossPartsInventory(bidId);
            _excelHelper.ReturnExcel(dsCross, fileName, Response);
            return File(Server.MapPath(fileName), "excel");
        }

        /// <summary>
        /// Set all locations in session
        /// </summary>
        /// <param name="locations"></param>
        /// <param name="bidId"></param>
        /// <param name="bidName"></param>
        /// <returns></returns>
        public JsonResult DownloadLocationInventory(List<string> locations, int bidId, string bidName)
        {
            HttpContext.Cache.Insert(this.LocationsKey(bidId), locations);
            return Json(new { Data = true });
        }

        /// <summary>
        /// download invetory excel file for selected locations 
        /// </summary>
        /// <param name="bidId"></param>
        /// <param name="bidName"></param>
        /// <returns></returns>
        public ActionResult DownloadLocationInventoryExcel(int bidId, string bidName)
        {
            string fileName = bidName + "_LocationsInventory_Download_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            DataSet dsCross = _inventoryRepository.ExportLocationInventory(HttpContext.Cache[this.LocationsKey(bidId)] as List<string>, bidId);
            HttpContext.Cache.Remove(this.LocationsKey(bidId));
            _excelHelper.ReturnExcel(dsCross, fileName, Response);
            return File(Server.MapPath(fileName), "excel");
        }
    }
}