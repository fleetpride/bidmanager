﻿using FleetPride.BidManager.App_Start;
using FleetPride.BidManager.Models;
using FleetPride.BidManager.Repository;
using FleetPride.BidManager.Utilities;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FleetPride.BidManager.Controllers
{
    public class CrossReportController : Controller
    {
        IAuthorizationRepository _userInfoRepository;
        IBidRepository _bidRepository;
        ICrossRepository _crossRepository;
        IEmailHelper _emmailHelper;

        /// <summary>  
        ///  Class Default constructor for instantiating the Repository Interfaces.
        /// </summary>
        public CrossReportController(IAuthorizationRepository userInfoRepository, IBidRepository bidRepository, ICrossRepository crossRepository, IEmailHelper emailHelper)
        {
            _userInfoRepository = userInfoRepository;
            _bidRepository = bidRepository;
            _crossRepository = crossRepository;
            _emmailHelper = emailHelper;
        }

        /// <summary>
        /// Index default action method
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (_userInfoRepository.HasAccess(Constants.Page.CrossReport))
            {
                if (Request.QueryString["BidId"] != null && Request.QueryString["BidName"] != null)
                {
                    int bidId = Convert.ToInt32(Request.QueryString["BidId"]);
                    string bidName = Convert.ToString(Request.QueryString["BidName"]);

                    ViewBag.BidName = bidName;
                    ViewBag.BidId = bidId;
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
                else
                {
                    ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();
                    return View();
                }
            }
            else
            {
                return new HttpUnauthorizedResult();
            }
        }

        /// <summary>
        /// get all bids as per role
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllBids()
        {
            var bidsList = _crossRepository.GetAllBids(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllBidsForDropdown(int bidId = 0)
        {
            var bidsList = _crossRepository.GetAllBidsForDropDown(_userInfoRepository.GetAuthorisedUserRole(), System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = bidsList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get bid parts list
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBidPartsList(int bidId)
        {
            var bidDataList = _bidRepository.GetBidPartsList(bidId, System.Web.HttpContext.Current.User.Identity.Name, _userInfoRepository.GetAuthorisedUserRole());
            ViewBag.Role = _userInfoRepository.GetAuthorisedUserRole();

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult UpdateBidStatus(int bidId, string bidName, string status)
        {
            var result = 1;
            _bidRepository.Submit(bidId, status, "");

            if (status == Constants.PricingTeam)
            {
                _emmailHelper.SendNotificationToPricing(bidName, bidId);
            }

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult UpdateCMAction(int bidId, string bidName)
        {
            int result =  _crossRepository.UpdateCMAction(bidId, System.Web.HttpContext.Current.User.Identity.Name);

            if (result == 2)
            {
                _bidRepository.Submit(bidId, Constants.PricingTeam, "");
                _emmailHelper.SendNotificationToPricing(bidName, bidId);
            }

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetCMActionStatusForBid(int bidId)
        {
            int result = _crossRepository.GetCMActionStatusForBid(bidId, System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetCrosses(int bidPartId)
        {
            var crossList = _crossRepository.GetCrosses(bidPartId, true);

            return new JsonResult()
            {
                Data = crossList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllCrosses(int bidId)
        {
            var bidDataList = _crossRepository.GetAllCrosses(bidId);

            return new JsonResult()
            {
                Data = bidDataList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetCMList(int bidId)
        {
            var cmList = _crossRepository.GetCMList(bidId);

            return new JsonResult()
            {
                Data = cmList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult FinalizeSelectedCross(List<FinalCrossEntity> finalizeCrosses, int bidId)
        {
            var updatedBidPartId = _crossRepository.FinalizeSelectedCross(_userInfoRepository.GetAuthorisedUserRole().ToString(),finalizeCrosses, bidId);
            return new JsonResult()
            {
                Data = updatedBidPartId,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SaveAll(List<CrosReportUpdates> updates)
        {
            _crossRepository.SaveAll(_userInfoRepository.GetAuthorisedUserRole().ToString(), updates);
            return new JsonResult()
            {
                Data = "NA",
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult DeclineSelectedCross(int bidPartId)
        {
            var updatedBidPartId = _crossRepository.DeclineSelectedCross(bidPartId);
            return new JsonResult()
            {
                Data = updatedBidPartId,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SendEmailToCM(List<CMUserEntity> dataToPost, string bidName, int bidId)
        {

            int result = _crossRepository.AddCMAction(dataToPost, bidId);

            _emmailHelper.SendNotificationToFinalizeCrosses(bidName, bidId, dataToPost);
        
            _bidRepository.Submit(bidId, Constants.CategoryManager, "");

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult SendUnCrossedEmailToCM(string userId, string fullName, string bidName, int bidId, int bidPartId)
        {
            //var userEmail = _userInfoRepository.GetUserEmail(userId);
            
            int result = _crossRepository.AddUncrossedPartsCMAction(userId, bidId, bidPartId, System.Web.HttpContext.Current.User.Identity.Name);

            //_emmailHelper.SendNotificationToFinalizeCrosses(bidName, bidId, dataToPost); --no need to send notification 

           // _bidRepository.Submit(bidId, Constants.CategoryManager, "");

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult DownloadCrosses(int bidId, string bidName)
        {
            string fileName = bidName + "_Crosses_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            DataSet dsCross = _crossRepository.DownloadCrosses(bidId);
            ExportDataIntoExcel(dsCross, fileName);
            return File(Server.MapPath(fileName), "excel");
        }

        public ActionResult DownloadPartsWithoutCross(int bidId, string bidName)
        {
            string fileName = bidName + "_PartsWithoutCrosses_" + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".xlsx";
            DataSet dsCross = _crossRepository.DownloadPartsWithoutCross(bidId);
            ExportDataIntoExcel(dsCross, fileName);
            return File(Server.MapPath(fileName), "excel");
        }

        private void ExportDataIntoExcel(DataSet dsCross, string fileName)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet worksheet1 = pck.Workbook.Worksheets.Add("Crosses");
                worksheet1.Row(1).Height = 20;
                worksheet1.Row(1).Style.Font.Bold = true;               
                worksheet1.Cells["A1"].LoadFromDataTable(dsCross.Tables[0], true);
                worksheet1.View.FreezePanes(2, 1);
                worksheet1.Cells.AutoFitColumns();

                using (var memoryStream = new MemoryStream())
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                    Response.AddHeader("status", "210");
                    Response.AddHeader("X-Requested-With", "XMLHttpRequest");
                    pck.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public JsonResult UpdateBidPartVerificationAction(int bidPartId, bool isVerified)
        {
            int result = _crossRepository.UpdateBidPartVerificationAction(bidPartId, isVerified, System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to Update BidPart Note
        /// </summary>
        /// <param name="bidPartId"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        public JsonResult UpdateBidPartNote(int bidPartId, string note)
        {
            int result = _crossRepository.UpdateBidPartNote(bidPartId, note, System.Web.HttpContext.Current.User.Identity.Name);

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        /// <summary>
        /// Method to get Parts Without Cross
        /// </summary>
        /// <param name="bidId"></param>
        /// <returns></returns>
        public JsonResult PartsWithoutCross(int bidId)
        {
            var partsWithoutCross = _crossRepository.PartsWithoutCross(bidId);

            return new JsonResult()
            {
                Data = partsWithoutCross,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetCMForUncrossedParts()
        {
            var cmList = _crossRepository.GetCMForUncrossedParts();
            return new JsonResult()
            {
                Data = cmList,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult AssignUncrossedToMultipleCM(string cmUserId, int bidId, int[] bidPartId)
        {
            //var userEmail = _userInfoRepository.GetUserEmail(userId);
            int result = 0;
            foreach (var item in bidPartId)
            {
                result = _crossRepository.AddUncrossedPartsCMAction(cmUserId, bidId, item, System.Web.HttpContext.Current.User.Identity.Name);
            }
            
            //_emmailHelper.SendNotificationToFinalizeCrosses(bidName, bidId, dataToPost); --no need to send notification 

            // _bidRepository.Submit(bidId, Constants.CategoryManager, "");

            return new JsonResult()
            {
                Data = result,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}