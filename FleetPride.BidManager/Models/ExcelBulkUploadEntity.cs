﻿using FleetPride.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    [ExcelMapper(Header = 1, UseDisplayName = true)]
    public class VendorCMRelationEntity : BaseExcelModel
    {

        [Required(ErrorMessage = "vendor is Required.")]
        [Range(0, 999999999, ErrorMessage = "vendor should be between 1 to 999999999")]
        [DisplayName("vendor")]
        public int VendorNumber { get; set; }

        [Required(ErrorMessage = "Vendor Name is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("vname")]
        public string VendorName { get; set; }

        //[Required(ErrorMessage = "DFTCAT is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("DFTCAT")]
        public string DefaultCategory { get; set; }

        //[Required(ErrorMessage = "buyer is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("buyer")]
        public string Buyer { get; set; }

        //[Required(ErrorMessage = "CATMGR is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("CATMGR")]
        public string CategoryManager { get; set; }

        //[Required(ErrorMessage = "PartCount is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("PartCount")]
        public string PartCount { get; set; }


    }
}