﻿using FleetPride.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    [ExcelMapper(Header = 1, UseDisplayName = true)]
    public class VendorGradeEntity : BaseExcelModel
    {
        public int VendorGradeId { get; set; }

        [Required(ErrorMessage = "Category is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("Category")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Vendor Name is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("Vendor Name")]
        public string VendorName { get; set; }

        [Required(ErrorMessage = "Vendor Number is Required.")]
        [Range(0, 999999999, ErrorMessage = "Vendor Number should be between 1 to 999999999")]
        [DisplayName("Vendor Number")]
        public int VendorNumber { get; set; }

        [Required(ErrorMessage = "Color Code is Required.")]
        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("Color Code")]
        public string ColorCode { get; set; }

        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("Color")]
        public string Color { get; set; }

        [StringLength(255, ErrorMessage = "{0} value cannot exceed {1} characters. ")]
        [DisplayName("Definition")]
        public string Definition { get; set; }

        public string LastModifiedBy { get; set; }
        public string LastModifiedOn { get; set; }
    }
}