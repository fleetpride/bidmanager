﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class BidEntity
    {
        public int BidId { get; set; }
        public bool IsProspect { get; set; }
        public string BidStatus { get; set; }
        public string BidType { get; set; }
        public string BidName { get; set; }
        public string BidDescription { get; set; }
        public string DueDate { get; set; }
        public string LaunchDate { get; set; }
        public string ActualLaunchDate { get; set; }
        public string CalculatedDueDate { get; set; }
        public string CurrentPhaseDueDate { get; set; }
        public string Rank { get; set; }
        public string Company { get; set; }
        public string CustNumber { get; set; }
        public string CustBranch { get; set; }
        public string CustName { get; set; }
        public string CustCorpId { get; set; }
        public string CustCorpAccName { get; set; }
        public string GroupId { get; set; }
        public string GroupDesc { get; set; }
        public string RequestType { get; set; }
        public string SubmittedDate { get; set; }
        public string FPFacingLocations { get; set; }
        public string TotalValueOfBid { get; set; }
        public string ProjectedGPDollar { get; set; }
        public string ProjectedGPPerc { get; set; }
        public bool IsFPPriceHold { get; set; }
        public string HoldDate { get; set; }
        public bool IsCustomerAllowSubstitutions { get; set; }
        public string BrandPreference { get; set; }
        public bool IsCustomerInventoryPurchaseGuarantee { get; set; }
        public string LevelOfInvetoryDetailCustomerCanProvide { get; set; }
        public string SourceExcelFileName { get; set; }
        public string CreatedByRole { get; set; }
        public string NAMUserID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Comments  { get; set; }
        public string BidStatusDisplay { get; set; }
        public string BidStatusDescirption { get; set; }     
        public bool CrossActionStatus { get; set; }
        public bool InventoryActionStatus { get; set; }
        public string CreatedByFullName { get; set; }
    }
}