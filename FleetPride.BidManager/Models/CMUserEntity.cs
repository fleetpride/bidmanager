﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class CMUserEntity
    {
        public string CMName { get; set; }
        public string CMUserId { get; set; }
        public string CMEmail { get; set; }

    }
}