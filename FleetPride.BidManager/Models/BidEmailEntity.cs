﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class BidEmailEntity
    {
        public int BidId { get; set; }
        public string BidStatus { get; set; }
        public string BidType { get; set; }
        public string BidName { get; set; }
        public string RVPEmail { get; set; }
        public string CreatorEmail { get; set; }
        public string CMEmail { get; set; }
        public string NAMEmail { get; set; }
        public string CmoEmail { get; set; }
    }
}