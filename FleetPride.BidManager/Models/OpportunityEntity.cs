﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace FleetPride.BidManager.Models
{
    public class OpportunityEntity
    {
        public string OppId { get; set; }

        public string OpportunityName { get; set; }

        /// <summary>
        /// Only date no time
        /// </summary>
        public DateTime closeDate { get; set; }

        /// <summary>
        /// 	01-New,50-Closed Won, 90-Closed Lost
        /// </summary>
        public string OpportunityStage { get; set; }
        public string AccountIseriesCode { get; set; }
        public decimal OpportunityAmount { get; set; }
        public string AccountGroup { get; set; }

        public string GetInsertJson()
        {
            var recordArray = new object[1];
            recordArray[0] = new {
                OpportunityName = this.OpportunityName
                , AccountIseriesCode = this.AccountIseriesCode
                , AccountGroup = this.AccountGroup
                , OpportunityAmount = this.OpportunityAmount
                , OpportunityStage = this.OpportunityStage
                , closeDate = this.closeDate.ToString("MM/dd/yyyy")
            };

            return GetJson(recordArray);
        }

        public string GetUpdateJson()
        {
            var recordArray = new object[1];
            recordArray[0] = new
            {
                OppId = this.OppId
                //,OpportunityAmount = this.OpportunityAmount
                ,OpportunityStage = this.OpportunityStage
            };

            return GetJson(recordArray);
        }

        private string GetJson(object[] recordArray)
        {
            var record = new { Record = recordArray };

            var serializer = new JsonSerializer();
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, record);
            return sw.ToString();
        }
    }
}