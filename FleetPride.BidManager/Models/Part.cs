﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class Part
    {
        public string PartInfo { get; set; }
        public int ProductKey { get; set; }
        public bool IsFlip { get; set; }
    }
}