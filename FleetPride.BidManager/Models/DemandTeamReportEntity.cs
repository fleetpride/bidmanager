﻿using FleetPride.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
     
    public class DemandTeamReportEntity
    {
        public int CrossPartInventoryId { get; set; }

        public int BidPartId { get; set; }

        public string ManufacturerPartNumber { get; set; }

        public string CustomerPartNumber { get; set; }

        public string PartDescription { get; set; }

        public int CrossPartId { get; set; }

        public string Pool { get; set; }

        public string PartNumber { get; set; }

        public string Price { get; set; }

        public string StockingType { get; set; }

        public string Location { get; set; }        

        public string CustPartUOM { get; set; }

        public string EstimatedAnnualUsage { get; set; }

        public string OldPool { get; set; }

        public string OldPartNumber { get; set; }        

        public string OldPrice { get; set; }

        public string OldStockingType { get; set; }

        public string CategoryManagerID { get; set; }
        
        public string IsActive { get; set; }

        public string LastUpdatedBy { get; set; }

        public string LastUpdatedOn { get; set; }

        public string ParentKit { get; set; }
        
        
    }
}