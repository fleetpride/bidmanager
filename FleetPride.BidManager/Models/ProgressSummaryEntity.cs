﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class ProgressSummaryEntity
    {
        public int BidId { get; set; }
        public string BidStatus { get; set; }
        public string BidStatusDisplay { get; set; }
        public string BidStatusDescirption { get; set; }
        public string BidType { get; set; }
        public string BidName { get; set; }
        public string BidDescription { get; set; }
        public string DueDate { get; set; }
        public string LaunchDate { get; set; }
        public string CalculatedDueDate { get; set; }
        public string CurrentPhaseDueDate { get; set; }
        public string Company { get; set; }
        public string CustNumber { get; set; }
        public string CustBranch { get; set; }
        public string CustName { get; set; }
        public string CustCorpId { get; set; }
        public string CustCorpAccName { get; set; }
        public string GroupId { get; set; }
        public string GroupDesc { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string CrossesReviewedBy { get; set; }
        public string CrossesNotReviewedBy { get; set; }
        public string InventoryReviewedBy { get; set; }
        public string InventoryNotReviewedBy { get; set; }
        public string CrossesFinalized { get; set; }
    }
}