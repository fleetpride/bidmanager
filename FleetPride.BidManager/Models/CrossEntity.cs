﻿using FleetPride.BidManager.Utilities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using FleetPride.Extensions;
using System.Data;

namespace FleetPride.BidManager.Models
{
    public class CrossEntity
    {
        public int CrossPartId { get; set; }
        public int BidPartId { get; set; }
        public int ProductKey { get; set; }
        public string CrossRank { get; set; }
        public string ReferenceTableName { get; set; }
        public string Source { get; set; }
        public string Company { get; set; }
        public string PartNumber { get; set; }
        public string PoolNumber { get; set; }
        public string PartDescription { get; set; }
        public string PartCategory { get; set; }
        public string VendorNumber { get; set; }
        public string VendorColor { get; set; }
        public string VendorName { get; set; }
        public string VendorColorDefinition { get; set; }
        public string Manufacturer { get; set; }
        public bool IsFlip { get; set; }
        public bool IsNonFP { get; set; }
        public string ValidationStatus { get; set; }
        public string ValidatedBy { get; set; }
        public string ValidatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string FinalPreference { get; set; }
        public string Comments { get; set; }
        public string PriceToQuote { get; set; }
        public string SuggestedPrice { get; set; }
        public string ICost { get; set; }
        public string Margin { get; set; }
        public string AdjustedICost { get; set; }
        public string AdjustedMargin { get; set; }
        public string IsWon { get; set; }
        public string DCStockCount { get; set; }
        public string StoresStockCount { get; set; }
        public string NationalL12StoresSold { get; set; }
        public string LocalL12StoresSold { get; set; }
        public int TotalL12StoresSold {
            get
            {
                return ConvertExtensions.ToInt(this.NationalL12StoresSold) + ConvertExtensions.ToInt(this.LocalL12StoresSold);
            }
        }
        public string L12Units { get; set; }
        public string L12Revenue { get; set; }
        public string NPMPartType { get; set; }
        public string CustomerPartNumber { get; set; }
        public CrossEntity(SqlDataReader rdr)
        {
            this.CrossPartId = Convert.ToInt32(rdr["CrossPartId"]);
            this.BidPartId = Convert.ToInt32(rdr["BidPartId"]);
            this.CrossRank = Convert.ToString(rdr["CrossRank"] == DBNull.Value ? 0 : rdr["CrossRank"]);
            //this.ProductKey = Convert.ToInt32(rdr["ProductKey"]);
            this.ReferenceTableName = Convert.ToString(rdr["ReferenceTableName"]);
            this.Source = Convert.ToString(rdr["Source"]);
            this.Company = Convert.ToString(rdr["Company"]);
            this.PartNumber = Convert.ToString(rdr["PartNumber"]);
            this.PoolNumber = Convert.ToString(rdr["PoolNumber"]);
            this.PartDescription = Convert.ToString(rdr["PartDescription"]);
            this.PartCategory = Convert.ToString(rdr["PartCategory"]);
            this.VendorNumber = Convert.ToString(rdr["VendorNumber"]);
            this.VendorColor = Convert.ToString(rdr["VendorColor"]);
            this.VendorName = Convert.ToString(rdr["VendorName"]);
            this.VendorColorDefinition = Convert.ToString(rdr["VendorColorDefinition"]);
            this.Manufacturer = Convert.ToString(rdr["Manufacturer"]);
            this.IsFlip = Convert.ToBoolean(rdr["IsFlip"] == DBNull.Value ? false : rdr["IsFlip"]);
            this.IsNonFP = Convert.ToBoolean(rdr["IsNonFP"] == DBNull.Value ? false : rdr["IsNonFP"]);
            this.ValidationStatus = Convert.ToString(rdr["ValidationStatus"] == DBNull.Value ? Constants.ValidationAwaited : rdr["ValidationStatus"]);
            this.ValidatedBy = Convert.ToString(rdr["ValidatedBy"] == DBNull.Value ? "" : rdr["ValidatedBy"]);
            this.ValidatedOn = Convert.ToString(rdr["ValidatedOn"] == DBNull.Value ? "" : rdr["ValidatedOn"]);
            this.CreatedBy = Convert.ToString(rdr["CreatedBy"] == DBNull.Value ? "" : rdr["CreatedBy"]);
            this.CreatedOn = Convert.ToString(rdr["CreatedOn"] == DBNull.Value ? "" : rdr["CreatedOn"]);
            this.FinalPreference = Convert.ToString(rdr["FinalPreference"] == DBNull.Value ? "" : rdr["FinalPreference"]);
            this.Comments = Convert.ToString(rdr["Comments"] == DBNull.Value ? "" : rdr["Comments"]);
            this.PriceToQuote = Convert.ToString(rdr["PriceToQuote"]);
            this.SuggestedPrice = Convert.ToString(rdr["SuggestedPrice"]);
            this.ICost = Convert.ToString(rdr["ICOST"]);
            this.Margin = Convert.ToString(rdr["Margin"]);
            this.AdjustedICost = Convert.ToString(rdr["AdjustedICost"]);
            this.AdjustedMargin = Convert.ToString(rdr["AdjustedMargin"]);
            this.IsWon = Convert.ToString(rdr["IsWon"]);
            this.DCStockCount = Convert.ToString(rdr["DCStockCount"] == DBNull.Value ? "" : rdr["DCStockCount"]);
            this.StoresStockCount = Convert.ToString(rdr["StoresStockCount"] == DBNull.Value ? "" : rdr["StoresStockCount"]);
            this.NationalL12StoresSold = Convert.ToString(rdr["NationalL12StoresSold"] == DBNull.Value ? "" : rdr["NationalL12StoresSold"]);
            this.LocalL12StoresSold = Convert.ToString(rdr["LocalL12StoresSold"] == DBNull.Value ? "" : rdr["LocalL12StoresSold"]);
            this.L12Units = Convert.ToString(rdr["L12Units"] == DBNull.Value ? "" : rdr["L12Units"]);
            this.L12Revenue = Convert.ToString(rdr["L12Revenue"] == DBNull.Value ? "" : rdr["L12Revenue"]);
            this.NPMPartType = Convert.ToString(rdr["NPMPartType"] == DBNull.Value ? "" : rdr["NPMPartType"]);
        }
        public CrossEntity(DataRow row)
        {
            this.CrossPartId = Convert.ToInt32(row["CrossPartId"]);
            this.BidPartId = Convert.ToInt32(row["BidPartId"]);
            this.CrossRank = Convert.ToString(row["CrossRank"] == DBNull.Value ? 0 : row["CrossRank"]);
            //this.ProductKey = Convert.ToInt32(row["ProductKey"]);
            this.ReferenceTableName = Convert.ToString(row["ReferenceTableName"]);
            this.Source = Convert.ToString(row["Source"]);
            this.Company = Convert.ToString(row["Company"]);
            this.PartNumber = Convert.ToString(row["PartNumber"]);
            this.PoolNumber = Convert.ToString(row["PoolNumber"]);
            this.PartDescription = Convert.ToString(row["PartDescription"]);
            this.PartCategory = Convert.ToString(row["PartCategory"]);
            this.VendorNumber = Convert.ToString(row["VendorNumber"]);
            this.VendorColor = Convert.ToString(row["VendorColor"]);
            this.VendorName = Convert.ToString(row["VendorName"]);
            this.VendorColorDefinition = Convert.ToString(row["VendorColorDefinition"]);
            this.Manufacturer = Convert.ToString(row["Manufacturer"]);
            this.IsFlip = Convert.ToBoolean(row["IsFlip"] == DBNull.Value ? false : row["IsFlip"]);
            this.IsNonFP = Convert.ToBoolean(row["IsNonFP"] == DBNull.Value ? false : row["IsNonFP"]);
            this.ValidationStatus = Convert.ToString(row["ValidationStatus"] == DBNull.Value ? Constants.ValidationAwaited : row["ValidationStatus"]);
            this.ValidatedBy = Convert.ToString(row["ValidatedBy"] == DBNull.Value ? "" : row["ValidatedBy"]);
            this.ValidatedOn = Convert.ToString(row["ValidatedOn"] == DBNull.Value ? "" : row["ValidatedOn"]);
            this.CreatedBy = Convert.ToString(row["CreatedBy"] == DBNull.Value ? "" : row["CreatedBy"]);
            this.CreatedOn = Convert.ToString(row["CreatedOn"] == DBNull.Value ? "" : row["CreatedOn"]);
            this.FinalPreference = Convert.ToString(row["FinalPreference"] == DBNull.Value ? "" : row["FinalPreference"]);
            this.Comments = Convert.ToString(row["Comments"] == DBNull.Value ? "" : row["Comments"]);
            this.PriceToQuote = Convert.ToString(row["PriceToQuote"]);
            this.SuggestedPrice = Convert.ToString(row["SuggestedPrice"]);
            this.ICost = Convert.ToString(row["ICOST"]);
            this.Margin = Convert.ToString(row["Margin"]);
            this.AdjustedICost = Convert.ToString(row["AdjustedICost"]);
            this.AdjustedMargin = Convert.ToString(row["AdjustedMargin"]);
            this.IsWon = Convert.ToString(row["IsWon"]);
            this.DCStockCount = Convert.ToString(row["DCStockCount"] == DBNull.Value ? "" : row["DCStockCount"]);
            this.StoresStockCount = Convert.ToString(row["StoresStockCount"] == DBNull.Value ? "" : row["StoresStockCount"]);
            this.NationalL12StoresSold = Convert.ToString(row["NationalL12StoresSold"] == DBNull.Value ? "" : row["NationalL12StoresSold"]);
            this.LocalL12StoresSold = Convert.ToString(row["LocalL12StoresSold"] == DBNull.Value ? "" : row["LocalL12StoresSold"]);
            this.L12Units = Convert.ToString(row["L12Units"] == DBNull.Value ? "" : row["L12Units"]);
            this.L12Revenue = Convert.ToString(row["L12Revenue"] == DBNull.Value ? "" : row["L12Revenue"]);
            this.NPMPartType = Convert.ToString(row["NPMPartType"] == DBNull.Value ? "" : row["NPMPartType"]);
            this.CustomerPartNumber = Convert.ToString(row["CustomerPartNumber"] == DBNull.Value ? "" : row["CustomerPartNumber"]); 
        }

        public CrossEntity()
        {
        }

    }
    public class FinalCrossEntity
    {
        public int CrossPartId { get; set; }
        public int BidPartId { get; set; }
        public string FinalPreference { get; set; }
    }

}