﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class CrosReportUpdates
    {
        public int BidPartId { get; set; }
        public string CrossPartId { get; set; }
        public string FinalPreference { get; set; }
        public bool IsVerified { get; set; }
    }
}