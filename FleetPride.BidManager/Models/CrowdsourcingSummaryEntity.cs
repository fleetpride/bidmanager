﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class CrowdsourcingSummaryEntity
    {
        public int BidId { get; set; }
        public string BidName { get; set; }
        public string BidStatus { get; set; }
        public string BidType { get; set; }
        public string Rank { get; set; }
        public string DueDate { get; set; }
        public string Status { get; set; }
        public string UploadPartCount { get; set; }
        public string CrowdSourcingPartCount { get; set; }
        public string UserSubmittedPartCount { get; set; }
        public string ValidatedPartCount { get; set; }
        public string PartsHolded { get; set; }
        public string PartsWithCrossEntry { get; set; }
        public string EnteredCrosses { get; set; }
        public string ValidatedCrosses { get; set; }
        public string RemainingCrosses { get; set; }
        public string CrossesFinalized { get; set; }
    }
}