﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class CrossPartLineReviewEntity
    {
        public int ID { get; set; }
        public int CrossPartID { get; set; }
        public int BidID { get; set; }
        public int BidPartID { get; set; }
        public string CrossPartNumber { get; set; }
        public string CrossPoolNumber { get; set; }
        public int ExistingStockingLocations { get; set; }
        public int DCStockCount { get; set; }
        public int L12Units { get; set; }
        public int NewStockingLocations { get; set; }
        public int ProjectedL12Units { get; set; }
        public decimal PriceToQuote { get; set; }
        public decimal ProjectedL12GP { get; set; }
        public decimal ProjectedInvestment { get; set; }
        public decimal ICost { get; set; }
        public decimal MedianCost { get; set; }
        public string StockingType { get; set; }
        public bool IsApproved { get; set; }
        public decimal ProjectedL12Revenue { get; set; }

        public string CrossPartDescription { get; set; }
        public string CrossPartCategory { get; set; }
        public string CrossPartVendor { get; set; }

        public int AlreadyStockingLocations { get; set; }
        public string CategoryManagerID { get; set; }
        public string CategoryDirectorID { get; set; }
        public string CategoryCmoID { get; set; }
        public string CreatedOn { get; set; }
        public string Comments { get; set; }

        public string NetNewStockingLocations
        {
            get
            {
                int val = Convert.ToInt32(this.NewStockingLocations) - Convert.ToInt32(this.AlreadyStockingLocations);
                if (val < 0)
                    val = 0;
                return val.ToString();
            }
        }

        public decimal CurrentL12Revenue { get; set; }
        public string ProjectedMonthlyUnits { get; set; }
        public int SalesPack { get; set; }

        public string RevisedPool { get; set; }
        public string RevisedPartNumber { get; set; }
        public String Estimated3MonthUsageWithoutSalesPack { get; set; }
        public String Estimated3MonthUsageWithSalesPack { get; set; }
    }
}