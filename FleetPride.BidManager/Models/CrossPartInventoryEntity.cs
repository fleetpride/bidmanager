﻿using FleetPride.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    [ExcelMapper(Header = 1, UseDisplayName = true)]
    public class CrossPartInventoryEntity : BaseExcelModel
    {
        public int CrossPartInventoryId { get; set; }

        public int BidPartId { get; set; }

        [DisplayName("Manufacturer Part Number")]
        public string ManufacturerPartNumber { get; set; }

        [DisplayName("Customer Part Number")]
        public string CustomerPartNumber { get; set; }

        [DisplayName("Part Description")]
        public string PartDescription { get; set; }

        public int CrossPartId { get; set; }

        [Required(ErrorMessage = "FP Pool is mandatory")]
        [DisplayName("FP Pool")]
        public string CrossPoolNumber { get; set; }

        [Required(ErrorMessage = "FP Part No is mandatory")]
        [DisplayName("FP Part No")]        
        public string CrossPartNumber { get; set; }

        [DisplayName("FP Part Description")]
        public string CrossPartDescription { get; set; }

        [DisplayName("Price")]
        public string CrossPrice { get; set; }

        [DisplayName("ICost")]
        public string CrossICOST { get; set; }

        [Required(ErrorMessage = "Location is mandatory")]
        [DisplayName("Location")]
        public string Location { get; set; }        

        [Range(1, 99999, ErrorMessage = "Invalid Cust Part UOM, should be between 1 to 99999")]
        [DisplayName("Cust Part UOM")]
        public string CustPartUOM { get; set; }

        [Required(ErrorMessage = "Estimated Annual Usage is mandatory")]
        [Range(0, 99999999, ErrorMessage = "Invalid Estimated Annual Usage, should be between 1 to 99999999")]
        [DisplayName("Estimated Annual Usage")]
        public decimal EstimatedAnnualUsage { get; set; }

        [Range(1, 99999, ErrorMessage = "Invalid New Pool, should be between 1 to 99999")]
        [DisplayName("New Pool")]
        public string NewPool { get; set; }

        [DisplayName("New Part No")]
        public string NewPartNo { get; set; }

        [Range(1, 9999999.99, ErrorMessage = "Invalid New Price, should be between 1 to 9999999")]
        [DisplayName("New Price")]
        public string NewPrice { get; set; }

        [DisplayName("IsActive(Yes/No)")]
        public string IsActive { get; set; }

        public string ParentKit { get; set; }

        public string LastUpdatedBy { get; set; }

        public string LastUpdatedOn { get; set; }
        public string StockingType { get; set; }
        public string NewStockingType { get; set; }

        public string RevisePartComments { get; set; }
        public string RevisedPool { get; set; }
        public string RevisedPartNumber { get; set; }
    }
}