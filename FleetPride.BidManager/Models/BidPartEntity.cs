﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class BidPartEntity
    {
        public int BidPartId { get; set; }
        public int BidId { get; set; }
        public string CustomerPartNumber { get; set; }
        public string PartDescription { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturerPartNumber { get; set; }
        public string Note { get; set; }
        public string EstimatedAnnualUsage { get; set; }
        public string IsCrowdSourcingEligible { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string IsSubmitted { get; set; }
        public string IsValidated { get; set; }
        public string SubmittedBy { get; set; }
        public string SubmittedOn { get; set; }
        public string ValidatedBy { get; set; }
        public string ValidatedOn { get; set; }
        public string IsFinalized { get; set; }
        public bool IsVerified { get; set; }
        public string Comments { get; set; }
        //public int PrimaryCross_CrossPartId { get; internal set; }
        //public int PrimaryCross_BidPartId { get; internal set; }
        //public string PrimaryCross_CrossRank { get; internal set; }
        //public string PrimaryCross_ReferenceTableName { get; internal set; }
        //public string PrimaryCross_Source { get; internal set; }
        //public string PrimaryCross_Company { get; internal set; }
        //public string PrimaryCross_PartNumber { get; internal set; }
        //public string PrimaryCross_PoolNumber { get; internal set; }
        //public string PrimaryCross_PartDescription { get; internal set; }
        //public string PrimaryCross_PartCategory { get; internal set; }
        //public string PrimaryCross_VendorNumber { get; internal set; }
        //public string PrimaryCross_VendorColor { get; internal set; }
        //public string PrimaryCross_VendorName { get; internal set; }
        //public string PrimaryCross_VendorColorDefinition { get; internal set; }
        //public string PrimaryCross_Manufacturer { get; internal set; }
        //public bool PrimaryCross_IsFlip { get; internal set; }
        //public bool PrimaryCross_IsNonFP { get; internal set; }
        //public string PrimaryCross_ValidationStatus { get; internal set; }
        //public string PrimaryCross_ValidatedBy { get; internal set; }
        //public string PrimaryCross_ValidatedOn { get; internal set; }
        //public string PrimaryCross_CreatedBy { get; internal set; }
        //public string PrimaryCross_CreatedOn { get; internal set; }
        //public string PrimaryCross_FinalPreference { get; internal set; }
        //public string PrimaryCross_Comments { get; internal set; }
        //public string PrimaryCross_PriceToQuote { get; internal set; }
        //public string PrimaryCross_SuggestedPrice { get; internal set; }
        //public string PrimaryCross_ICost { get; internal set; }
        //public string PrimaryCross_Margin { get; internal set; }
        //public string PrimaryCross_AdjustedICost { get; internal set; }
        //public string PrimaryCross_AdjustedMargin { get; internal set; }
        //public string PrimaryCross_IsWon { get; internal set; }
        //public string PrimaryCross_DCStockCount { get; internal set; }
        //public string PrimaryCross_StoresStockCount { get; internal set; }
        //public string PrimaryCross_NationalL12StoresSold { get; internal set; }
        //public string PrimaryCross_LocalL12StoresSold { get; internal set; }
        //public string PrimaryCross_L12Units { get; internal set; }
        //public string PrimaryCross_L12Revenue { get; internal set; }
        //public string PrimaryCross_NPMPartType { get; internal set; }

        public CrossEntity PrimaryCross { get; set; }

        public string CMUserId { get; set; }
    }
}