﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetPride.BidManager.Models;

namespace FleetPride.BidManager.Models
{
    public class CrossEntityWithPrepopulatedData
    {
        public List<CrossEntity> Crosses { get; set; }

        public List<PreviouslyWonCross> PrevouslyWonParts { get; set; }
    }
}