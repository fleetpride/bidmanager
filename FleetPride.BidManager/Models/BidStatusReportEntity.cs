﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class BidStatusReportEntity
    {
        public int Id { get; set; }        
        public int BidId { get; set; }
        public bool IsProspect { get; set; }
        public string BidStatus { get; set; }
        public string BidStatusDisplay { get; set; }
        public string BidStatusDescirption { get; set; }
        public string BidType { get; set; }
        public string BidName { get; set; }
        public string BidDescription { get; set; }        
        public string LaunchDate { get; set; }
        public string Company { get; set; }
        public string CustNumber { get; set; }
        public string CustBranch { get; set; }
        public string CustName { get; set; }
        public string CustCorpId { get; set; }
        public string CustCorpAccName { get; set; }
        public string GroupId { get; set; }
        public string GroupDesc { get; set; }      
        public int BidPartId { get; set; }
        public string CustomerPartNumber { get; set; }
        public string PartDescription { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturerPartNumber { get; set; }
        public int CrossPartId { get; set; }
        public string CrossPoolNumber { get; set; }
        public string CrossPartNumber { get; set; }
        public string CrossPartDescription { get; set; }
        public string PriceToQuote { get; set; }
        public int CrossPartInventoryId { get; set; }
        public string Location { get; set; }
        public string CustPartUOM { get; set; }
        public string EstimatedAnnualUsage { get; set; }
        public string NewPool { get; set; }
        public string NewPartNo { get; set; }
        public string NewPrice { get; set; }
        public string IsActive { get; set; }
        public string IType { get; set; }
        public string NewIType { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public int CrossPartLineReviewId { get; set; }
        public int DCSafetyStock { get; set; }
        public string NPPartType { get; set; }
        public string NewNPPartType { get; set; }
        public string CrossSupersedeNote { get; set; }
        public string NewSupersedeNote { get; set; }        
        public string CategoryManagerID { get; set; }
        public string CategoryManagerName { get; set; }
        public bool IsApproved { get; set; }
        public string ParentKit { get; set; }
    }
}