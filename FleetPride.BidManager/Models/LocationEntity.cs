﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class LocationEntity
    {
        public string Location { get; set; }
        public string LocationName { get; set; }
        public string TerritoryId { get; set; }
        public string PriceRegion { get; set; }

    }
}