﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace FleetPride.BidManager.Models
{
    public class PreviouslyWonCross
    {
        public PreviouslyWonCross(DataRow row)
        {
            this.ReferenceTableName = Convert.ToString(row["ReferenceTableName"] == DBNull.Value ? string.Empty : row["ReferenceTableName"]);
            this.BidPartNumber = Convert.ToString(row["BidPartNumber"] == DBNull.Value ? string.Empty : row["BidPartNumber"]);
            this.CrossPartNumber = Convert.ToString(row["CrossPartNumber"] == DBNull.Value ? string.Empty : row["CrossPartNumber"]);
            this.FinalPreference = Convert.ToString(row["FinalPreference"] == DBNull.Value ? string.Empty : row["FinalPreference"]);
        }
        public PreviouslyWonCross()
        {

        }
        public string ReferenceTableName { get; set; }
        public string BidPartNumber { get; set; }
        public string CrossPartNumber { get; set; }
        public string FinalPreference { get; set; }
    }
}