﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetPride.BidManager.Models
{
    public class UserEntity
    {
        public string UserId { get; set; }
        public string UserRole { get; set; }
        public string UserEmail { get; set; }
 
    }

    public class Users
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
    }
    //public class UserEntityComparer : IEqualityComparer<UserEntity>
    //{
    //    public bool Equals(UserEntity x, UserEntity y)
    //    {
    //        return string.Equals(x.UserId, y.UserId, StringComparison.InvariantCultureIgnoreCase);
    //    }

    //    public int GetHashCode(UserEntity obj)
    //    {
    //        return obj.GetHashCode();
    //    }
    //}
}